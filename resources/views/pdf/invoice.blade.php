<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Invoice Pemesanan</title>
	<style type="text/css">
		body{
			font-family: sans-serif;
		}
		table td{
			font-size: 14px;
		}
		table.data td,
		table.data th{
			border:2px solid black;
			padding: 5px;
		}
		table.data{
			border-collapse: collapse;
		}
		.text-center{
			text-align: center;
		}

		.page-break {
			page-break-after: always;
		}
	</style>

</head>
<body>
	<table width="100%" style="margin-bottom: 10px">
		<tr>
			<td width="40%">
				<img style="width: 300px;" src="{{ asset('images/logo-biqinin.png') }}" alt="Logo">
			</td>
			<td>
				<h2 class="text-center">{{ $data['site_name'] }}</h2>
				<p style="margin:0" class="text-center">{{ $data['office_address'] }}</p>
				<p style="margin:0" class="text-center">Telepon/Whatsapp : {{ $data[
				'office_phone'] }}/{{ $data['office_whatsapp'] }}</p>
				<p style="margin:0" class="text-center">Email : {{ $data['email'] }}</p>
			</td>
		</tr>
	</table>
	<div style="background: #666; height: 3px"></div>
	<table style="margin-top: 20px;" width="100%" class="data">
		<caption><h3>Invoice Pemesanan Biqinin.com<br> 
		</h3></caption>

		<tr>
			<td style="width: 35%">Nomor Invoice</td>
			<td>{{ $data['invoice'] }}</td>
		</tr>
		<tr>
			<td>Tanggal Pesanan</td>
			<td>{{ $data['order_date'] }}</td>
		</tr>

		<tr>
			<td>Nama Pemesanan</td>
			<td>{{ $data['customer'] }}</td>
		</tr>

		<tr>
			<td>Alamat Pengiriman</td>
			<td>{{ $data['address'] }}</td>
		</tr>
		<tr>
			<td>Jenis Kayu Yang Dipilih</td>
			<td>{{ $data['wood'] }}</td>
		</tr>
		<tr>
			<td>Jenis Finishing Warna</td>
			<td>{{ $data['finishing'] }}</td>
		</tr>
		<tr>
			<td>Jumlah Pesanan</td>
			<td>{{ $data['number_of_orders'] }}</td>
		</tr>
		<tr>
			<td>Harga Barang</td>
			<td>{{ $data['total_price'] }}</td>
		</tr>
		<tr>
			<td>Estimasi Pengerjaan</td>
			<td>{{ $data['work_estimation'] }}</td>
		</tr>
		<tr>
			<td>Kurir Pengiriman</td>
			<td>{{ $data['courier'] }}</td>
		</tr>
		<tr>
			<td>Biaya Pengiriman</td>
			<td>{{ $data['courier_cost'] }}</td>
		</tr>
		<tr>
			<td>Informasi Tambahan</td>
			<td>{{ $data['additional_information'] }}</td>
		</tr>

		<tr>
			<td>Total Pembayaran</td>
			<td>{{ $data['total'] }}</td>
		</tr>
		
	</table>

	<table style="width: 100%; margin-top: 20px">

        <tr>
            <td style="width: 70%;" colspan="2"></td>
            <td style="height: 60px; vertical-align: bottom;" align="center"> 
               {{ $data['admin'] }}
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td style="height: 100px; vertical-align: bottom;" align="center">
                <span style="text-decoration: underline;">{{ tanggalIndonesia(\Carbon\Carbon::now()) }} <br> 
            </td>
        </tr>
    </table>

	<div class="page-break"></div>

	<table width="100%" style="margin-bottom: 10px">
		<tr>
			<td width="40%">
				<img style="width: 300px;" src="{{ asset('images/logo-biqinin.png') }}" alt="Logo">
			</td>
			<td>
				<h2 class="text-center">{{ $data['site_name'] }}</h2>
				<p style="margin:0" class="text-center">{{ $data['office_address'] }}</p>
				<p style="margin:0" class="text-center">Telepon/Whatsapp : {{ $data[
				'office_phone'] }}/{{ $data['office_whatsapp'] }}</p>
				<p style="margin:0" class="text-center">Email : {{ $data['email'] }}</p>
			</td>
		</tr>
	</table>
	<div style="background: #666; height: 3px"></div>
	
	<h3>Penerima Barang</h3>
	<p>
		<b>Nama Penerima</b> : <br>
		{{ $data['customer'] }}
	</p>

	<p>
		<b>Alamat Penerima</b> : <br>
		Jalan KHA Dahlan No. 07 <br>
		Kelurahan Kauman, Kecamatanan batang <br>
		Batang, Jawa Tengah <br>
		51215
	</p>
	<p>
		<b>No. HP / Whatsapp</b> : <br>
		62893784637467
	</p>
	<div style="margin: 30px 0px 30px 0px;border-bottom: solid 2px black"></div>

	<div style="margin-top: 30px; margin-bottom: 30px">
		<h3>Pengirim Barang</h3>
		<p>
			<b>Nama Pengirim</b> : <br>
			{{ $data['site_name'] }}
		</p>

		<p>
			<b>No. HP / Whatsapp</b> : <br>
			62893784637467
		</p>
	</div>

	<div style="margin-top: 40px;border-bottom: dashed 2px black"></div>

</body>
</html>