@extends('client.index')
@section('extra-content')
<div class="col-md-6 col-sm-6 col-xs-12 col-lg-6">
  <div class="banner-content" style="padding-top: 10px !important;">
  	<h1 style="color: white !important;">Biqinin.com</h1>
    <p style="font-size: 1.2em" class="text-justify">
    	{{ $about }}
    </p>
  </div><!-- /.banner-content -->
</div><!-- /.col-md-6 -->

@endsection