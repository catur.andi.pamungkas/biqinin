@extends('client.index')
@section('extra-content')
<div style="margin: 10px 0px; " class="col-md-6 col-sm-6 col-xs-12 col-lg-6">
    <div style="width:100%; padding:20px 30px;border-radius: 6px; background: #fff;box-shadow: -1px 0px 48px -2px rgba(0,0,0,0.44);"
        class="main-content">
        <h3 class="text-center">Masuk Untuk Memulai Pemesanan</h3>
        <div class="main">
            {{-- <div class="row">
        <div class="col-sm-6 col-xs-12">
          <a href="{{ url('auth/facebook') }}" class="mb-10 btn btn-lg btn-primary btn-block"><i
                class="fa fa-facebook-square"></i> Facebook</a>
        </div>
        <div class="col-sm-6 col-xs-12">
            <a href="{{ url('auth/google') }}" class="mb-10 btn btn-lg btn-danger btn-block"><i
                    class="fa fa-google"></i> Google</a>
        </div>
    </div>
    <div class="login-or">
        <hr class="hr-or">
        <span class="span-or">or</span>
    </div> --}}

    <form role="form" method="POST" action="{{ route('login') }}">
        @csrf
        <div class="form-group {{ $errors->has('username') ? 'has-error' : '' }}">
            <label for="username">Username</label>
            <input placeholder="Username" type="text" name="username" class="form-control" id="email"
                value="{{ old('username') }}" />

            @if ($errors->has('username'))
            <span class="help-block" role="alert">
                <strong>{{ $errors->first('username') }}</strong>
            </span>
            @endif

        </div>
        <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">

            <label for="password">Password</label>
            <input placeholder="Password" type="password" name="password" class="form-control" id="password" />

            @if ($errors->has('password'))
            <span class="help-block" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
            @endif

            @if (Route::has('password.request'))
            <a class="btn btn-link" href="{{ route('password.request') }}">
                {{__('Lupa Password?') }}
            </a>
            @endif
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                Ingat Saya
            </label>

            <a href="{{ url('register') }}" class="btn btn-link">Belum Mendaftar? Daftar Sekarang</a>
        </div>
        <button type="submit" class="btn btn-block btn-lg btn-success">
            Log In
        </button>
    </form>

</div>
</div>
</div>
@endsection

@push('script')
<script>
    bootstrapValidate('#email','required:Email Wajib Diisi!');
    bootstrapValidate('#password','required:Password Wajib Diisi!');
</script>
@endpush
