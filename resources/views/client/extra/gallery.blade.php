@extends('client.index')
@section('extra-content')

<div class="col-md-6 col-sm-6 col-xs-12 col-lg-6">

    <h2 style="color: white !important; margin-bottom: 15px;">Galeri Foto Biqinin.com</h2>
    @if (count($gallery)>0)
      <div id="myCarousel" class="carousel slide" data-ride="carousel">

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
          @foreach ($gallery as $row)
              <div class="item {{ $loop->first ? 'active': '' }}">
                <img src="{{ asset('storage/gallery/' . $row->photo) }}" alt="{{ $row->caption }}">
                <div class="carousel-caption">
                  <p>{{ $row->caption }}</p>
                </div>
              </div>
          @endforeach
          
        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    @endif
    
</div>

@endsection
