@extends('client.index')
@section('extra-content')
<div style="margin: 10px 0px; " class="col-md-6 col-sm-6 col-xs-12 col-lg-6">
  <div style="width:100%; padding:20px 30px;border-radius: 6px; background: #fff;box-shadow: -1px 0px 48px -2px rgba(0,0,0,0.44);" class="main-content">
   
    <h3 class="text-center">Mendaftar Untuk Menjadi Member Biqinin.com</h3>
    <p class="text-center">atau <a href="{{ url('login') }}">Login</a> Jika Anda Telah Memiliki Akun</p>
    <form class="form-horizontal" style="margin-top: 30px;" method="POST" action="{{ route('register') }}">
      @csrf
      <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
        <label class="control-label col-sm-3" for="email">E-Mail</label>
        <div class="col-sm-9">
          <input
          placeholder="Alamat E-Mail"
          type="email" 
          name="email"
          class="form-control"
          id="email"
          value="{{ old('email') }}"
          />
          @if ($errors->has('email'))
            <span class="help-block" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
          @endif
        </div>
        
      </div>
      <div class="form-group {{ $errors->has('username') ? 'has-error' : '' }}">
        <label class="control-label col-sm-3" for="username">Username</label>
        <div class="col-sm-9">
          <input
            placeholder="Username Anda"
            type="text"
            name="username"
            class="form-control"
            id="username"
            value="{{ old('username') }}"
          />
          @if ($errors->has('username'))
            <span class="help-block" role="alert">
                <strong>{{ $errors->first('username') }}</strong>
            </span>
          @endif
        </div>
      </div>
      <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
        <label class="control-label col-sm-3" for="password">Password</label>
        <div class="col-sm-9">
          <input
            placeholder="Password"
            type="password"
            name="password"
            class="form-control"
            id="password"
            value="{{ old('password') }}"
          />

          @if ($errors->has('password'))
            <span class="help-block" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
          @endif
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-3" for="password-confirm"> Ulangi Password</label>
        <div class="col-sm-9"> 
          <input id="password-confirm" placeholder="Konfirmasi Password" type="password" class="form-control" name="password_confirmation">
        </div>
      </div>
      <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
        <label class="control-label col-sm-3" for="first_name">Nama Depan</label>
        <div class="col-sm-9">
          <input
            type="text"
            name="first_name"
            class="form-control"
            placeholder="Nama Depan"
            id="first_name"
            value="{{ old('first_name') }}"
          />

          @if ($errors->has('first_name'))
            <span class="help-block" role="alert">
                <strong>{{ $errors->first('first_name') }}</strong>
            </span>
          @endif
        </div>
      </div>
      <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
        <label class="control-label col-sm-3" for="last_name">Nama Belakang</label>
        <div class="col-sm-9">
          <input
            type="text"
            name="last_name"
            class="form-control"
            placeholder="Nama Belakang"
            id="last_name"
            value="{{ old('last_name') }}"
          />

          @if ($errors->has('last_name'))
            <span class="help-block" role="alert">
                <strong>{{ $errors->first('last_name') }}</strong>
            </span>
          @endif
        </div>

      </div>

      <div class="form-group {{ $errors->has('phone_number') ? 'has-error' : '' }}">
        <label class="control-label col-sm-3" for="phone_number">Nomor HP/Whatsapp</label>
        <div class="col-sm-9">
          <input
            type="text"
            class="form-control"
            placeholder="Nomor Handphone/Whatsapp Contoh Format: (628912345678)"
            name="phone_number"
            id="phone_number"
            value="{{ old('phone_number') }}"
          />

          @if ($errors->has('phone_number'))
            <span class="help-block" role="alert">
                <strong>{{ $errors->first('phone_number') }}</strong>
            </span>
          @endif
        </div>

      </div>
      
      <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
        <label class="control-label col-sm-3" for="address">Alamat Lengkap</label>
        <div class="col-sm-9">
          <textarea
            col="10"
            row="8"
            class="form-control"
            placeholder="Alamat Lengkap"
            id="address"
            name="address"
            value="{{ old('address') }}"></textarea>

          @if ($errors->has('address'))
            <span class="help-block" role="alert">
                <strong>{{ $errors->first('address') }}</strong>
            </span>
          @endif
        </div>

      </div>

      <div class="form-group {{ $errors->has('province_id') ? 'has-error' : '' }}">
        <label class="control-label col-sm-3" for="province_id">Provinsi</label>
        <div class="col-sm-9">
          <select
            data-live-search="true"
            name="province_id"
            id="province_id"
            class="form-control">
            <option value="" selected="true" disabled>- Pilih Provinsi -</option>
            @foreach ($provinces as $province)
              <option value="{{ $province->id }}">{{ $province->name }}</option>
            @endforeach
          </select>

          @if ($errors->has('province_id'))
            <span class="help-block" role="alert">
                <strong>{{ $errors->first('province_id') }}</strong>
            </span>
          @endif
        </div>

      </div>

      <div class="form-group {{ $errors->has('city_id') ? 'has-error' : '' }}">
        <label class="control-label col-sm-3" for="province_id">Kota/Kabupaten</label>
        <div class="col-sm-9">
          <select
            data-live-search="true"
            name="city_id"
            id="city_id"
            class="form-control">
            <option value="" selected="true" disabled>- Pilih Kota/Kabupaten -</option>
            
          </select>

          @if ($errors->has('city_id'))
            <span class="help-block" role="alert">
                <strong>{{ $errors->first('city_id') }}</strong>
            </span>
          @endif
        </div>

      </div>
  
      <div class="form-group {{ $errors->has('district_id') ? 'has-error' : '' }}">
        <label class="control-label col-sm-3" for="district_id">Kecamatan</label>
        <div class="col-sm-9">
          <select
            data-live-search="true"
            name="district_id"
            id="district_id"
            class="form-control">
            <option value="" selected="true" disabled>- Pilih Kecamatan -</option>
            
          </select>

          @if ($errors->has('district_id'))
            <span class="help-block" role="alert">
                <strong>{{ $errors->first('district_id') }}</strong>
            </span>
          @endif
        </div>

      </div>

      <div class="form-group {{ $errors->has('village_id') ? 'has-error' : '' }}">
        <label class="control-label col-sm-3" for="village_id">Kelurahan/Desa</label>
        <div class="col-sm-9">
          <select
            data-live-search="true"
            name="village_id"
            id="village_id"
            class="form-control">
            <option value="" selected="true" disabled>- Pilih Kelurahan/Desa -</option>
            
          </select>

          @if ($errors->has('village_id'))
            <span class="help-block" role="alert">
                <strong>{{ $errors->first('village_id') }}</strong>
            </span>
          @endif
        </div>

      </div>

      <div class="form-group {{ $errors->has('postal_code') ? 'has-error' : '' }}">
        <label class="control-label col-sm-3" for="postal_code">Kodepos</label>
        <div class="col-sm-9">
          <input
            type="text"
            class="form-control"
            placeholder="Kodepos"
            name="postal_code"
            id="postal_code"
            value="{{ old('postal_code') }}"
          />

          @if ($errors->has('postal_code'))
            <span class="help-block" role="alert">
                <strong>{{ $errors->first('postal_code') }}</strong>
            </span>
          @endif
        </div>

      </div>
      
      <div class="form-group"> 
        <div class="col-sm-offset-3 col-sm-9">
          <button type="submit" class="btn btn-lg btn-success">Daftar Sekarang</button>
        </div>
      </div>
    </form>
      
    
  </div>
</div>
@endsection

@push('script')
  <script>
    bootstrapValidate('#email','required:Email Wajib Diisi!');
    bootstrapValidate('#password','required:Password Wajib Diisi!');
    bootstrapValidate('[name=address]','required:Alamat Wajib Diisi!');
    bootstrapValidate('[name=phone_number]','required:Nomor HP/Whatsapp Wajib Diisi!');
    bootstrapValidate('[name=first_name]','required:Nama Depan Wajib Diisi!');
    bootstrapValidate('[name=last_name]','required:Nama Belakang Wajib Diisi!');
    bootstrapValidate('[name=postal_code]','required:Kodepos Wajib Diisi!');
    bootstrapValidate('#province_id','required:Provinsi Wajib Dipilih!');
    bootstrapValidate('#city_id','required:Kota/Kabupaten Wajib Dipilih!');
    bootstrapValidate('#district_id','required:Kecamatan Wajib Dipilih!');
    bootstrapValidate('#village_id','required:Kelurahan/Desa Wajib Dipilih!');

    @if (session()->has('error'))
      $(function(){
        swal("Mohon Maaf", `{{ session()->get('error') }}`, "error")
      });
    @endif
  </script>
@endpush