@extends('client.index')
@section('extra-content')
<div class="col-md-6 col-sm-6 col-xs-12 col-lg-6">
  <div class="banner-content">
  	@guest
	    <h3>Bikin Yang Kamu Mau !</h3>
	    <p>Silahkan Mendaftar Untuk Menjadi Member Biqinin.com Atau Login Jika Anda Sudah Memiliki Akun</p>
	    <a href="{{ url('register') }}" id="register-btn" class="text-center banner-btn">Register</a>
	    <a href="{{ url('login') }}" id="login-btn"class="text-center banner-btn">Login</a>
    @endguest

    @auth
    	<h3>Selamat Datang, {{ Auth::user()->username }}</h3>
    	

      @role('admin|superadmin')
        <p>Masuk Ke Halaman Dashboard Untuk Mengelola Pesanan</p>
        <a href="{{ url('admin/dashboard') }}" class="text-center banner-btn">Dashboard Saya</a>
      @endrole

      @role('customer')
        <p>Masuk Ke Halaman Dashboard Untuk Mulai Melakukan pemesanan barang yang kamu mau !</p>
        <a href="{{ url('customer/dashboard') }}" class="text-center banner-btn">Dashboard Saya</a>
      @endrole
	    
    @endauth
  </div><!-- /.banner-content -->
</div><!-- /.col-md-6 -->

@endsection