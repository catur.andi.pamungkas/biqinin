@extends('layouts.client')
@section('title', 'Biqinin.com | Bikin yang Kamu Mau!')

@push('style')
    <link rel="stylesheet" href="{{ asset('client/extra-css/home.css') }}">
@endpush
@section('content')
    <section class="banner-style-six">
        <div class="thm-container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12 col-lg-6">
                    <!-- <img src="img/banner-moc-6-1.png" alt="Awesome Image"/> -->
                    <div class="testimonials-carousel-style-two owl-theme owl-carousel">
                        <div class="item">
                            <div class="single-testimonials-style-two">
                                <img style="width: 460px;" src="{{ asset('client') }}/img/1.png" alt="Awesome Image"/>
                                
                            </div><!-- /.single-testimonials-style-two -->
                        </div><!-- /.item -->
                        <div class="item">
                            <div class="single-testimonials-style-two">
                                <img style="width: 450px;" src="{{ asset('client') }}/img/2.png" alt="Awesome Image"/>
                                
                            </div><!-- /.single-testimonials-style-two -->
                        </div><!-- /.item -->
                        <div class="item">
                            <div class="single-testimonials-style-two">
                                <img style="width: 480px;" src="{{ asset('client') }}/img/3.png" alt="Awesome Image"/>
                                
                            </div><!-- /.single-testimonials-style-two -->
                        </div><!-- /.item -->

                        <div class="item">
                            <div class="single-testimonials-style-two">
                                <img style="width: 450px;" src="{{ asset('client') }}/img/4.png" alt="Awesome Image"/>
                                
                            </div><!-- /.single-testimonials-style-two -->
                        </div><!-- /.item -->

                        
                    </div><!-- /.row -->


                </div><!-- /.col-md-6 -->
                
                {{-- Extra Content Diisi Dengan Konten Register,Login, Text Homepage --}}
                @yield('extra-content')
                
            </div><!-- /.row -->
        </div><!-- /.thm-container -->
    </section><!-- /.banner-style-one -->  
    
</section><!-- /.featured-tab-box -->

@endsection


@push('script')
    <script src="{{ asset('js/axios.js') }}"></script>
    <script>
        $(document).ready(function(){

            $('select').selectpicker();

            $('#province_id').on('change', function(){
                let provinceId = $('#province_id').val();
                axios.get('/province/' + provinceId)
                  .then(function (response) {
                    console.log(response.data);
                    console.log($('#city_id'))
                    $('#city_id').html('');
                    $.each(response.data.cities, (key,val)=>{

                        $('#city_id').append(`<option value="${val.id}">${val.name}</option>`);
                    });
                    $('select').selectpicker('refresh');
                    
                  })
                  .catch(function (error) {
                    console.log(error);
                  });
            });

            $('#city_id').on('change', function(){
                let cityId = $('#city_id').val();
                axios.get('/city/' + cityId)
                  .then(function (response) {
                    $('#district_id').html('');
                    $.each(response.data.districts, (key,val)=>{

                        $('#district_id').append(`<option value="${val.id}">${val.name}</option>`);
                    });
                    $('select').selectpicker('refresh');
                    
                  })
                  .catch(function (error) {
                    console.log(error);
                  });
            });

            $('#district_id').on('change', function(){
                let districtId = $('#district_id').val();
                axios.get('/district/' + districtId)
                  .then(function (response) {
                    $('#village_id').html('');
                    $.each(response.data.villages, (key,val)=>{

                        $('#village_id').append(`<option value="${val.id}">${val.name}</option>`);
                    });
                    $('select').selectpicker('refresh');
                    
                  })
                  .catch(function (error) {
                    console.log(error);
                  });
            });

        });
    </script>
@endpush
