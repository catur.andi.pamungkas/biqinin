<section id="box-login-register" class="featured-tab-box gray-bg">
    <div class="thm-container">
        <ul class="tab-title-box clearfix" role="tablist">
            <li class="single-tab-title text-center active" data-tab-name="login">
                <a href="#login" aria-controls="login" role="tab" data-toggle="tab">
                    <div class="icon-box">
                        {{-- <i class="mixup-icon-business"></i> --}}
                        <i class="fa fa-user"></i>
                    </div><!-- /.icon-box -->
                    <h3>Login</h3>
                </a>
            </li><!-- /.single-tab-title -->
            <li class="single-tab-title text-center" data-tab-name="register">
                <a href="#register" aria-controls="register" role="tab" data-toggle="tab">
                    <div class="icon-box">
                        <i class="mixup-icon-user-checked"></i>
                    </div><!-- /.icon-box -->
                    <h3>Registrasi</h3>
                </a>
            </li><!-- /.single-tab-title -->
                  
        </ul><!-- /.tab-title-box -->
    </div><!-- /.thm-container -->
    <div class="features-tab-content">
        <div class="thm-container">
            <div class="inner gray-bg">
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="login">
                        <div class="single-tab-content">
                            <h3 class="text-center">Masuk Untuk Memulai Pemesanan</h3>
                            <p class="text-center">Gunakan Akun Facebook atau Google Anda Melakukan Login Dengan Instan</p>
                            <div class="main">
                                <div class="row">
                                  <div class="col-sm-6">
                                    <a href="{{ url('auth/facebook') }}" class="mb-10 btn btn-lg btn-primary btn-block"><i class="fa fa-facebook-square"></i> Facebook</a>
                                  </div>
                                  <div class="col-sm-6">
                                    <a href="{{ url('auth/google') }}" class="mb-10 btn btn-lg btn-danger btn-block"><i class="fa fa-google"></i> Google</a>
                                  </div>
                                </div>
                                <div class="login-or">
                                  <hr class="hr-or">
                                  <span class="span-or">or</span>
                                </div>

                                <form role="form" method="POST" action="{{ route('login') }}">
                                  @csrf
                                  <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                    <label for="email">E-Mail</label>
                                    <input
                                      placeholder="Alamat E-Mail"
                                      type="email" class="form-control"
                                      id="email"
                                      value="{{ old('email') }}"
                                    />

                                    @if ($errors->has('email'))
                                      <span class="help-block" role="alert">
                                          <strong>{{ $errors->first('email') }}</strong>
                                      </span>
                                    @endif

                                  </div>
                                  <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                                    
                                    <label for="password">Password</label>
                                    <input
                                      placeholder="Password"
                                      type="password"
                                      class="form-control"
                                      id="password"
                                    />

                                    @if ($errors->has('password'))
                                      <span class="help-block" role="alert">
                                          <strong>{{ $errors->first('password') }}</strong>
                                      </span>
                                    @endif

                                    @if (Route::has('password.request'))
                                      <a class="btn btn-link" href="{{ route('password.request') }}">
                                          {{ __('Lupa Password?') }}
                                      </a>
                                    @endif
                                  </div>
                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                      Ingat Saya </label>
                                  </div>
                                  <button type="submit" class="btn btn btn-primary">
                                    Log In
                                  </button>
                                </form>
                              
                            </div>
                        </div><!-- /.single-tab-content -->
                    </div><!-- /.tab-pane fade in -->
                    <div class="tab-pane fade in" id="register">
                        <div class="single-tab-content">
                          <h3 class="text-center">Mendaftar Menjadi Member Biqinin.com</h3>
                            <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                              @csrf
                              <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                <label class="control-label col-sm-2" for="email">E-Mail</label>
                                <input
                                  placeholder="Alamat E-Mail"
                                  type="email" class="form-control"
                                  id="email"
                                  value="{{ old('email') }}"
                                />
                                @if ($errors->has('email'))
                                  <span class="help-block" role="alert">
                                      <strong>{{ $errors->first('email') }}</strong>
                                  </span>
                                @endif
                              </div>
                              <div class="form-group {{ $errors->has('username') ? 'has-error' : '' }}">
                                <label class="control-label col-sm-2" for="username">Username</label>
                                <input
                                  placeholder="Username Anda"
                                  type="username" class="form-control"
                                  id="username"
                                  value="{{ old('username') }}"
                                />
                                @if ($errors->has('username'))
                                  <span class="help-block" role="alert">
                                      <strong>{{ $errors->first('username') }}</strong>
                                  </span>
                                @endif
                              </div>
                              <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                                <label class="control-label col-sm-2" for="password">Password</label>
                                <input
                                  placeholder="Password"
                                  type="password" class="form-control"
                                  id="password"
                                  value="{{ old('password') }}"
                                />

                                @if ($errors->has('password'))
                                  <span class="help-block" role="alert">
                                      <strong>{{ $errors->first('password') }}</strong>
                                  </span>
                                @endif

                              </div>
                              <div class="form-group">
                                <label class="control-label col-sm-2" for="password-confirm"> Konfirmasi Password:</label>
                                <div class="col-sm-10"> 
                                  <input id="password-confirm" placeholder="Konfirmasi Password" type="password" class="form-control" name="password_confirmation">
                                </div>
                              </div>
                              <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
                                <label class="control-label col-sm-2" for="first_name">Nama Depan</label>
                                <input
                                  type="text"
                                  class="form-control"
                                  placeholder="Nama Depan"
                                  id="first_name"
                                  value="{{ old('first_name') }}"
                                />

                                @if ($errors->has('first_name'))
                                  <span class="help-block" role="alert">
                                      <strong>{{ $errors->first('first_name') }}</strong>
                                  </span>
                                @endif

                              </div>
                              <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
                                <label class="control-label col-sm-2" for="last_name">Nama Belakang</label>
                                <input
                                  type="text"
                                  class="form-control"
                                  placeholder="Nama Belakang"
                                  id="last_name"
                                  value="{{ old('last_name') }}"
                                />

                                @if ($errors->has('last_name'))
                                  <span class="help-block" role="alert">
                                      <strong>{{ $errors->first('last_name') }}</strong>
                                  </span>
                                @endif

                              </div>
                              
                              <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
                                <label class="control-label col-sm-2" for="address">Alamat Lengkap</label>
                                <textarea
                                  col="10"
                                  row="5"
                                  class="form-control"
                                  placeholder="Alamat Lengkap"
                                  id="address"
                                  value="{{ old('address') }}">
                                </textarea>

                                @if ($errors->has('address'))
                                  <span class="help-block" role="alert">
                                      <strong>{{ $errors->first('address') }}</strong>
                                  </span>
                                @endif

                              </div>
                              
                              <div class="form-group"> 
                                <div class="col-sm-offset-2 col-sm-10">
                                  <button type="submit" class="btn btn-primary">Daftar Sekarang</button>
                                </div>
                              </div>
                            </form>
                        </div><!-- /.single-tab-content -->
                    </div><!-- /.tab-pane fade in -->
                    
                </div><!-- /.tab-content -->
            </div><!-- /.inner -->
        </div><!-- /.thm-container -->
    </div><!-- /.features-tab-content -->