<!DOCTYPE html>
<html lang="en">
<head>


    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-135481765-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-135481765-1');
    </script>

     <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-K3V6ZDC');</script>
    <!-- End Google Tag Manager -->


    <meta charset="UTF-8" />
    <meta name="yandex-verification" content="bc1ad901b8821876" />
    <title>@yield('title')</title>

    <meta name="robots" content="index, follow" />

    <meta name="geo.region" content="ID-BTG" />
    <meta name="geo.placename" content="Batang" />
    <meta name="geo.position" content="-6.908496;109.727332" />
    <meta name="ICBM" content="-6.908496, 109.727332" />
    <meta name="google-site-verification" content="MLg2CYe8XKb3U00Vt4xMLOTSTOstqG_bllFVjgPdk_g" />
    <meta name="keywords" content="woodcraft batang, biqinin, bikinin furniture kayu, bikin woodcraft, woodcraft pekalongan,undlabs, web development, biqinin.com, bikinin, jasa buat furniture, jasa buat kayu, jasa buat kayu jati belanda, jasa bikin website"; />

    <!-- mobile responsive meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('client/img/favicon/apple-touch-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('client/img/favicon/apple-touch-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('client/img/favicon/apple-touch-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('client/img/favicon/apple-touch-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('client/img/favicon/apple-touch-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('client/img/favicon/apple-touch-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('client/img/favicon/apple-touch-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('client/img/favicon/apple-touch-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('client/img/favicon/apple-touch-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('client/img/favicon/android-chrome-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('client/img/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('client/img/favicon/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('client/img/favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('client') }}/img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#359538">
    <meta name="msapplication-TileImage" content="{{ asset('client') }}/img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#359538">
    {{-- <link rel="stylesheet" href="{{ asset('client/css/sweetalert.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('client/css/responsive.css')}}"> --}}

    <link rel="stylesheet" href="{{ asset('client/css/style.min.css')}}">
    <style>
        .carousel-caption{
            background-color: rgba(0,0,0,0.6);
            right: 0 !important;
            left: 0 !important;

        }
    </style>
    @stack('style')

</head>
<body class="active-preloader-ovh home-page-6">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K3V6ZDC"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div class="preloader yellow-bg"><div class="spinner"></div></div> <!-- /.preloader -->

<header class="header header-home-six">

    <nav class="navbar navbar-default header-navigation stricky-fixed">
        <div class="thm-container clearfix">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed mixup-icon-menu" data-toggle="collapse" data-target=".main-navigation" aria-expanded="false"> </button>
                <a class="navbar-brand" href="{{ route('home') }}">
                    <img src="{{ asset('client')}}/img/logo.png" alt="Logo Biqinin.com"/>
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse main-navigation mainmenu " id="main-nav-bar">

                <ul class="nav navbar-nav navigation-box">
                    <li class="{{ request()->is('*/') || request()->is('home') ? 'current' :'' }}">
                        <a href="{{ url('/') }}">Home</a>
                    </li>
                    <li class="{{ request()->is('galleries') ? 'current' :'' }}"> <a href="{{ url('galleries') }}">Gallery</a> </li>

                    <li class="{{ request()->is('about-us') ? 'current' :'' }}"> <a href="{{ url('about-us') }}">About Us</a> </li>

                    @guest
                        <li class="{{ request()->is('login') ? 'current' :'' }}"> <a href="{{ route('login') }}">Login</a> </li>
                        <li class="{{ request()->is('register') ? 'current' :'' }}"> <a href="{{ route('register') }}">Register</a> </li>
                    @endguest

                    @auth

                        @role('admin|superadmin')
                            <li><a style="text-decoration: underline;font-weight: bold" href="{{ url('admin/dashboard') }}">Dashboard Saya</a></li>
                        @endrole

                        @role('customer')
                            <li><a style="text-decoration: underline;font-weight: bold" href="{{ url('customer/dashboard') }}">Dashboard Saya</a></li>
                        @endrole

                        <li>
                            <a style="text-decoration: underline;font-weight: bold" class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>

                    @endauth


                </ul>
            </div><!-- /.navbar-collapse -->
            <div class="right-side-box">

                <div class="social">
                    <a target="_blank" href="https://wa.me/{{ get_social('wa') }}" class="fa fa-whatsapp"></a><!--
                    --><a target="_blank" href="{{ get_social('fb') }}" class="fa fa-facebook"></a><!--
                    --><a target="_blank" href="{{ get_social('ig') }}" class="fa fa-instagram"></a>
                </div><!-- /.social -->
            </div><!-- /.right-side-box -->
        </div><!-- /.container -->
    </nav>
</header><!-- /.header -->


{{-- Main Content --}}
@yield('content')
{{-- ./MainContent --}}

<footer class="footer-style-two yellow-bg">
        <div class="footer-bottom text-center">
            <div class="thm-container">
                <p style="color:yellow;">&copy; Copyright <?= date("Y") ?> <a style="color:yellow;" href="#">Biqinin.com - Developt By Undlabs Creative</a></p>
            </div><!-- /.thm-container -->
        </div><!-- /.footer-bottom text-center -->
</footer><!-- /.footer-style-one -->


<div class="scroll-to-top scroll-to-target yellow-bg" data-target="html"><i class="fa fa-angle-up"></i></div>



<script src="{{ asset('client/') }}/js/jquery.js"></script>
<script src="{{ asset('client')}}/js/bootstrap.min.js"></script>


{{-- <script src="{{ asset('client')}}/js/bootstrap-select.min.js"></script>
<script src="{{ asset('client')}}/js/bootstrap-validate.js"></script>
<script src="{{ asset('client')}}/js/owl.carousel.min.js"></script>
<script src="{{ asset('client')}}/js/isotope.js"></script>
<script src="{{ asset('client')}}/js/jquery.magnific-popup.min.js"></script>
<script src="{{ asset('client')}}/js/waypoints.min.js"></script>
<script src="{{ asset('client')}}/js/jquery.counterup.min.js"></script>
<script src="{{ asset('client')}}/js/wow.min.js"></script>
<script src="{{ asset('client')}}/js/jquery.easing.min.js"></script>
<script src="{{ asset('client')}}/js/sweetalert.js"></script>
<script src="{{ asset('client')}}/js/custom.js"></script> --}}

<script src="{{ asset('client') }}/js/app.js"></script>


{{-- Push Scripts --}}
@stack('script')

</body>
</html>
