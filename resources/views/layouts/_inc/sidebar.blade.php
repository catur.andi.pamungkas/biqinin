<div class="polished-sidebar bg-light col-12 col-md-3 col-lg-2 p-0 collapse d-md-inline" id="sidebar-nav">

  <ul class="polished-sidebar-menu ml-0 pt-4 p-0 d-md-block">

    <li class="{{ request()->is('*/dashboard') ? 'active' :'' }}">
      <a href="{{ url('admin/dashboard') }}"><i class="fas fa-tachometer-alt"></i> Dashboard</a>
    </li>

    @role('superadmin')
      <li class="{{ activeRoute('woods')}}">
        <a href="{{ url('admin/woods') }}"><i class="fas fa-box"></i> Kayu</a>
      </li>
      <li class="{{ activeRoute('finishing') }}">
        <a href="{{ url('admin/finishing') }}"><i class="fas fa-brush"></i> Finishing Warna</a>
      </li>
      <li class="{{ activeRoute('banks')}}">
        <a href="{{ url('admin/banks') }}"><i class="fas fa-money-check"></i> Akun Bank</a>
      </li>
      <li class="{{ activeRoute('galleries')}}">
        <a href="{{ url('admin/galleries') }}"><i class="fas fa-images"></i> Gallery Foto</a>
      </li>
    @endrole


    <li class="{{ activeRoute('orders') }}">
      <a href="{{ route('admin.orders') }}"><i class="fas fa-luggage-cart"></i> Pesanan <span class="mt-1 float-right badge badge-danger">{{ $submitted }} baru</span></a>
    </li>

    <li class="{{ activeRoute('chat') }}">
      <a href="{{ url('admin/chat') }}"><i class="fas fa-headset"></i> Chat</a>
    </li>

    <li class="{{ activeRoute('customers') }}">
      <a href="{{ url('admin/customers') }}"><i class="fas fa-users"></i> Daftar Pelanggan</a>
    </li>

    <li class="{{ activeRoute('settings') }}">
      <a href="{{ route('settings') }}"><i class="fas fa-cogs"></i> Pengaturan</a>
    </li>

    <li>
        <a href="{{ route('logout') }}"
           onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
            <i class="fas fa-lock"></i> Logout
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </li>

  </ul>
</div>
