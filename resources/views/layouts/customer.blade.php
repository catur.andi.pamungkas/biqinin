
<!DOCTYPE html>

<html class="no-js" lang="en">
<head>
  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-K3V6ZDC');</script>
  <!-- End Google Tag Manager -->

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-135481765-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-135481765-1');
  </script>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>@yield('title') | Biqinin.com</title>
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- mobile responsive meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('client/img/favicon/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('client/img/favicon/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('client/img/favicon/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('client/img/favicon/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('client/img/favicon/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('client/img/favicon/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('client/img/favicon/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('client/img/favicon/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('client/img/favicon/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('client/img/favicon/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('client/img/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('client/img/favicon/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('client/img/favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('client') }}/img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#359538">
    <meta name="msapplication-TileImage" content="{{ asset('client') }}/img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#359538">

    <link rel="stylesheet" href="{{ asset('client/css/sweetalert.css') }}">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin-customer.css') }}">
  @stack('style')

  <style>
    .grid-highlight {
      padding-top: 1rem;
      padding-bottom: 1rem;
      background-color: #5c6ac4;
      border: 1px solid #202e78;
      color: #fff;
    }

    hr {
      margin: 6rem 0;
    }

    hr+.display-3,
    hr+.display-2+.display-3 {
      margin-bottom: 2rem;
    }

    .polished-sidebar ul.polished-sidebar-menu li.active{
      background: #50b83c;
    }
  </style>
  <script type="text/javascript">
    document.documentElement.className = document.documentElement.className.replace('no-js', 'js') + (document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#BasicStructure", "1.1") ? ' svg' : ' no-svg');
  </script>

</head>

<body>

  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K3V6ZDC"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->

    {{-- Nav Sidebar --}}
    <nav class="navbar bg-success navbar-expand p-0">
      <a class="navbar-brand text-center col-xs-12 col-md-3 col-lg-2 mr-0" href="{{ route('home') }}">
        <img src="{{ asset('client/img/logo.png') }}" alt="">
      </a>
      <button class="btn btn-link text-light d-block d-md-none" data-toggle="collapse" data-target="#sidebar-nav" role="button" >
        <span class="text-light oi oi-menu"></span>
      </button>

      <div class="ml-auto dropdown d-none d-md-block">

        <button class="btn btn-link btn-link-primary dropdown-toggle" id="navbar-dropdown" data-toggle="dropdown">
          <!-- Username -->
          Selamat Datang, {{ Auth::user()->username }}
        </button>
        <div class="dropdown-menu dropdown-menu-right" id="navbar-dropdown">
          <a class="dropdown-item" href="{{ route('logout') }}"
             onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
              <span class="oi oi-power-standby"></span> Logout
          </a>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
          </form>
        </div>
      </div>
    </nav>

  <div class="container-fluid h-100 p-0">
    <div style="min-height: 100%" class="flex-row d-flex align-items-stretch m-0">
        {{-- Sidebar --}}
        <div class="polished-sidebar bg-light col-12 col-md-3 col-lg-2 p-0 collapse d-md-inline" id="sidebar-nav">
          <ul class="polished-sidebar-menu ml-0 pt-4 p-0 d-md-block">
            <li class="{{ request()->is('*/dashboard') ? 'active' :'' }}">
              <a href="{{ url('customer/dashboard') }}"><i class="fas fa-tachometer-alt"></i> Dashboard</a>
            </li>
            <li class="{{ activeRoute('how-to-order')}}">
              <a href="{{ url('customer/how-to-order') }}"><i class="fas fa-list-ul"></i> Cara Pemesanan</a>
            </li>
            <li class="{{ activeRoute('orders') }}">
              <a href="{{ url('customer/orders') }}"><i class="fas fa-cart-plus"></i> Pemesanan</a>
            </li>
            <li class="{{ activeRoute('history') }}">
              <a href="{{ url('customer/history') }}"><i class="fas fa-list-alt"></i> Histori Pemesanan <span class="mt-1 float-right badge badge-danger">{{ auth()->user()->customer->orders()->whereStatus('PROCESS')->count() }}</span></a>
            </li>
            <li class="{{ activeRoute('profile') }}">
              <a href="{{ url('customer/profile') }}"><i class="fas fa-user"></i> Profil</a>
            </li>

            <li class="{{ activeRoute('chat') }}">
              <a href="{{ url('customer/chat') }}"><i class="fas fa-headset"></i> Chat</a>
            </li>

            <li>
                <a href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                 document.getElementById('logout-form-menu').submit();">
                    <i class="fas fa-sign-out-alt"></i> Logout
                </a>

                <form id="logout-form-menu" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>

          </ul>
        </div>

        {{-- Main Content --}}
        @yield('content')

      </div>
  </div>

  <script src="{{ mix('js/app.js') }}"></script>
  <script src="{{ asset('js/plugins.js') }}"></script>
  @stack('script')

</body>

</html>
