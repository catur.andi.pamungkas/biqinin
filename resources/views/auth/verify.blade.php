@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Verifikasi Alamat E-mail Anda') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                    <div class="alert alert-success" role="alert">
                        {{ __('Verifikasi baru telah berhasil dikirimkan ke alamat e-mail anda.') }}
                    </div>
                    @endif

                    {{ __('Sebelum Melanjutkan, Mohon Periksa E-mail anda untuk memverifikasi akun anda.') }}
                    {{ __('Jika anda tidak menerima E-Mail dari kami') }}, <a
                        href="{{ url('email/resend')}}">{{ __('Kirim Ulang Permintaan Verifikasi Baru') }}</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
