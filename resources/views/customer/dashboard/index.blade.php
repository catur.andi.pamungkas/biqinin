
@extends('layouts.customer')

@section('title', 'Dashboard')
@section('content')

@push('style')
  <link rel="stylesheet" href="{{ asset('css/jquery.fancybox.css') }}">
@endpush

<div class="col-lg-10 col-md-9 p-4" id="app">
  <div class="row ">
    <div class="col-md-12 pl-3 pt-2">
        <div class="pl-3">
            <h3>Dashboard</h3>
        </div>
    </div>
  </div>

  <!-- start info box -->
  <div class="row ">
    <div class="col-md-12 pl-3 pt-2">
      <div class="row pl-3">

        <div class="col-lg-6 col-md-6 mb-2 col-sm-6">
          <div class="card border-0 shadow-sm bg-danger text-light">
            <div class="card-body">
                <div class="media">
                    <div class="media-body">
                      <h2 class="fw-bold">{{ \DB::table('orders')->where('customer_id', '=',Auth::user()->customer->id )->count() }}</h2>
                      <h6>Pesanan Saya</h6>
                    </div>
                    <span class="oi oi-cart p-2 fs-9 text-danger-lighter"></span>
                </div>
            </div>
            <div class="card-footer border-0 text-center p-1 bg-danger-lighter">
              <a href="{{ url('customer/orders') }}" class="text-light">
                  Pemesanan <span class="oi oi-arrow-circle-right"></span>
              </a>
            </div>
          </div>
        </div>

        <div class="col-lg-6 col-md-6 mb-2 col-sm-6">
          <div class="card border-0 shadow-sm bg-primary text-light">
            <div class="card-body">
                <div class="media">
                    <div class="media-body">
                      <h2 class="fw-bold">{{ \DB::table('orders')->where('customer_id', '=',Auth::user()->customer->id )->count()}}</h2>
                      <h6>Histori Pemesanan</h6>
                    </div>
                    <span class="oi oi-basket p-2 fs-9 text-indigo-lighter"></span>
                </div>
            </div>
            <div class="card-footer border-0 text-center p-1 bg-primary-lighter">
              <a href="{{ url('customer/history') }}" class="text-light">
                  Histori Pesanan <span class="oi oi-arrow-circle-right"></span>
              </a>
            </div>
          </div>
        </div>

         <!-- start third box -->
              <div class="col-lg-12 mb-2">
                <div class="card">
                  <div class="card-header bg-white pb-1">
                    <h6>Pesanan Terakhir Saya</h6>
                  </div>
                  <div class="card-body px-0 py-0">
                    <div class="table-responsive">
                      <table class="table mb-0">
                        <thead>
                          <tr>
                            <th>Nomor Invoice</th>
                            <th>Tanggal</th>
                            <th>Status</th>
                            <th>Foto</th>
                          </tr>
                        </thead>
                        <tbody>
                          @forelse ($orders as $order)
                            <tr>
                              <td>{{ $order->invoice_number }}</td>
                              <td>{{ tanggalIndonesia($order->created_at) }}</td>
                              <td>
                                  {{-- <div class="badge badge-success text-light">{{ $order->order_status }}</div> --}}
                                  @include('components.formatters.order_table.order_status', [
                                      'order' => $order
                                  ])
                                </td>
                              <td>
                                <a data-fancybox="gallery" href="{{ asset('storage/orders/' . $order->photo) }}"><img style="width: 200px" class="thumbnail" src="{{ asset('storage/orders/' . $order->photo) }}"></a>
                              </td>
                            </tr>
                          @empty
                            <tr>
                              <td colspan="5" class="text-center">Anda Belum Melakukan Pemesanan</td>
                            </tr>
                          @endforelse

                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div class="card-footer d-flex justify-content-between bg-white">

                    <a href="{{ url('customer/history') }}" class="btn btn-link text-primary">Lihat Semua Pesanan</a>
                  </div>
                </div>
              </div>

      </div>
    </div>
  </div>
  <!-- end info box -->
</div>

@endsection

@push('script')
  <script src="{{ asset('js/jquery.fancybox.js') }}"></script>
@endpush
