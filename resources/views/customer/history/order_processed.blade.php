<div class="col-md-12 pl-3 pt-2">
    <div class="pl-3">
        <h3>Pesanan Yang Harus Kamu Bayar</h3>
        <div>
            <p class="text-muted">
          Bayar Pesanan Kamu Sebelum Pesanan dibatalkan oleh sistem
            </p>
        </div>
    </div>

    <div class="pl-3">
        <div class="card">
        <div class="card-header bg-danger text-white text-capitalize">
            Admin Sudah Memproses pesanan kamu nih, tinggal menunggu pembayaran kamu. Selangkah lagi buat menyelesaikan pesanan ini, Silahkan Konfirmasi pembayaran kamu ini dengan menyertakan bukti transfer pembayaran dibawah ini.
        </div>
        <div class="card-body">
          <div class="table-responsive">
              <table class="table table-striped">
                  <thead>
                      <tr>
                          <th>Tanggal</th>
                          <th>Invoice</th>
                          <th>Foto</th>
                          <th>Aksi</th>
                      </tr>
                  </thead>
                  <tbody>
                      @foreach ($processedOrders as $data)
                          <tr>
                              <td>{{ tanggalIndonesia($data->created_at) }}</td>
                              <td>{{ $data->invoice_number }}</td>
                              <td>
                                  <a href="{{asset('storage/orders/' . $data->photo)}}" data-fancybox="images" data-caption="{{$data->invoice_number}}">
                                      <img src="{{asset('storage/orders/' . $data->photo)}}" style="width: 200px" class="thumbnail" alt="{{$data->invoice_number}}" />
                                  </a>
                              </td>
                              <td>
                                  <button type="button" onclick="paymentConfirmation({{$data->id}})" class="btn btn-xs btn-danger">Konfirmasi Pembayaran</button>
                              </td>
                          </tr>
                      @endforeach
                  </tbody>
              </table>
          </div>
        </div>
    </div>
</div>
