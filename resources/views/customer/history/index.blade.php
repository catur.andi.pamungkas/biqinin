@extends('layouts.customer')

@section('title', 'Pesanan')
@push('style')
	<link rel="stylesheet" href="{{ asset('css/jquery.fancybox.css') }}">
@endpush

@section('content')
<div class="col-lg-10 col-md-9 p-4">
    <div class="row h-100">

		@if ($processedOrders->count() > 0)
			@include('customer.history.order_processed')
		@endif


      	<div class="col-md-12 pl-3 pt-2">
          	<div class="pl-3">
              	<h3>Daftar Pesanan Saya</h3>
              	<div>
                  	<p class="text-muted">
                    Menampilkan Data Pesanan Saya
                  	</p>
              	</div>
          	</div>

          	<div class="pl-3">
              	<div class="card">
	  			<div class="card-header bg-primary text-white">
  					Pesanan Saya
	  			</div>
	  			<div class="card-body">
	  				@if (session()->has('success'))
	  					<div class="alert alert-success" role="alert">
						  <h4 class="alert-heading">Pesanan Berhasil !</h4>
						  <p>{{ session()->get('success') }}</p>
						</div>
	  				@endif

					<div class="ml-auto mb-3">
                        @orderfilter([
                            'route' => route('history')
                        ])
                        @endorderfilter
  					</div>

	  				{!! $html->table(['class'=>'display table table-striped dt-responsive nowrap', 'style' => 'width:100%', 'cellspacing' => '0']) !!}
	  			</div>
	  		</div>
      	</div>
    </div>
</div>
@include('admin.orders.detail')
@include('customer.history.paid', compact('banks'))
@endsection

@push('script')
	{!! $html->scripts() !!}
	<script src="{{ asset('js/jquery.mask.min.js') }}"></script>
	<script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
	<script src="{{ asset('plugins/moment/locales.min.js') }}"></script>

	<script>

		$(function() {
			$( '.field-uang' ).mask('000.000.000', {reverse: true});
		});

		function convertToRupiah(angka)
	    {
	        angka = Number(angka);
	        var rupiah = '';
	        var angkarev = angka.toString().split('').reverse().join('');
	        for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
	        return 'Rp. '+rupiah.split('',rupiah.length-1).reverse().join('');
	    }

		function detailOrder(id){
	    	let url = "orders/detail/" + id;
	    	$('#modalDetail').modal('show');
	        let BASE_IMAGE_URL = "{{ asset('storage/orders/') }}";
	    	axios.get(url)
			  .then(function (response) {
			    // handle success
			    console.log(response.data);
			    let data = response.data;
			    $('#modalDetail .modal-title').text(data.invoice_number);
			    let output = `
			    	<tr>
						<td>Nomor Invoice Pesanan</td>
						<td>${data.invoice_number}</td>
					</tr>
					<tr>
						<td>Nama Customer</td>
						<td>${data.customer.first_name} ${data.customer.last_name}</td>
					</tr>
					<tr>
						<td>Tanggal Pemesanan</td>
						<td>${moment(data.created_at).format("LLL")}</td>
					</tr>
					<tr>
						<td>Jenis Kayu</td>
						<td>${data.wood.wood_name}</td>
					</tr>
					<tr>
						<td>Warna Finishing</td>
						<td>${data.finishing.finishing_color_name}</td>
					</tr>
					<tr>
						<td>Banyaknya Jumlah Barang Dipesan</td>
						<td>${data.number_of_orders} Barang</td>
					</tr>
					<tr>
						<td>Catatan Tambahan</td>
						<td>${data.additional_information}</td>
					</tr>
					<tr>
						<td>Foto</td>
						<td><img src="${BASE_IMAGE_URL}/${data.photo}" class="thumbnail img-fluid" /></td>
					</tr>
					<tr>
						<td>Status Pemesanan</td>
						<td><span class="badge badge-success">${data.order_status}</span></td>
					</tr>
					<tr>
						<td>Total Harga Barang</td>
						<td>${convertToRupiah(data.total_price) || 'Belum Diset'}</td>
					</tr>

					<tr>
						<td>Estimasi Pengerjaan</td>
						<td>${data.work_estimation || 'Belum Diset'}</td>
					</tr>
					<tr>
						<td>Kurir Pengiriman</td>
						<td>${data.courier || 'Belum Diset'}</td>
					</tr>
					<tr>
						<td>Biaya Pengiriman</td>
						<td>${convertToRupiah(data.courier_cost) || 'Belum Diset'}</td>
					</tr>
					<tr>
						<td>No. Resi Pengiriman</td>
						<td>${data.airbill_number || 'Belum Diset'}</td>
					</tr>
					<tr>
						<td>Total Keseluruhan</td>
						<td>${convertToRupiah(data.total) || 'Belum Diset'}</td>
					</tr>
			    `;
		    	$('#tableOrderDetail').html(output);
		  })
		  .catch(function (error) {
		    // handle error
		    console.log(error);
		  })
		  .then(function () {
		    // always executed
		  });
		}

		function paymentConfirmation(id) {
			const url = "orders/set-paid";
			const form = $('#paidModal form');
			$('#paidModal').modal('show');
			$('#paidModal .modal-title').text('Konfirmasi Pembayaran');

			$('#paidModal form').on('submit', function(e) {
				e.preventDefault();
				form.find('.invalid-feedback').detach();
			   	form.find(`.form-control`).removeClass('is-invalid');

			   	const formData = new FormData();
	            const transferDocument = $('#transfer_document')[0].files[0];
	            formData.append('id', id);
	            formData.append("bank_id", $('#bank_id').val());
	            formData.append("payment_type", $('[name=payment_type]').val());
	            formData.append("payment_amount", $('[name=payment_amount]').cleanVal());
	            formData.append("payment_description",$('[name=payment_description]').val());
	            formData.append("transfer_document", transferDocument);
                // formData.append('_method', 'PATCH');

	            const settings = { headers: { 'content-type': 'multipart/form-data' } }
				axios({
					method: 'POST',
					url: url,
					data: formData,
					settings
				})
			    .then(function (response) {
			    	if (response.status == 200) {
						$('#paidModal').modal('hide');
				      	swal("Sukses", `Terimakasih atas Konfirmasi pembayaran anda, tunggu Admin kami melakukan pengecekan Konfirmasi Pembayaran anda.`, "success");
				      	$("#dataTableBuilder").DataTable().ajax.reload();
                        setTimeout(()=>window.location.reload(), 2000);
                        // $('#paidModal form')[0].reset();
			    	}

			    })
			    .catch(function (error) {
			      	if(error.response.status == 400){
			     		let errors = error.response.data;
			     		$.each(errors, (key,val) => {
	                      	$(`#${key}`).addClass('is-invalid');
	                      	$(`#${key}`).after(`<div class="d-inline-block invalid-feedback" role="alert">${val.join(", ")}</div>`);
	                    });
			     	} else if(error.response.status == 500){
			     		$('#paidModal').modal('hide');
			     		swal("Error", `Terjadi Kesalahan di Server, Periksa Koneksi Internet Anda`, "error");
			     	}
			    })
			    .finally(()=>{

			    });

			});
		}

		function repeatOrder(id) {
			let url = "orders/reorder";
			swal({
			title: "Anda Yakin ?",
			  text: "Yakin Ingin Pesan Ulang ? Jangan Sampai Lupa Bayar Ya!",
			  type: "warning",
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "Ya, Pesan Ulang!",
			  closeOnConfirm: true,
			  title: "Anda Yakin?",
			  showCancelButton: true,
			  cancelButtonText: "Batalkan",
			  closeOnCancel: true
			},
			function(isConfirm) {
			  if (isConfirm) {
				axios.post(url, {
			      id: id
			    })
			    .then(function (response) {
			      swal("Sukses", `Pesanan Berhasil Diproses Ulang, Tunggu Konfirmasi Lewat SMS dan E-Mail`, "success");
			      $("#dataTableBuilder").DataTable().ajax.reload();
			    })
			    .catch(function (error) {
			      console.log(error);
			    });
			  }
			});
		}
	</script>

	<script src="{{ asset('js/jquery.fancybox.js') }}"></script>
@endpush
