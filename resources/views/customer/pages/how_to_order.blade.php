@extends('layouts.customer')

@section('title', 'Cara Pemesanan')
@section('content')
<div class="col-lg-10 col-md-9 p-4" id="app">
    <div class="row h-100">
      <div class="col-md-12 pl-3 pt-2">
          	<div class="pl-3">
              	<h3>Cara Melakukan Pemesanan di Biqinin.com</h3>
          	</div>

          	<div class="pl-3">
              	<div class="card">
						<div class="card-body">
						{!! $setting->how_to_order !!}
						</div>
	  		</div>
			</div>
			
    </div>
</div>
@endsection

