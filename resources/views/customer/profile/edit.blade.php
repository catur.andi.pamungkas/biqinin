@extends('layouts.customer')

@section('title')
 Profil Saya
@endsection

@push('style')
  <link rel="stylesheet" href="{{ asset('plugins/selectpicker/css/bootstrap-select.min.css') }}">
@endpush

@section('content')
<div class="col-lg-10 col-md-9 p-4" id="app">
  	<div class="row pl-3 pt-2">
  		<div class="col-lg-12 col-sm-12 col-md-12">
        <div class="card">
            <div class="card-header bg-success text-light mb-1">
              <h5>Ubah Profil Saya</h5>
            </div>
            <div class="card-body">
                <form method="post" action="{{ route('profile.update', $profile->id) }}" >
                	@csrf
                  @method('PUT')
                    <div class="form-group row">
                      <label for="first_name" class="col-sm-3 col-form-label">Nama Depan</label>
                      <div class="col-sm-9">
                        <input
                          autofocus
                        	name="first_name"
                        	type="text"
                          value="{{ $profile->first_name }}"
                        	class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }}"/>
                          @if ($errors->has('first_name'))
                            <div class="invalid-feedback has-error-required" role="alert">
                                {{ $errors->first('first_name') }}
                            </div>
                          @endif
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="last_name" class="col-sm-3 col-form-label">Nama Belakang</label>
                      <div class="col-sm-9">
                        <input
                          name="last_name"
                          type="text"
                          value="{{ $profile->last_name }}"
                          class="form-control {{ $errors->has('last_name') ? ' is-invalid' : '' }}"/>
                          @if ($errors->has('last_name'))
                            <div class="invalid-feedback has-error-required" role="alert">
                                {{ $errors->first('last_name') }}
                            </div>
                          @endif
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="first_name" class="col-sm-3 col-form-label">Username</label>
                      <div class="col-sm-9">
                        <input
                          readonly
                          name="username"
                          type="text"
                          value="{{ $profile->user->username }}"
                          class="form-control {{ $errors->has('username') ? ' is-invalid' : '' }}"/>
                          @if ($errors->has('username'))
                            <div class="invalid-feedback has-error-required" role="alert">
                                {{ $errors->first('username') }}
                            </div>
                          @endif
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="office_address" class="col-sm-3 col-form-label">Alamat Lengkap</label>
                      <div class="col-sm-9">
                        <textarea
                          name="address"
                          id="address"
                          rows="8"
                          placeholder="Alamat Lengkap Kantor"
                          class="form-control {{ $errors->has('address') ? ' is-invalid' : '' }}">{{ $profile->address }}</textarea>

                        @if ($errors->has('address'))
                          <div class="invalid-feedback has-error-required" role="alert">
                              {{ $errors->first('address') }}
                          </div>
                        @endif
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="email" class="col-sm-3 col-form-label">Alamat E-Mail</label>
                      <div class="col-sm-9">
                        <input
                          readonly
                          name="email"
                          type="email"
                          value="{{ $profile->user->email }}"
                          class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}"/>
                          @if ($errors->has('email'))
                            <div class="invalid-feedback has-error-required" role="alert">
                                {{ $errors->first('email') }}
                            </div>
                          @endif
                      </div>
                    </div>

                    <div class="form-group row">
                        <label for="phone_number" class="col-sm-3 col-form-label">Nomor HP/Whatsapp</label>
                        <div class="col-sm-9">
                          <input
                            name="phone_number"
                            type="text"
                            value="{{ $profile->phone_number }}"
                            class="form-control {{ $errors->has('phone_number') ? ' is-invalid' : '' }}"/>
                            @if ($errors->has('phone_number'))
                              <div class="invalid-feedback has-error-required" role="alert">
                                  {{ $errors->first('phone_number') }}
                              </div>
                            @endif
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="province_id" class="col-sm-3 col-form-label">Provinsi</label>
                        <div class="col-sm-9">
                            <select data-live-search="true" class="select2 form-control {{ $errors->has('province_id') ? ' is-invalid' : '' }}" name="province_id" id="province_id">
                              <option value="">- Pilih Provinsi -</option>
                              @foreach ($provinces as $row)
                                <option value="{{ $row->id }}">{{ $row->name }}</option>
                              @endforeach
                            </select>
                            @if ($errors->has('province_id'))
                              <div class="invalid-feedback has-error-required" role="alert">
                                  {{ $errors->first('province_id') }}
                              </div>
                            @endif
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="city_id" class="col-sm-3 col-form-label">Kabupaten/Kota</label>
                        <div class="col-sm-9">
                          
                            <select
                            data-live-search="true"
                              class=" form-control {{ $errors->has('city_id') ? ' is-invalid' : '' }}"
                              name="city_id"
                              id="city_id">
                              <option selected="true" disabled value="">- Pilih Kabupaten/Kota -</option>
                            </select>
                            @if ($errors->has('city_id'))
                              <div class="invalid-feedback has-error-required" role="alert">
                                  {{ $errors->first('city_id') }}
                              </div>
                            @endif
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="district_id" class="col-sm-3 col-form-label">Kecamatan</label>
                        <div class="col-sm-9">
                            <select
                            data-live-search="true"
                              class=" form-control {{ $errors->has('district_id') ? ' is-invalid' : '' }}"
                              name="district_id"
                              id="district_id">
                              <option selected="true" disabled value="">- Pilih Kecamatan -</option>
                            </select>
                            @if ($errors->has('district_id'))
                              <div class="invalid-feedback has-error-required" role="alert">
                                  {{ $errors->first('district_id') }}
                              </div>
                            @endif
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="village_id" class="col-sm-3 col-form-label">Kelurahan/Desa</label>
                        <div class="col-sm-9">
                            <select
                            data-live-search="true"
                              class=" form-control {{ $errors->has('village_id') ? ' is-invalid' : '' }}"
                              name="village_id"
                              id="village_id">
                              <option selected="true" disabled value="">- Pilih Kelurahan -</option>
                            </select>
                            @if ($errors->has('village_id'))
                              <div class="invalid-feedback has-error-required" role="alert">
                                  {{ $errors->first('village_id') }}
                              </div>
                            @endif
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="postal_code" class="col-sm-3 col-form-label">Kodepos</label>
                        <div class="col-sm-9">
                          <input
                            name="postal_code"
                            type="text"
                            value="{{ $profile->postal_code }}"
                            class="form-control {{ $errors->has('postal_code') ? ' is-invalid' : '' }}"/>
                            @if ($errors->has('postal_code'))
                              <div class="invalid-feedback has-error-required" role="alert">
                                  {{ $errors->first('postal_code') }}
                              </div>
                            @endif
                        </div>
                      </div>
                    
                    <div class="form-group row">
                    	<div class="col-sm-3"></div>
                      <div class="col-sm-9">
                        <button type="submit" class="btn btn-success">Update Profil Saya</button>
                        <a href="{{ url('customer/profile') }}" class="btn btn-warning">Batal</a>
                      </div>
                    </div>
                  </form>
            </div>
        </div>
      </div>
  	</div>

    <div class="row pl-3 pt-2">
      <div class="col-lg-12 col-sm-12 col-md-12">
        <div class="card">
          <div class="card-header bg-success text-light mb-1">
            Ubah Kata Sandi           
          </div>
          <div class="card-body">
            <form action="{{ route('profile.password') }}" method="post">
              @csrf
                <div class="form-group row">
                  <label for="password" class="col-sm-3 col-form-label">Kata Sandi</label>
                  <div class="col-sm-9">
                    <input
                      name="password"
                      type="password"
                      placeholder="Kata Sandi"
                      class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}"/>
                      @if ($errors->has('password'))
                        <div class="invalid-feedback has-error-required" role="alert">
                            {{ $errors->first('password') }}
                        </div>
                      @endif
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-sm-3"></div>
                  <div class="col-sm-9">
                    <button type="submit" class="btn btn-danger btn-lg">Ubah Kata Sandi</button>
                  </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection

@push('script')
  <script src="{{ asset('plugins/selectpicker/js/bootstrap-select.min.js') }}"></script>
  <script>

    $(document).ready(function(){
      bootstrapValidate('[name=first_name]', 'required:Nama Depan Wajib Diisi');
      bootstrapValidate('[name=last_name]', 'required:Nama Belakang Wajib Diisi');
      bootstrapValidate('[name=address]', 'required:Alamat Wajib Diisi');
      bootstrapValidate('[name=phone_number]', 'required:Nomor Telepon Wajib Diisi');
      bootstrapValidate('[name=postal_code]', 'required:Kodepos Wajib Diisi');
      bootstrapValidate('[name=province_id]', 'required:Provinsi Wajib Diisi');
      bootstrapValidate('[name=city_id]', 'required:Kota Wajib Diisi');
      bootstrapValidate('[name=district_id]', 'required:Kecamatan Wajib Diisi');
      bootstrapValidate('[name=village_id]', 'required:Kelurahan/Desa Wajib Diisi');

            $('select').selectpicker();

            $('#province_id').on('change', function(){
                let provinceId = $('#province_id').val();
                axios.get('/province/' + provinceId)
                  .then(function (response) {
                    console.log(response.data);
                    console.log($('#city_id'))
                    $('#city_id').html('');
                    $.each(response.data.cities, (key,val)=>{

                        $('#city_id').append(`<option value="${val.id}">${val.name}</option>`);
                    });
                    $('select').selectpicker('refresh');
                    
                  })
                  .catch(function (error) {
                    console.log(error);
                  });
            });

            $('#city_id').on('change', function(){
                let cityId = $('#city_id').val();
                axios.get('/city/' + cityId)
                  .then(function (response) {
                    $('#district_id').html('');
                    $.each(response.data.districts, (key,val)=>{

                        $('#district_id').append(`<option value="${val.id}">${val.name}</option>`);
                    });
                    $('select').selectpicker('refresh');
                    
                  })
                  .catch(function (error) {
                    console.log(error);
                  });
            });

            $('#district_id').on('change', function(){
                let districtId = $('#district_id').val();
                axios.get('/district/' + districtId)
                  .then(function (response) {
                    $('#village_id').html('');
                    $.each(response.data.villages, (key,val)=>{

                        $('#village_id').append(`<option value="${val.id}">${val.name}</option>`);
                    });
                    $('select').selectpicker('refresh');
                    
                  })
                  .catch(function (error) {
                    console.log(error);
                  });
            });

        });
   
    @if (session()->has('success'))
      $(function(){
        swal("Sukses", `{{ session()->get('success') }}`, "success")
      });
    @endif
  </script>
@endpush