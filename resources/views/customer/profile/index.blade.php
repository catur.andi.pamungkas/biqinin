@extends('layouts.customer')

@section('title')
  Profil Saya
@endsection


@section('content')
<div class="col-lg-10 col-md-9 p-4" id="app">

  	<div class="row pl-3 pt-2">
  		<div class="col-lg-12 col-sm-12 col-md-12">
            <div class="card">
                <div class="card-header bg-success text-light mb-1">
                  <h5>Profil Saya</h5>
                  <a href="{{ route('profile.edit') }}" class="btn btn-warning pull-right">Ubah Profil Saya</a>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-striped">
                        <tr>
                          <td>Nama Lengkap Customer</td>
                          <td>{{ $profile->first_name }} {{ $profile->last_name }}</td>
                        </tr>
                        <tr>
                          <td>Username</td>
                          <td>{{ $profile->user->username }}</td>
                        </tr>
                        <tr>
                          <td>E-Mail</td>
                          <td>{{ $profile->user->email }}</td>
                        </tr>
                        <tr>
                          <td>No. HP/Whatsapp</td>
                          <td>{{ $profile->phone_number }}</td>
                        </tr>
                        <tr>
                          <td>Alamat</td>
                          <td>{{ $profile->address }}</td>
                        </tr>
                        <tr>
                          <td>Provinsi</td>
                          <td>{{ $profile->province->name }}</td>
                        </tr>
                        <tr>
                          <td>Kabupaten/Kota</td>
                          <td>{{ $profile->city->name }}</td>
                        </tr>
                        <tr>
                          <td>Kecamatan</td>
                          <td>{{ $profile->district->name }}</td>
                        </tr>
                        <tr>
                          <td>Kelurahan/Desa</td>
                          <td>{{ $profile->village->name }}</td>
                        </tr>
                        <tr>
                          <td>Kodepos</td>
                          <td>{{ $profile->postal_code }}</td>
                        </tr>
                        <tr>
                          <td>Mendaftar Sejak</td>
                          <td>{{ $profile->created_at->diffForHumans() }}</td>
                        </tr>
                        <tr>
                          <td>Diverifikasi Sejak</td>
                          <td>{{ \Carbon\Carbon::parse($profile->user->email_verified_at)->diffForHumans() }}</td>
                        </tr>
                      </table>
                    </div>
                </div>
            </div>
  	</div>
</div>
@endsection

@push('script')
  <script>
    @if (session()->has('success'))
      $(function(){
        swal("Sukses", `{{ session()->get('success') }}`, "success")
      });
    @endif
  </script>
@endpush
