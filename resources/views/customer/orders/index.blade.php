@extends('layouts.customer')

@section('title')
Pemesanan
@endsection

@push('style')
<link rel="stylesheet" href="{{ asset('css/filepond.css') }}">
<link rel="stylesheet" href="{{ asset('css/filepond-plugin-image-preview.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/selectpicker/css/bootstrap-select.min.css') }}">
@endpush

@section('content')
<div class="col-lg-10 col-md-9 p-4" id="app">

    <div class="row pl-3 pt-2">
        <div class="col-lg-12 col-sm-12 col-md-12">
            <div class="card">
                <div class="card-header bg-success text-light mb-1">
                    <h5>Lakukan Pemesanan Barang</h5>
                </div>
                <div class="card-body">
                    <div class="alert alert-info mb-4">
                        Belum paham cara pemesanan di Biqinin.com ? Baca Tata Cara Pemesanan <a
                            href="{{ url('customer/how-to-order') }}" class="btn btn-success">Disini</a>
                    </div>
                    <form enctype="multipart/form-data" method="post" action="{{ route('orders.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="photo" class="col-sm-3 col-form-label">Foto</label>
                            <div class="col-sm-9">
                                <input class="my-pond" name="photo" type="file" class="form-control" />

                            </div>

                        </div>

                        <div class="form-group row">
                            <label for="invoice_number" class="col-sm-3 col-form-label">Nomor Invoice Pesanan</label>
                            <div class="col-sm-9">
                                <input readonly="" type="text" name="invoice_number" class="form-control">

                                @if ($errors->has('wood_id'))
                                <div class="invalid-feedback has-error-required" role="alert">
                                    {{ $errors->first('wood_id') }}
                                </div>
                                @endif
                            </div>

                        </div>

                        <div class="form-group row">
                            <label for="wood_id" class="col-sm-3 col-form-label">Jenis Kayu</label>
                            <div class="col-sm-9">
                                <select data-live-search="true" class="form-control" name="wood_id" id="wood_id">
                                    <option value="" selected disabled>- Pilih Jenis Kayu-</option>
                                    @foreach ($woods as $row)
                                    <option value="{{ $row->id }}">{{ $row->wood_name }}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('wood_id'))
                                <div class="invalid-feedback has-error-required" role="alert">
                                    {{ $errors->first('wood_id') }}
                                </div>
                                @endif

                                <div id="woodPreview" class="d-none mt-3">
                                    <img src="" style="width: 75%" class="img-fluid rounded d-block mx-auto" />
                                    <p class="text-center mt-2"></p>
                                </div>
                            </div>

                        </div>

                        <div class="form-group row">
                            <label for="finishing_color_id" class="col-sm-3 col-form-label">Jenis Finishing
                                Warna</label>
                            <div class="col-sm-9">
                                <select data-live-search="true" class="form-control" name="finishing_color_id"
                                    id="finishing_color_id">
                                    <option value="" selected disabled>- Pilih Finishing Warna -</option>
                                </select>

                                @if ($errors->has('finishing_color_id'))
                                <div class="invalid-feedback has-error-required" role="alert">
                                    {{ $errors->first('finishing_color_id') }}
                                </div>
                                @endif

                                <div id="finishingPreview" class="d-none mt-3">
                                     <img src="" style="width: 75%" class="img-fluid rounded d-block mx-auto" />
                                    <p class="text-center mt-2"></p>
                                </div>
                            </div>

                        </div>

                        <div class="form-group row">
                            <label for="number_of_orders" class="col-sm-3 col-form-label">Jumlah Pesanan</label>
                            <div class="col-sm-9">
                                <input value="1" name="number_of_orders" required id="number_of_orders" type="number"
                                    min="1" placeholder="Jumlah Pesanan"
                                    class="form-control {{ $errors->has('number_of_orders') ? ' is-invalid' : '' }}"></textarea>

                                @if ($errors->has('number_of_orders'))
                                <div class="invalid-feedback has-error-required" role="alert">
                                    {{ $errors->first('number_of_orders') }}
                                </div>
                                @endif
                            </div>

                        </div>

                        <div class="form-group row">
                            <label for="additional_information" class="col-sm-3 col-form-label">Keterangan
                                Tambahan</label>
                            <div class="col-sm-9">
                                <textarea name="additional_information" required id="additional_information" rows="6"
                                    placeholder="cth. Ukuran (Panjang, Lebar, Tinggi), Warna, dll."
                                    class="form-control {{ $errors->has('additional_information') ? ' is-invalid' : '' }}"></textarea>

                                @if ($errors->has('additional_information'))
                                <div class="invalid-feedback has-error-required" role="alert">
                                    {{ $errors->first('additional_information') }}
                                </div>
                                @endif
                            </div>

                        </div>

                        <div class="form-group row">
                            <label for="shipping_address" class="col-sm-3 col-form-label">Alamat Pengiriman</label>
                            <div class="col-sm-9">
                                <table class="table table-bordered">
                                    <tr>
                                        <td style="width: 30%;">
                                            <div class="form-check">
                                                <input onclick="toggleAddressOption(false, true)"  class="form-check-input" type="radio" name="shipping_address"
                                                    id="shippingAddressDefault" value="default" checked>
                                                <label class="form-check-label" for="shippingAddressDefault">
                                                    Default
                                                </label>
                                            </div>
                                        </td>
                                        <td>
                                            <textarea readonly placeholder="Masukan Alamat Pengiriman Lain"
                                                name="default_address" id="default_address" rows="4"
                                                class="form-control {{ $errors->has('default_address') ? ' is-invalid' : '' }}">{{ Auth::user()->customer->address }}, {{ Auth::user()->customer->city->name }}, Kecamatan {{ Auth::user()->customer->district->name }} - {{ Auth::user()->customer->province->name }}, Kelurahan {{ Auth::user()->customer->village->name }}, Kodepos {{ Auth::user()->customer->postal_code }}
                                            </textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-check">
                                                <input onclick="toggleAddressOption(true, false)"  class="form-check-input" type="radio" name="shipping_address"
                                                    id="shippingAddressOther" value="other">
                                                <label class="form-check-label" for="shippingAddressOther">
                                                    Lainnya
                                                </label>
                                            </div>
                                        </td>
                                        <td>
                                            <textarea disabled="true" placeholder="Masukan Alamat Pengiriman Lain" name="other_address"
                                                id="other_address" rows="4"
                                                class="form-control {{ $errors->has('other_address') ? ' is-invalid' : '' }}"></textarea>
                                            @if ($errors->has('other_address'))
                                            <div class="invalid-feedback has-error-required" role="alert">
                                                {{ $errors->first('other_address') }}
                                            </div>
                                            @endif
                                        </td>
                                    </tr>
                                </table>
                            </div>

                        </div>

                        <div class="form-group row">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-9">
                                <button type="submit" class="btn btn-success">Submit Order</button>
                                <a href="{{ route('orders.index') }}" class="btn btn-warning">Batal</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @endsection

    @push('script')
    <script src="{{ asset('js/filepond.js') }}"></script>
    <script src="{{ asset('js/filepond-plugin-image-preview.js') }}"></script>
    <script src="{{ asset('js/filepond.jquery.js') }}"></script>
    <script src="{{ asset('plugins/selectpicker/js/bootstrap-select.min.js') }}"></script>
    <script>
        bootstrapValidate('[name=additional_information]','required:Keterangan Tambahan Wajib Diisi!');

    $(function(){
      $('#wood_id, #finishing_color_id').selectpicker();


      $('#wood_id').on('change', function() {
        getFinishingColors();
      });

      $('#finishing_color_id').on('change', function() {
        selectedFinishing();
      });

      // First register any plugins
      generateInvoice();

      $.fn.filepond.registerPlugin(FilePondPluginImagePreview);
      $.fn.filepond.setOptions({
        server: {
          process: {
              url: '/upload',
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          },
          revert: {
            url: '/revert',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          }
        }
      });


      // Turn input element into a pond
      $('.my-pond').filepond();

      $('.my-pond').filepond('required', true);

      // Listen for addfile event
      $('.my-pond').on('FilePond:addfile', function(e) {
          console.log('file added event', e);
      });
    });

    function getFinishingColors() {
      let id = $('#wood_id').val();
      let url = `{{ url('customer/woods/') }}/${id}`;
      let BASE_IMAGE_WOODS = `{{ asset('storage/woods') }}`;
      let finishings = [];

      axios.get(url)
        .then(res=>{
          if(res.status === 200) {
            let data = res.data;
            $('#woodPreview').removeClass('d-none').addClass('d-block');
            $('#woodPreview img').attr('src', `${BASE_IMAGE_WOODS}/${data.photo}`);
            $('#woodPreview p').html(`<b>Preview: </b>Foto ${data.wood_name}`);

            let output = `<option value="" selected disabled>- Pilih Jenis Warna Finishing -</option>`;
            $.each(data.finishings, function(key,val) {
              output += `
                <option value="${val.id}">${val.finishing_color_name}</option>
              `;
            });
            return output;
          }
        })
        .then(output => {
          $('#finishing_color_id').html(output);
          $('#finishing_color_id').selectpicker('refresh');
          $('#finishingPreview').removeClass('d-block').addClass('d-none');

        })
        .catch(err => console.log(err));
    }

    function selectedFinishing() {
      let id = $('#finishing_color_id').val();
      let url = `{{ url('customer/finishing/') }}/${id}`;
      let BASE_IMAGE_FINISHINGS = `{{ asset('storage/finishings') }}`;

      axios.get(url)
        .then(res=>{
          if(res.status === 200) {
            let data = res.data;
            console.log(data);
            $('#finishingPreview').removeClass('d-none').addClass('d-block');
            $('#finishingPreview img').attr('src', `${BASE_IMAGE_FINISHINGS}/${data.photo}`);
            $('#finishingPreview p').html(`<b>Preview: </b> Foto ${data.finishing_color_name}`);
            $('#finishing_color_id').selectpicker('show');
          }
        })
        .catch(err => console.log(err));
    }


    function generateInvoice() {
      var date = new Date().getTime();

      var text = "";
      var possible = "0123456789";

      for (var i = 0; i < 4; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

      let output = 'INV-' + text + date;

      $('input[name=invoice_number]').val(output);
    }

    function toggleAddressOption(defaultAddress, shippingAddress) {
        document.getElementById('default_address').disabled = defaultAddress;
        document.getElementById('other_address').disabled = shippingAddress;
    }

    </script>
    @endpush


