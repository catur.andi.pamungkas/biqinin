
@isset ($order)
    <button type="button" onclick="detailOrder({{$order->id}})" class="btn btn-xs btn-success">Detail</button>

	@if ($order->order_status == "SUBMIT")
		<button type="button" onclick="processOrder({{$order->id}})" class="btn btn-xs btn-warning">Proses</button>
	@endif

	@if ($order->order_status == "CANCEL")
		<button type="button" onclick="setToPaidOrder({{$order->id}})" class="btn btn-xs btn-info">Set Pesanan Dibayar</button>
    @endif

    @if ($order->order_status == "PAID")
		<button type="button" onclick="setPaymentComplete({{$order->id}})" class="btn btn-xs btn-warning">Konfirmasi Pembayaran Selesai</button>
	@endif

	@if ($order->order_status == "PAID_CONFIRM")
		<button type="button" onclick="changeAirbillNumber({{$order->id}})" class="btn btn-xs btn-info">Ubah No. Resi</button>

		<a target="_blank" href="{{ route('orders.invoice', $order->id) }}" class="btn btn-xs btn-primary">Cetak Invoice</a>
	@endif

	@if ($order->order_status == "PROCESS")
		<button type="button" onclick="setToPaidOrder({{$order->id}})" class="btn btn-xs btn-info">Set Pesanan Dibayar</button>
	@endif


	<button type="button" onclick="cancelOrder({{$order->id}})" class="btn btn-xs btn-danger">Cancel</button>

@endisset
