
@if ($file != '' || $file != null)
	<a href="{{$file}}" data-fancybox="images" data-caption="{{$caption}}">
		<img src="{{$file}}" style="width: 200px" class="thumbnail" alt="{{$caption}}" />
	</a>
@else
	Belum Ada Foto
@endif
