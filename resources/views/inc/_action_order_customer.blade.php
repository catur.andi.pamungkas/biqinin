
@isset ($order)
    <button type="button" onclick="detailOrder({{$order->id}})" class="btn btn-xs btn-success">Detail Pesanan</button>

	@if ($order->order_status == "CANCEL")
		<button type="button" onclick="repeatOrder({{$order->id}})" class="btn btn-xs btn-danger">Pesan Ulang</button>
	@endif

@endisset
