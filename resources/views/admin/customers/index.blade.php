@extends('layouts.admin')

@section('title', 'Jenis Kayu')
@section('content')
<div class="col-lg-10 col-md-9 p-4">
    <div class="row h-100">
      <div class="col-md-12 pl-3 pt-2">
          	<div class="pl-3">
              	<h3>Data Member</h3>
              	<div>
                  	<p class="text-muted">
                    Menampilkan Data Member Terdaftar
                  	</p>
              	</div>
          	</div>

          	<div class="pl-3">
              	<div class="card">
	  			<div class="card-body">
					<div class="ml-auto mb-3">
  						<form action="{{ route('customers.index') }}">
					        <div class="input-group">
					        	<input
						          value="{{Request::get('keyword')}}"
						          name="keyword"
						          class="form-control"
						          type="text"
						          placeholder="Cari Berdasarkan Email atau Username Member"/>
						          <div class="input-group-append">
					                <input
					                type="submit"
					                value="Cari"
					                class="btn btn-primary">
					            </div>
						    </div>
						    
						</form>
  					</div>
			    	<div class="table-responsive">
		  				<table class="table table-striped" id="data-table">
							<thead>
								<th>No.</th>
								<th>Email</th>
								<th>Member Sejak</th>
								<th>Nama Member</th>
								<th>Kota</th>
								<th>Action</th>
							</thead>
							<tbody>
								@if (count($customers)>0)
									@php
										$no = $customers->firstItem();
									@endphp
									@foreach ($customers as $customer)
										<tr>
											<td>{{ $no++}}</td>
											<td>{{ $customer->user->email }}</td>
											<td>{{ $customer->created_at->diffForHumans() }}</td>
											<td>{{ $customer->first_name}} {{ $customer->last_name }}</td>
											<td>{{ $customer->city->name }}</td>
											<td>
												
												<button onclick="openDetail({{ $customer->id }})" class="d-inline btn btn-success"><span class="oi oi-eye"></span></button>
											</td>
										</tr>
									@endforeach
								@else
									<tr>
										<td colspan="6" class="text-center">Maaf, Data Kosong</td>
									</tr>
								@endif
								
								
							</tbody>
						</table>
						{{ $customers->links() }}
					</div>
	  			</div>
	  		</div>
      </div>
    </div>
</div>

<div class="modal fade" id="modalDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalTitle"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table table-bordered" id="tableCustomerDetail">
        	
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>
@endsection

@push('script')
<script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('plugins/moment/locales.min.js') }}"></script>
<script>
	
	moment.locale('id');

	function openDetail(id){
    	let url = `customers/` + id;
    	$('#modalDetail').modal('show');
        
    	axios.get(url)
		  .then(function (response) {
		    // handle success
		    console.log(response.data);
		    let data = response.data;
		    $('#modalDetail .modal-title').text('Detail Customer ' + data.first_name + ' ' + data.last_name);

		    let output = `
				<tr>
					<td>Nama Lengkap Customer</td>
					<td>${data.first_name} ${data.last_name}</td>
				</tr>
				<tr>
					<td>Username</td>
					<td>${data.user.username}</td>
				</tr>
				<tr>
					<td>E-Mail</td>
					<td>${data.user.email}</td>
				</tr>
				<tr>
					<td>No. HP/Whatsapp</td>
					<td>${data.phone_number}</td>
				</tr>
				<tr>
					<td>Alamat</td>
					<td>${data.address}</td>
				</tr>
				<tr>
					<td>Provinsi</td>
					<td>${data.province.name}</td>
				</tr>
				<tr>
					<td>Kabupaten/Kota</td>
					<td>${data.city.name}</td>
				</tr>
				<tr>
					<td>Kecamatan</td>
					<td>${data.district.name}</td>
				</tr>
				<tr>
					<td>Kelurahan/Desa</td>
					<td>${data.village.name}</td>
				</tr>
				<tr>
					<td>Kodepos</td>
					<td>${data.postal_code}</td>
				</tr>
				<tr>
					<td>Mendaftar Sejak</td>
					<td>${moment(data.created_at).format("LLL")}</td>
				</tr>
				<tr>
					<td>Diverifikasi Sejak</td>
					<td>${moment(data.user.email_verified_at).format("LLL")}</td>
				</tr>
		    `;

		    $('#tableCustomerDetail').html(output);
		  })
		  .catch(function (error) {
		    // handle error
		    console.log(error);
		  })
		  .then(function () {
		    // always executed
		  });
    }
    
	$(function(){
		$('.btn-delete').on('click', ()=>{
			let id = $('.btn-delete').data("id");
			let route = `finishing/${id}`;
			window.fn.delete(id,route);
		});
	});

    @if (session()->has('success'))
      $(function(){
        swal("Sukses", `{{ session()->get('success') }}`, "success")
      });
    @endif

    
</script>
@endpush