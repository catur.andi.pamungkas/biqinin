@extends('layouts.admin')

@section('title')
  Pengaturan Website
@endsection

@push('style')
  <link rel="stylesheet" href="{{ asset('css/filepond.css') }}">
  <link rel="stylesheet" href="{{ asset('css/filepond-plugin-image-preview.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/summernote/summernote-bs4.css') }}">
@endpush

@section('content')
<div class="col-lg-10 col-md-9 p-4">
  	<div class="row pl-3 pt-2">

      @role('superadmin')
    		<div class="col-lg-12 col-sm-12 col-md-12">
          <div class="card">
              <div class="card-header bg-primary text-light mb-1">
                <h5>Ubah Pengaturan Website</h5>
              </div>
              <div class="card-body">
                  <form method="post" action="{{ route('settings.update', $setting->id) }}" >
                  	@csrf
                    @method('PUT')
                      <div class="form-group row">
                        <label for="site_name" class="col-sm-3 col-form-label">Nama Website</label>
                        <div class="col-sm-9">
                          <input
                            autofocus
                          	name="site_name"
                          	type="text"
                            value="{{ $setting->site_name }}"
                          	class="form-control {{ $errors->has('site_name') ? ' is-invalid' : '' }}"/>
                            @if ($errors->has('site_name'))
                              <div class="invalid-feedback has-error-required" role="alert">
                                  {{ $errors->first('site_name') }}
                              </div>
                            @endif
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="office_address" class="col-sm-3 col-form-label">Alamat Lengkap Kantor</label>
                        <div class="col-sm-9">
                          <textarea
                            name="office_address"
                            id="office_address"
                            rows="8"
                            placeholder="Alamat Lengkap Kantor"
                            class="form-control {{ $errors->has('office_address') ? ' is-invalid' : '' }}">{{ $setting->office_address }}</textarea>

                          @if ($errors->has('office_address'))
                            <div class="invalid-feedback has-error-required" role="alert">
                                {{ $errors->first('office_address') }}
                            </div>
                          @endif
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="facebook" class="col-sm-3 col-form-label">Facebook</label>
                        <div class="col-sm-9">
                          <input
                            name="facebook"
                            type="url"
                            value="{{ $setting->facebook }}"
                            class="form-control {{ $errors->has('facebook') ? ' is-invalid' : '' }}"/>
                            @if ($errors->has('facebook'))
                              <div class="invalid-feedback has-error-required" role="alert">
                                  {{ $errors->first('facebook') }}
                              </div>
                            @endif
                        </div>
                      </div>

                      <div class="form-group row">
                          <label for="instagram" class="col-sm-3 col-form-label">Instagram</label>
                          <div class="col-sm-9">
                            <input
                              name="instagram"
                              type="url"
                              value="{{ $setting->instagram }}"
                              class="form-control {{ $errors->has('instagram') ? ' is-invalid' : '' }}"/>
                              @if ($errors->has('instagram'))
                                <div class="invalid-feedback has-error-required" role="alert">
                                    {{ $errors->first('instagram') }}
                                </div>
                              @endif
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="whatsapp" class="col-sm-3 col-form-label">No. Whatsapp</label>
                          <div class="col-sm-9">
                            <input
                              name="whatsapp"
                              type="text"
                              value="{{ $setting->whatsapp }}"
                              class="form-control {{ $errors->has('whatsapp') ? ' is-invalid' : '' }}"/>
                              @if ($errors->has('whatsapp'))
                                <div class="invalid-feedback has-error-required" role="alert">
                                    {{ $errors->first('whatsapp') }}
                                </div>
                              @endif
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="phone_number" class="col-sm-3 col-form-label">No. Handphone</label>
                          <div class="col-sm-9">
                            <input
                              name="phone_number"
                              type="text"
                              value="{{ $setting->phone_number }}"
                              class="form-control {{ $errors->has('phone_number') ? ' is-invalid' : '' }}"/>
                              @if ($errors->has('phone_number'))
                                <div class="invalid-feedback has-error-required" role="alert">
                                    {{ $errors->first('phone_number') }}
                                </div>
                              @endif
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="email" class="col-sm-3 col-form-label">Alamat E-Mail</label>
                          <div class="col-sm-9">
                            <input
                              name="email"
                              type="email"
                              value="{{ $setting->email }}"
                              class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}"/>
                              @if ($errors->has('email'))
                                <div class="invalid-feedback has-error-required" role="alert">
                                    {{ $errors->first('email') }}
                                </div>
                              @endif
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="owner_name" class="col-sm-3 col-form-label">Nama Pemilik</label>
                          <div class="col-sm-9">
                            <input
                              name="owner_name"
                              type="text"
                              value="{{ $setting->owner_name }}"
                              class="form-control {{ $errors->has('owner_name') ? ' is-invalid' : '' }}"/>
                              @if ($errors->has('owner_name'))
                                <div class="invalid-feedback has-error-required" role="alert">
                                    {{ $errors->first('owner_name') }}
                                </div>
                              @endif
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="npwp" class="col-sm-3 col-form-label">Nomor NPWP</label>
                          <div class="col-sm-9">
                            <input
                              name="npwp"
                              type="text"
                              value="{{ $setting->npwp }}"
                              class="form-control {{ $errors->has('npwp') ? ' is-invalid' : '' }}"/>
                              @if ($errors->has('npwp'))
                                <div class="invalid-feedback has-error-required" role="alert">
                                    {{ $errors->first('npwp') }}
                                </div>
                              @endif
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="about_company" class="col-sm-3 col-form-label">Tentang Perusahaan</label>
                          <div class="col-sm-9">
                            <textarea
                              name="about_company"
                              id="about_company"
                              rows="8"
                              placeholder="Tentang Perusahaan"
                              class="form-control {{ $errors->has('about_company') ? ' is-invalid' : '' }}">{{ $setting->about_company }}</textarea>

                            @if ($errors->has('about_company'))
                              <div class="invalid-feedback has-error-required" role="alert">
                                  {{ $errors->first('about_company') }}
                              </div>
                            @endif
                          </div>
                        </div>

                        <div class="form-group row">
                          <label for="how_to_order" class="col-sm-3 col-form-label">Cara Pemesanan</label>
                          <div class="col-sm-9">
                            <textarea
                              name="how_to_order"
                              id="how_to_order"
                              class="form-control {{ $errors->has('how_to_order') ? ' is-invalid' : '' }}">{{ $setting->how_to_order }}</textarea>

                            @if ($errors->has('how_to_order'))
                              <div class="invalid-feedback has-error-required" role="alert">
                                  {{ $errors->first('how_to_order') }}
                              </div>
                            @endif
                          </div>
                        </div>

                        <div class="form-group row">
                            <label for="how_to_make_payment" class="col-sm-3 col-form-label">Cara Melakukan Pembayaran</label>
                            <div class="col-sm-9">
                              <textarea
                                name="how_to_make_payment"
                                id="how_to_make_payment"
                                class="form-control {{ $errors->has('how_to_make_payment') ? ' is-invalid' : '' }}">{{ $setting->how_to_make_payment }}</textarea>

                              @if ($errors->has('how_to_make_payment'))
                                <div class="invalid-feedback has-error-required" role="alert">
                                    {{ $errors->first('how_to_make_payment') }}
                                </div>
                              @endif
                            </div>
                          </div>

                      <div class="form-group row">
                      	<div class="col-sm-3"></div>
                        <div class="col-sm-9">
                          <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                      </div>
                    </form>
              </div>
          </div>
        </div>
      @endrole

      <div class="col-lg-12 col-sm-12 col-md-12">
        <div class="card">
          <div class="card-header bg-primary text-light mb-1">
            <h5>Ubah Kata Sandi Pengguna</h5>
          </div>
          <div class="card-body">
            <form action="{{ route('settings.password') }}" method="post">
              @csrf
                <div class="form-group row">
                  <label for="password" class="col-sm-3 col-form-label">Kata Sandi</label>
                  <div class="col-sm-9">
                    <input
                      name="password"
                      type="password"
                      placeholder="Kata Sandi Untuk Login"
                      class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}"/>
                      @if ($errors->has('password'))
                        <div class="invalid-feedback has-error-required" role="alert">
                            {{ $errors->first('password') }}
                        </div>
                      @endif
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-sm-3"></div>
                  <div class="col-sm-9">
                    <button type="submit" class="btn btn-primary btn-lg">Ubah Kata Sandi</button>
                  </div>
                </div>
            </form>
          </div>
        </div>
      </div>
  	</div>
</div>
@endsection

@push('script')
  <script src="{{ asset('plugins/summernote/summernote-bs4.min.js') }}"></script>
  <script>
    $(function(){
      $('#how_to_order').summernote({
        placeholder: 'Cara Penggunaan Aplikasi',
        tabsize: 2,
        height: 250
      });

      $('#how_to_make_payment').summernote({
        placeholder: 'Cara Melakukan Pembayaran',
        tabsize: 2,
        height: 200
      });
    });

    @if (session()->has('success'))
      $(function(){
        swal("Sukses", `{{ session()->get('success') }}`, "success")
      });
    @endif
  </script>
@endpush
