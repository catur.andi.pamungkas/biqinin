@extends('layouts.admin')

@section('title', 'Akun Bank')
@section('content')
<div class="col-lg-10 col-md-9 p-4">
    <div class="row h-100">
      <div class="col-md-12 pl-3 pt-2">
          	<div class="pl-3">
              	<h3>Data Akun Bank</h3>
              	<div>
                  	<p class="text-muted">
                    Menampilkan Data Akun Bank
                  	</p>
              	</div>
          	</div>

          	<div class="pl-3">
              	<div class="card">
	  			<div class="card-header">
  					<a href="{{ route('banks.create') }}" class="btn btn-primary"><span class="oi io-plus"></span> Tambah Akun Bank</a>
	  			</div>
	  			<div class="card-body">
					
					<div class="ml-auto mb-3">
  						<form action="{{ route('banks.index') }}">
					        <div class="input-group">
					        	<input
						          value="{{Request::get('keyword')}}"
						          name="keyword"
						          class="form-control"
						          type="text"
						          placeholder="Cari Berdasarkan Nama Bank"/>
						          <div class="input-group-append">
					                <input
					                type="submit"
					                value="Cari"
					                class="btn btn-primary">
					            </div>
						    </div>
						    
						</form>
  					</div>
			    	<div class="table-responsive">
		  				<table class="table table-striped" id="data-table">
							<thead>
								<th>No.</th>
								<th>Nama Bank</th>
								<th>Nomor Rekening Bank</th>
								<th>Atas Nama</th>
								<th>Action</th>
							</thead>
							<tbody>
								@if (count($banks)>0)
									@php
										$no = $banks->firstItem();
									@endphp
									@foreach ($banks as $bank)
										<tr>
											<td>{{ $no++}}</td>
											<td>{{ $bank->bank_name }}</td>
											<td>{{ $bank->account_number }}</td>
											<td>{{ $bank->owner_name }}</td>
											<td>
												<a href="{{ route('banks.edit', $bank->id) }}" class="btn btn-info"><span class="oi oi-pencil"></span></a>
												<a onclick="removeData({{ $bank->id }})" href="javascript:void(0)" class="btn btn-delete btn-danger"><span class="oi oi-trash"></span></a>
											</td>
										</tr>
									@endforeach
								@else
									<tr>
										<td colspan="5" class="text-center">Maaf, Data Kosong</td>
									</tr>
								@endif
								
								
							</tbody>
						</table>
						{{ $banks->links() }}
					</div>
	  			</div>
	  		</div>
      </div>
    </div>
</div>
@endsection

@push('script')
	<script>
    
	function removeData(id){
		let route = `banks/${id}`;
		window.fn.delete(id,route);
	}

    @if (session()->has('success'))
      $(function(){
        swal("Sukses", `{{ session()->get('success') }}`, "success")
      });
    @endif
</script>
@endpush