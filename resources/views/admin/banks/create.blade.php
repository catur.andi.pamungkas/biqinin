@extends('layouts.admin')

@section('title')
  Tambah Akun Bank
@endsection
@section('content')
<div class="col-lg-10 col-md-9 p-4">

  	<div class="row pl-3 pt-2">
  		<div class="col-lg-12 col-sm-12 col-md-12">
            <div class="card">
                <div class="card-header bg-primary text-light mb-1">
                  <h5>Tambah Akun Bank</h5>
                </div>
                <div class="card-body">
                    <form method="post" action="{{ route('banks.store') }}" >
                    	@csrf
                        
                        <div class="form-group row">
                          <label for="bank_name" class="col-sm-3 col-form-label">Nama Bank</label>
                          <div class="col-sm-9">
                            <input
                            	name="bank_name"
                            	type="text"
                            	class="form-control {{ $errors->has('bank_name') ? ' is-invalid' : '' }}"
                              value="{{ old('bank_name') }}"
                            	placeholder="Nama Bank">
                              @if ($errors->has('bank_name'))
                                <div class="invalid-feedback has-error-required" role="alert">
                                    {{ $errors->first('bank_name') }}
                                </div>
                              @endif
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="account_number" class="col-sm-3 col-form-label">Nomor Rekening Bank</label>
                          <div class="col-sm-9">
                            <input
                              name="account_number"
                              type="text"
                              class="form-control {{ $errors->has('account_number') ? ' is-invalid' : '' }}"
                              value="{{ old('account_number') }}"
                              placeholder="Nomor Rekening Bank">
                              @if ($errors->has('account_number'))
                                <div class="invalid-feedback has-error-required" role="alert">
                                    {{ $errors->first('account_number') }}
                                </div>
                              @endif
                          </div>
                        </div>

                        <div class="form-group row">
                          <label for="owner_name" class="col-sm-3 col-form-label">Nama Pemilik</label>
                          <div class="col-sm-9">
                            <input
                              name="owner_name"
                              type="text"
                              class="form-control {{ $errors->has('owner_name') ? ' is-invalid' : '' }}"
                              value="{{ old('owner_name') }}"
                              placeholder="Nomor Rekening Bank">
                              @if ($errors->has('owner_name'))
                                <div class="invalid-feedback has-error-required" role="alert">
                                    {{ $errors->first('owner_name') }}
                                </div>
                              @endif
                          </div>
                        </div>
                        
                        <div class="form-group row">
                        	<div class="col-sm-3"></div>
                          <div class="col-sm-9">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <a href="{{ route('banks.index') }}" class="btn btn-warning">Batal</a>
                          </div>
                        </div>
                      </form>
                </div>
            </div>
  	</div>
</div>
@endsection

@push('script')
  <script>
    bootstrapValidate('[name=account_number]','required:Nomor Rekening Wajib Diisi!');
    bootstrapValidate('[name=bank_name]','required:Nama Bank Wajib Diisi!');
    bootstrapValidate('[name=owner_name]','required:Nama Pemilik Rekening Wajib Diisi!');
  </script>
@endpush