@extends('layouts.admin')

@section('title', 'Finishing Warna')


@push('style')
	<link rel="stylesheet" href="{{ asset('css/jquery.fancybox.css') }}">
@endpush

@section('content')
<div class="col-lg-10 col-md-9 p-4">
    <div class="row h-100">
      <div class="col-md-12 pl-3 pt-2">
          	<div class="pl-3">
              	<h3>Data Jenis Finishing Warna</h3>
              	<div>
                  	<p class="text-muted">
                    Menampilkan Data Jenis Finishing Warna
                  	</p>
              	</div>
          	</div>

          	<div class="pl-3">
              	<div class="card">
	  			<div class="card-header">
  					<a href="{{ route('finishing.create') }}" class="btn btn-primary"><span class="oi io-plus"></span> Tambah Finishing Warna</a>
	  			</div>
	  			<div class="card-body">
					
					<div class="ml-auto mb-3">
  						<form action="{{ route('finishing.index') }}">
					        <div class="input-group">
					        	<input
						          value="{{Request::get('keyword')}}"
						          name="keyword"
						          class="form-control"
						          type="text"
						          placeholder="Cari Berdasarkan Nama Finishing Warna"/>
						          <div class="input-group-append">
					                <input
					                type="submit"
					                value="Cari"
					                class="btn btn-primary">
					            </div>
						    </div>
						    
						</form>
  					</div>
			    	
	  				<table class="table table-striped" id="data-table">
						<thead>
							<th>No.</th>
							<th>Nama Finishing Warna</th>
							<th>Untuk Jenis Kayu</th>
							<th>Foto</th>
							<th>Action</th>
						</thead>
						<tbody>
							@if (count($finishings)>0)
								@php
									$no = $finishings->firstItem();
								@endphp
								@foreach ($finishings as $finishing)
									<tr>
										<td>{{ $no++}}</td>
										<td>{{ $finishing->finishing_color_name }}</td>
										<td>
											@foreach ($finishing->woods as $wood)
												<li>{{ $wood->wood_name }}</li>
											@endforeach
										</td>
										<td>
											<a data-fancybox="images" href="{{ asset('storage/finishings/' . $finishing->photo) }}"><img style="width: 100px" class="thumbnail" src="{{ asset('storage/finishings/' . $finishing->photo) }}"></a>
										</td>
										<td>
											<a href="{{ route('finishing.edit', $finishing->id) }}" class="btn btn-info"><span class="oi oi-pencil"></span></a>
											<a onclick="removeData({{ $finishing->id }})" href="javascript:void(0)" class="btn btn-delete btn-danger"><span class="oi oi-trash"></span></a>
										</td>
									</tr>
								@endforeach
							@else
								<tr>
									<td colspan="5" class="text-center">Maaf, Data Kosong</td>
								</tr>
							@endif
							
							
						</tbody>
					</table>
					{{ $finishings->links() }}
	  			</div>
	  		</div>
      </div>
    </div>
</div>
@endsection

@push('script')
	<script src="{{ asset('js/jquery.fancybox.js') }}"></script>
	<script>
    
	function removeData(id){
		let route = `finishing/${id}`;
		window.fn.delete(id,route);
	}

    @if (session()->has('success'))
      $(function(){
        swal("Sukses", `{{ session()->get('success') }}`, "success")
      });
    @endif
</script>
@endpush