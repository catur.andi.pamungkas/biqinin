@extends('layouts.admin')

@section('title')
  Ubah Jenis Warna Finishing
@endsection

@push('style')
  <link rel="stylesheet" href="{{ asset('css/filepond.css') }}">
  <link rel="stylesheet" href="{{ asset('css/filepond-plugin-image-preview.css') }}">
   <link rel="stylesheet" href="{{ asset('plugins/selectpicker/css/bootstrap-select.min.css') }}">
@endpush

@section('content')
<div class="col-lg-10 col-md-9 p-4">

  	<div class="row pl-3 pt-2">
  		<div class="col-lg-12 col-sm-12 col-md-12">
            <div class="card">
                <div class="card-header bg-primary text-light mb-1">
                  <h5>Ubah Jenis Warna Finishing</h5>
                </div>
                <div class="card-body">
                    <form method="post" enctype="multipart/form-data" action="{{ route('finishing.update', $finishing->id) }}" >
                    	@csrf
                        @method('PUT')
                        <div class="form-group row">
                          <label for="finishing_color_name" class="col-sm-3 col-form-label">Nama Warna Finishing</label>
                          <div class="col-sm-9">
                            <input
                            	name="finishing_color_name"
                            	type="text"
                            	class="form-control{{ $errors->has('finishing_color_name') ? ' is-invalid' : '' }}"
                              value="{{ $finishing->finishing_color_name }}"
                            	placeholder="Nama Jenis Warna Finishing">
                              @if ($errors->has('finishing_color_name'))
                                <div class="invalid-feedback has-error-required" role="alert">
                                    {{ $errors->first('finishing_color_name') }}
                                </div>
                              @endif
                          </div>
                        </div>

                        <div class="form-group row">
                          <label for="wood_finishings" class="col-sm-3 col-form-label">Finishing Untuk Jenis Kayu</label>
                          <div class="col-sm-9">
                            <select data-actions-box="true" multiple data-live-search="true" class="form-control" name="wood_finishings[]" id="wood_finishings">
                              @foreach ($woods as $row)
                                <option value="{{ $row->id }}">{{ $row->wood_name }}</option>
                              @endforeach
                            </select>

                            @if ($errors->has('wood_finishings'))
                              <div class="invalid-feedback has-error-required" role="alert">
                                  {{ $errors->first('wood_finishings') }}
                              </div>
                            @endif
                          </div>
                        </div>
                        
                        <div class="form-group row">
                          <label for="photo" class="col-sm-3 col-form-label">Foto</label>
                          <div class="col-sm-9">
                            <input
                              class="my-pond"
                              name="photo"
                              type="file"
                              class="form-control"/>

                          </div>

                        </div>
                        
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label"></label>
                          <div class="col-sm-9">
                            <img src="{{ asset('storage/finishings/' . $finishing->photo) }}" alt="" class="img-fluid thumbnail">

                          </div>
                        </div>

                        
                        <div class="form-group row">
                        	<div class="col-sm-3"></div>
                          <div class="col-sm-9">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <a href="{{ route('finishing.index') }}" class="btn btn-warning">Batal</a>
                          </div>
                        </div>
                      </form>
                </div>
            </div>
  	</div>
</div>
@endsection


@push('script')

  <script src="{{ asset('js/filepond.js') }}"></script>
  <script src="{{ asset('js/filepond-plugin-image-preview.js') }}"></script>
  <script src="{{ asset('js/filepond.jquery.js') }}"></script>
  <script src="{{ asset('plugins/selectpicker/js/bootstrap-select.min.js') }}"></script>
  <script>

    bootstrapValidate('[name=finishing_color_name]','required:Nama Jenis Warna Finishing Wajib Diisi!');

    $('select').selectpicker();
    getSelected();

    $(function(){

      // First register any plugins
      $.fn.filepond.registerPlugin(FilePondPluginImagePreview);
      $.fn.filepond.setOptions({
        server: {
          process: {
              url: '/upload',
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          },
          revert: {
            url: '/revert',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          }
        }
      });


      // Turn input element into a pond
      $('.my-pond').filepond();
    
      // Listen for addfile event
      $('.my-pond').on('FilePond:addfile', function(e) {
          console.log('file added event', e);
      });
    });

    function getSelected(){
      let url = `{{ route('finishing.show', $finishing->id) }}`;
      let woods = [];
      axios.get(url)
        .then(res => {
          let data = res.data;
          $.each(data.woods, (key,val) => {
            woods.push(val.id);
          });
          $('select').selectpicker('val', woods);
          $('select').selectpicker('refresh');
        })  
        .catch(err=>{
          console.log(err);
        });
    }

  </script>
@endpush