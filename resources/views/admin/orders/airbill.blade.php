<div class="modal fade" id="airbillModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-success text-light">
        <h5 class="modal-title" id="modalTitle"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="javascript:void(0)" method="post">
          <div class="form-group">
            <label for="airbill_number" class="control-label">No. Resi Pengiriman</label>
            <input type="text" class="form-control" id="airbill_number" name="airbill_number" placeholder="Input No. Resi">
          </div>
       
      </div>
      <div class="modal-footer">
          <button type="submit" class="btn btn-success">Update Resi</button>
         </form>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>