<div class="modal fade" id="paidModal" tabindex="-1" role="dialog" aria-labelledby="paidModal" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-success text-light">
        <h5 class="modal-title" id="modalTitle"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="javascript:void(0);" method="post">
          <div class="form-group">
            <label for="payment_type" class="control-label">Tipe Pembayaran</label>
            <select class="form-control" id="payment_type" name="payment_type">
              <option value="" disabled>- Pilih Tipe Pembayaran-</option>
              <option value="bank_transfer">Transfer Bank</option>
              <option value="kredit">Kredit</option>
              <option value="debit">Debit</option>
            </select>
          </div>

          <div class="form-group">
            <label for="payment_description" class="control-label">Deskripsi Pembayaran</label>
            <textarea rows="5" class="form-control" id="payment_description" name="payment_description" placeholder="Keterangan / Berita transfer"></textarea>
          </div>

          <div class="form-group">
            <label for="payment_amount" class="control-label">Nominal Transfer</label>
            <input type="text" class="field-uang form-control" id="payment_amount" name="payment_amount" placeholder="Nominal Transfer">
          </div>

       
      </div>
      <div class="modal-footer">
          <button id="btn-submit" type="submit" class="btn btn-success">Update Pembayaran</button>
         </form>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>