<div class="modal fade" id="modalProcess" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-success text-light">
        <h5 class="modal-title" id="modalTitle"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="javascript:void(0);" method="post" id="formProcess">
          <div class="form-group">
            <label for="total_price" class="control-label">Total Pembayaran Yang Dibebankan</label>
            <input type="text" class="form-control field-uang" id="total_price" name="total_price" placeholder="Total Pembayaran Dalam Rp.">
          </div>
          <div class="form-group">
            <label for="number_of_orders" class="control-label">Jumlah Yang Dipesan</label>
            <input disabled type="text" class="form-control" name="number_of_orders">
          </div>
          <div class="form-group">
            <label for="work_estimation" class="control-label">Estimasi Pengerjaan (Dalam Hari)</label>
            <input type="text" class="form-control" id="work_estimation" name="work_estimation" placeholder="Estimasi Pengerjaan dalam hari">
          </div>
          <div class="form-group">
            <label for="courier" class="control-label">Kurir Pengiriman</label>
            <input type="text" class="form-control" id="courier" name="courier" placeholder="Kurir Pengiriman">
          </div>
          <div class="form-group">
            <label for="courier_cost" class="control-label">Biaya Pengiriman</label>
            <input type="text" name="courier_cost" id="courier_cost" class="form-control field-uang" placeholder="Biaya Pengiriman">
          </div>

          
      </div>
      <div class="modal-footer">
        <button type="submit" id="btn-submit" class="btn btn-primary">Proses Pesanan</button>
        </form>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>