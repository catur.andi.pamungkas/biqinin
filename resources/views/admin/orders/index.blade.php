@extends('layouts.admin')

@section('title', 'Pesanan')
@push('style')
	<link rel="stylesheet" href="{{ asset('css/jquery.fancybox.css') }}">
@endpush

@section('content')
<div class="col-lg-10 col-md-9 p-4">
    <div class="row h-100">
      <div class="col-md-12 pl-3 pt-2">
          	<div class="pl-3">
              	<h3>Daftar Pesanan</h3>
              	<div>
                  	<p class="text-muted">
                    Menampilkan Data Pesanan
                  	</p>
              	</div>
          	</div>

          	<div class="pl-3">
              	<div class="card">
	  			<div class="card-header">
  					Pesanan
	  			</div>
	  			<div class="card-body">

					<div class="ml-auto mb-3">

                        @orderfilter([
                            'route' => route('admin.orders')
                        ])
                        @endorderfilter

  					</div>

	  				{!! $html->table(['class'=>'display table table-striped dt-responsive nowrap', 'style' => 'width:100%', 'cellspacing' => '0']) !!}
	  			</div>
	  		</div>
      </div>
    </div>
</div>

@include('admin.orders.detail')
@include('admin.orders.process')
@include('admin.orders.airbill')
@include('admin.orders.paid')
@include('admin.orders.confirm_payment_complete')
@endsection

@push('script')
	{!! $html->scripts() !!}
	<script src="{{ asset('js/jquery.mask.min.js') }}"></script>
	<script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
	<script src="{{ asset('plugins/moment/locales.min.js') }}"></script>
	<script>
		$(document).ready(function(){
			$( '.field-uang' ).mask('000.000.000', {reverse: true});

		});

		function detailOrder(id){
			moment.locale('id');
	    	let url = "orders/detail/" + id;
	    	$('#modalDetail').modal('show');
	        let BASE_IMAGE_URL = "{{ asset('storage/orders/') }}";
	    	axios.get(url)
			  .then(function (response) {
			    // handle success
			    console.log(response.data);
			    let data = response.data;
			    $('#modalDetail .modal-title').text(data.invoice_number);
			    let output = `
			    	<tr>
						<td>Nomor Invoice Pesanan</td>
						<td>${data.invoice_number}</td>
					</tr>
					<tr>
						<td>Nama Customer</td>
						<td>${data.customer.first_name} ${data.customer.last_name}</td>
					</tr>
					<tr>
						<td>Tanggal Pemesanan</td>
						<td>${moment(data.created_at).format("LLL")}</td>
					</tr>
					<tr>
						<td>Banyaknya Jumlah Barang Dipesan</td>
						<td>${data.number_of_orders} Barang</td>
					</tr>
					<tr>
						<td>Jenis Kayu</td>
						<td>${data.wood.wood_name}</td>
					</tr>
					<tr>
						<td>Warna Finishing</td>
						<td>${data.finishing.finishing_color_name}</td>
					</tr>
					<tr>
						<td>Catatan Tambahan</td>
						<td>${data.additional_information}</td>
					</tr>
					<tr>
						<td>Dikirim Ke</td>
						<td>
							${data.shipping_address}
						</td>
					</tr>
					<tr>
						<td>Foto</td>
						<td><img style="width: 250px" src="${BASE_IMAGE_URL}/${data.photo}" class="img-thumbnail img-fluid d-block mx-auto" /></td>
					</tr>
					<tr>
						<td>Status Pemesanan</td>
						<td><span class="badge badge-success">${data.order_status}</span></td>
					</tr>
					<tr>
						<td>Total Harga Barang</td>
						<td>${convertToRupiah(data.total_price) || 'Belum Diset'}</td>
					</tr>

					<tr>
						<td>Estimasi Pengerjaan</td>
						<td>${data.work_estimation || 'Belum Diset'}</td>
					</tr>
					<tr>
						<td>Kurir Pengiriman</td>
						<td>${data.courier || 'Belum Diset'}</td>
					</tr>
					<tr>
						<td>Biaya Pengiriman</td>
						<td>${convertToRupiah(data.courier_cost) || 'Belum Diset'}</td>
					</tr>
					<tr>
						<td>No. Resi Pengiriman</td>
						<td>${data.airbill_number || 'Belum Diset'}</td>
					</tr>
					<tr>
						<td>Total Keseluruhan</td>
						<td>${convertToRupiah(data.total) || 'Belum Diset'}</td>
					</tr>
			    `;
		    	$('#tableOrderDetail').html(output);
		  })
		  .catch(function (error) {
		    console.log(error);
		  });
		}

		function convertToRupiah(angka)
	    {
	        angka = Number(angka);
	        var rupiah = '';
	        var angkarev = angka.toString().split('').reverse().join('');
	        for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
	        return 'Rp. '+rupiah.split('',rupiah.length-1).reverse().join('');
	    }

	    function getNumberOfOrders(id){
	    	let url = "orders/detail/" + id;
	    	axios.get(url)
			  .then((response)=> {
			  	$('[name="number_of_orders"').val(response.data.number_of_orders);
			  })
			  .catch(err=>{
			  	console.log(err)
			  });
	    }

		function processOrder(id){
			let url = "orders/process";
			$('#modalProcess').modal('show');
			$('#modalProcess .modal-title').text('Proses Pesanan');
			getNumberOfOrders(id);
			$('#modalProcess #formProcess').on('submit', (e)=>{
				// e.preventDefault();
				console.log(e);
				let form = $('#modalProcess #formProcess');
				form.find('.invalid-feedback').detach();
			   	form.find(`.form-control`).removeClass('is-invalid');

			   	axios.post(url, {
			      id: id,
			      total_price: $('[name=total_price]').cleanVal(),
			      work_estimation: $('[name=work_estimation]').val(),
			      courier: $("[name=courier]").val(),
			      courier_cost: $('[name=courier_cost]').cleanVal()

			    },
			    {
			    	onUploadProgress: progressEvent => {
					    let percentCompleted = Math.floor((progressEvent.loaded * 100) / progressEvent.total);
						$('#btn-submit').html(`<div class="spinner-border text-light" role="status"><span class="sr-only">Loading... ${percentCompleted}</span></div>`)
					}
			    })
			    .then(function (response) {
			      console.log(response);
			      $('#modalProcess').modal('hide');
			      swal("Sukses", `Pesanan Berhasil Diproses`, "success");
			      $("#dataTableBuilder").DataTable().ajax.reload();
			    })
			    .catch(function (error) {

			     	if(error.response.status == 400 || error.response.status == 422  ){
			     		let errors = error.response.data;
			     		$.each(errors, (key,val) => {
	                      	// $(`#${key}`).closest('.form-control');
	                      	$(`#${key}`).addClass('is-invalid');
	                      	$(`#${key}`).after(`<div class="d-inline-block invalid-feedback" role="alert">${val.join(", ")}</div>`);
	                    });
			     	} else if(error.response.status == 500){
			     		$('#modalProcess').modal('hide');
			     		swal("Error", `Terjadi Kesalahan di Server, Periksa Koneksi Internet Anda`, "error");
			     	}
			    }).finally(() => {
			    	$('#btn-submit').remove(`.spinner-border`);
			    });
			});

		}

		function cancelOrder(id) {
			let url = "orders/cancel";
			swal({
			title: "Anda Yakin ?",
			  text: "Pesanan Akan Dibatalkan",
			  type: "warning",
			  showLoaderOnConfirm: true,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "Ya, Batalkan pesanan ini!",
			  closeOnConfirm: false,
			  title: "Anda Yakin?",
			  showCancelButton: true,
			  cancelButtonText: "Tutup",
			  closeOnCancel: true
			},
			function(isConfirm) {
			  if (isConfirm) {
				axios.post(url, {
			      id: id
			    })
			    .then(function (response) {
			      swal("Sukses", `Pesanan Berhasil Dibatalkan`, "success");
			      $("#dataTableBuilder").DataTable().ajax.reload();
			    })
			    .catch(function (error) {
			      console.log(error);
			    });
			  }
			});
		}

		function changeAirbillNumber(id){
			let url = "orders/airbill";
			$('#airbillModal').modal('show');
			$('#airbillModal .modal-title').text('Ubah No. Resi Pengiriman');
			$('#airbillModal form').on('submit', (e)=>{
				e.preventDefault();

				let form = $('#airbillModal form');
				form.find('.invalid-feedback').detach();
			   	form.find(`.form-control`).removeClass('is-invalid');

				axios.post(url, {
			      id: id,
			      airbill_number: $('[name=airbill_number]').val()
			    })
			    .then(function (response) {
			      console.log(response);
			      $('#airbillModal').modal('hide');
			      swal("Sukses", `Pesanan Berhasil Dikirimkan`, "success");
			      $("#dataTableBuilder").DataTable().ajax.reload();
			    })
			    .catch(function (error) {
			     	if(error.response.status == 400 || error.response.status == 422 ){
			     		let errors = error.response.data;
			     		$.each(errors, (key,val) => {
	                      	$(`#${key}`).addClass('is-invalid');
	                      	$(`#${key}`).after(`<div class="d-inline-block invalid-feedback" role="alert">${val.join(", ")}</div>`);
	                    });
			     	} else if(error.response.status == 500){
			     		$('#airbillModal').modal('hide');
			     		swal("Error", `Terjadi Kesalahan di Server, Periksa Koneksi Internet Anda`, "error");
			     	}
			    });
			});
		}

		function setToPaidOrder(id){
			let url = "orders/set-paid";
			const form = $('#paidModal form');
			$('#paidModal').modal('show');
			$('#paidModal .modal-title').text('Konfirmasi Pembayaran');

			$('#paidModal form').on('submit', function(e) {
				e.preventDefault();
				form.find('.invalid-feedback').detach();
			   	form.find(`.form-control`).removeClass('is-invalid');
				axios.post(url, {
			      id: id,
			      payment_type: $('#payment_type').val(),
			      payment_description: $('#payment_description').val(),
			      payment_amount: $('#payment_amount').cleanVal()
			    })
			    .then(function (response) {
		    		$('#paidModal').modal('hide');
			      	swal("Sukses", `Pesanan Berhasil Di set ke DIBAYAR`, "success");
			      	$("#dataTableBuilder").DataTable().ajax.reload();
			    })
			    .catch(function (error) {
			      	if(error.response.status == 400 || error.response.status == 422 ){
			     		let errors = error.response.data;
			     		$.each(errors, (key,val) => {
	                      	$(`#${key}`).addClass('is-invalid');
	                      	$(`#${key}`).after(`<div class="d-inline-block invalid-feedback" role="alert">${val.join(", ")}</div>`);
	                    });
			     	} else if(error.response.status == 500){
			     		$('#paidModal').modal('hide');
			     		swal("Error", `Terjadi Kesalahan di Server, Periksa Koneksi Internet Anda`, "error");
			     	}
			    })
			    .then(()=>{
			    	$('#paidModal form')[0].reset();
			    });

			});

		}


        function setPaymentComplete(id) {
            $('#confirmPaymentCompleteModal').modal('show');
			$('#confirmPaymentCompleteModal .modal-title').text('Konfirmasi Pembayaran Selesai');
            const BASE_PAYMENT_IMAGE_URL = "{{ asset('storage/payments') }}";
            let url = "orders/detail/" + id;
	    	axios.get(url)
            .then((response)=> {
                const data = response.data;
                moment.locale('id');
                $('#formConfirmPaymentComplete #payment_type').val(data.payment_type);
                $('#formConfirmPaymentComplete #payment_description').val(data.payment_description);
                $('#formConfirmPaymentComplete #payment_date').val(moment(data.payment_date).format("LLL"));
                $('#formConfirmPaymentComplete #payment_amount').val(convertToRupiah(data.payment_amount));
                $('#proofOfPaymentImage').attr('src', `${BASE_PAYMENT_IMAGE_URL}/${data.transfer_document}`).removeClass('d-none').addClass('d-block mx-auto img-thumbnail');
            })
            .catch(err=>{
                console.log(err)
            });


            $('#confirmPaymentCompleteModal form').on('submit', function(e) {
                const url= "{{ route('orders.set_payment_complete') }}";
                e.preventDefault();
                axios.post(url, {
                    id: id
                }).then(function (response) {
                    $('#confirmPaymentCompleteModal').modal('hide');
                    swal("Sukses", `Berhasil melakukan Konfirmasi Pembayaran`, "success");
                    $("#dataTableBuilder").DataTable().ajax.reload();
                })
                .catch(function (error) {
                    if(error.response.status == 500){
                        $('#confirmPaymentCompleteModal').modal('hide');
                        swal("Error", `Terjadi Kesalahan di Server, Periksa Koneksi Internet Anda`, "error");
                    }
                });
            });
        }

	</script>

	<script src="{{ asset('js/jquery.fancybox.js') }}"></script>
@endpush
