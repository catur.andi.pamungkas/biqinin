<div class="modal fade" id="confirmPaymentCompleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header bg-success text-light">
          <h5 class="modal-title" id="modalTitle"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <img src="" class="d-none img-fluid mb-3" id="proofOfPaymentImage" alt="Proof Of Payment">
            <form id="formConfirmPaymentComplete" action="javascript:void(0);" method="post">
                <div class="form-group">
                    <label for="payment_type" class="control-label">Tipe Pembayaran</label>
                    <input type="text" class="form-control" disabled id="payment_type">
                </div>

                <div class="form-group">
                    <label for="payment_description" class="control-label">Deskripsi Pembayaran</label>
                    <textarea rows="5" disabled class="form-control" id="payment_description" name="payment_description" placeholder="Keterangan / Berita transfer"></textarea>
                </div>

                <div class="form-group">
                    <label for="payment_date" class="control-label">Waktu Pembayaran</label>
                    <input type="text" disabled class="form-control" id="payment_date">
                </div>

                <div class="form-group">
                    <label for="payment_amount" class="control-label">Nominal Transfer</label>
                    <input type="text" class="field-uang form-control" disabled id="payment_amount" name="payment_amount" placeholder="Nominal Transfer">
                </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-success">KonfirmasikanPembayaran</button>
           </form>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        </div>
      </div>
    </div>
  </div>
