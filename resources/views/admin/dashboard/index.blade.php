
@extends('layouts.admin')

@section('title', 'Dashboard')
@section('content')

@push('style')
  <link rel="stylesheet" href="{{ asset('css/jquery.fancybox.css') }}">
@endpush

<div class="col-lg-10 col-md-9 p-4">
  <div class="row ">
    <div class="col-md-12 pl-3 pt-2">
        <div class="pl-3">
            <h3>Dashboard</h3>
        </div>
    </div>
  </div>

  <!-- start info box -->
  <div class="row ">
    <div class="col-md-12 pl-3 pt-2">
        <div class="row pl-3">

          <div class="col-md-6 col-lg-6 col-12 mb-2 col-sm-6">
            <div class="media shadow-sm p-0 bg-primary text-light rounded">
              <span class="oi top-0 rounded-left bg-white text-primary h-100 p-4 oi-cart fs-5"></span>
              <div class="media-body p-2">
                <h6 class="media-title m-0">Pesanan Baru</h6>
                <div class="media-text">
                  <h3>{{ $submitted }}</h3>
                </div>
              </div>
            </div>
          </div>



          <div class="col-md-6 col-lg-6 col-12 mb-2 col-sm-6">
              <div class="media shadow-sm p-0 bg-primary text-light rounded">
                <span class="oi top-0 rounded-left bg-white text-primary h-100 p-4 oi-cart fs-5"></span>
                <div class="media-body p-2">
                  <h6 class="media-title m-0">Pesanan Di Proses</h6>
                  <div class="media-text">
                    <h3>{{ $processed}}</h3>
                  </div>
                </div>
              </div>
            </div>

          <div class="col-md-6 col-lg-6 col-12 mb-2 col-sm-6">
              <div class="media shadow-sm p-0 bg-primary text-light rounded">
                <span class="oi top-0 rounded-left bg-white text-primary h-100 p-4 oi-cart fs-5"></span>
                <div class="media-body p-2">
                  <h6 class="media-title m-0">Pesanan Dibatalkan</h6>
                  <div class="media-text">
                    <h3>{{ $cancelled }}</h3>
                  </div>
                </div>
              </div>
            </div>


          <div class="col-md-6 col-lg-6 col-12 mb-2 col-sm-6">
            <div class="media shadow-sm p-0 bg-primary text-light rounded">
              <span class="oi top-0 rounded-left bg-white text-primary h-100 p-4 oi-cart fs-5"></span>
              <div class="media-body p-2">
                <h6 class="media-title m-0">Pesanan Dikirim</h6>
                <div class="media-text">
                  <h3>{{ $delivered}}</h3>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-6 col-lg-6 col-12 mb-2 col-sm-6">
              <div class="media shadow-sm p-0 bg-primary text-light rounded">
                <span class="oi top-0 rounded-left bg-white text-primary h-100 p-4 oi-cart fs-5"></span>
                <div class="media-body p-2">
                  <h6 class="media-title m-0">Pesanan Perlu Dibayar/Dibayar</h6>
                  <div class="media-text">
                    <h3>{{ $has_paid }}</h3>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-6 col-lg-6 col-12 mb-2 col-sm-6">
                <div class="media shadow-sm p-0 bg-primary text-light rounded">
                  <span class="oi top-0 rounded-left bg-white text-primary h-100 p-4 oi-cart fs-5"></span>
                  <div class="media-body p-2">
                    <h6 class="media-title m-0">Pesanan Sudah Dibayar/Dikonfirmasi</h6>
                    <div class="media-text">
                      <h3>{{ $paid_confirmed }}</h3>
                    </div>
                  </div>
                </div>
              </div>


          <div class="col-md-6 col-lg-6 col-12 mb-2 col-sm-6">
            <div class="media shadow-sm p-0 bg-primary text-light rounded">
                <span class="oi top-0 rounded-left bg-white text-primary h-100 p-4 oi-cart fs-5"></span>
              <div class="media-body p-2">
                <h6 class="media-title m-0">Pesanan Selesai</h6>
                <div class="media-text">
                  <h3>{{ $finished }}</h3>
                </div>
              </div>
            </div>
          </div>

        </div>

        <!-- start third box -->
            <div class="row pl-3 mb-5 mt-3">
              <div class="col-lg-12 mb-2">
                <div class="card">
                  <div class="card-header bg-white pb-1">
                    <h6>Pesanan Terbaru</h6>
                  </div>
                  <div class="card-body px-0 py-0">
                    <div class="table-responsive">
                      <table class="table mb-0">
                        <thead>
                          <tr>
                            <th>Nomor Invoice</th>
                            <th>Tanggal</th>
                            <th>Status</th>
                            <th>Foto</th>
                            <th>Customer</th>
                          </tr>
                        </thead>
                        <tbody>
                          @forelse ($orders as $order)
                            <tr>
                              <td>{{ $order->invoice_number }}</td>
                              <td>{{ tanggalIndonesia($order->created_at) }}</td>
                                <td>
                                    @include('components.formatters.order_table.order_status', [
                                      'order' => $order
                                  ])
                                  {{-- <div class="badge badge-success text-light">{{ $order->order_status }}</div> --}}
                                </td>
                              <td>
                                <a data-fancybox="gallery" href="{{ asset('storage/orders/' . $order->photo) }}"><img style="width: 200px" class="thumbnail" src="{{ asset('storage/orders/' . $order->photo) }}"></a>
                              </td>
                              <td>{{ $order->customer->first_name }} {{ $order->customer->last_name }}</td>
                            </tr>
                          @empty
                            <tr>
                              <td colspan="5" class="text-center">Belum Ada Pesanan Baru</td>
                            </tr>
                          @endforelse

                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div class="card-footer d-flex justify-content-between bg-white">

                    <a href="{{ url('admin/orders') }}" class="btn btn-link text-primary">Lihat Semua Pesanan</a>
                  </div>
                </div>
              </div>

            </div>
            <!-- end third box -->
    </div>
  </div>
  <!-- end info box -->
</div>
@endsection

@push('script')
  <script src="{{ asset('js/jquery.fancybox.js') }}"></script>
@endpush
