@extends('layouts.admin')

@section('title')
  Tambah Galleri Foto
@endsection

@push('style')
  <link rel="stylesheet" href="{{ asset('css/filepond.css') }}">
  <link rel="stylesheet" href="{{ asset('css/filepond-plugin-image-preview.css') }}">
@endpush

@section('content')
<div class="col-lg-10 col-md-9 p-4">

  	<div class="row pl-3 pt-2">
  		<div class="col-lg-12 col-sm-12 col-md-12">
            <div class="card">
                <div class="card-header bg-primary text-light mb-1">
                  <h5>Tambah Galleri Foto</h5>
                </div>
                <div class="card-body">
                    <form enctype="multipart/form-data" method="post" action="{{ route('galleries.store') }}" >
                    	@csrf
                        
                        <div class="form-group row">
                          <label for="photo" class="col-sm-3 col-form-label">Foto</label>
                          <div class="col-sm-9">
                            <input
                              class="my-pond"
                            	name="photo"
                            	type="file"
                            	class="form-control"/>

                          </div>

                        </div>

                        <div class="form-group row">
                          <label for="caption" class="col-sm-3 col-form-label">Keterangan</label>
                          <div class="col-sm-9">
                            <textarea name="caption" required id="caption" rows="8" placeholder="Keterangan Gambar" class="form-control {{ $errors->has('caption') ? ' is-invalid' : '' }}"></textarea>

                            @if ($errors->has('caption'))
                              <div class="invalid-feedback has-error-required" role="alert">
                                  {{ $errors->first('caption') }}
                              </div>
                            @endif
                          </div>
                          
                        </div>
                        
                        <div class="form-group row">
                        	<div class="col-sm-3"></div>
                          <div class="col-sm-9">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <a href="{{ route('galleries.index') }}" class="btn btn-warning">Batal</a>
                          </div>
                        </div>
                      </form>
                </div>
            </div>
  	</div>
</div>
@endsection

@push('script')
  <script src="{{ asset('js/filepond.js') }}"></script>
  <script src="{{ asset('js/filepond-plugin-image-preview.js') }}"></script>
  <script src="{{ asset('js/filepond.jquery.js') }}"></script>
  <script>
    bootstrapValidate('[name=caption]','required:Keterangan Gambar Wajib Diisi!');
   
    $(function(){
      // First register any plugins
      $.fn.filepond.registerPlugin(FilePondPluginImagePreview);
      $.fn.filepond.setOptions({
        server: {
          process: {
              url: '/upload',
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          },
          revert: {
            url: '/revert',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          }
        }
      });


      // Turn input element into a pond
      $('.my-pond').filepond();

      $('.my-pond').filepond('required', true);
    
      // Listen for addfile event
      $('.my-pond').on('FilePond:addfile', function(e) {
          console.log('file added event', e);
      });
    });
  </script>
@endpush