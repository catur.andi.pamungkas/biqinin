@extends('layouts.admin')

@section('title', 'Galleri Foto')

@push('style')
	<link rel="stylesheet" href="{{ asset('css/jquery.fancybox.css') }}">
@endpush

@section('content')
<div class="col-lg-10 col-md-9 p-4">
    <div class="row h-100">
      <div class="col-md-12 pl-3 pt-2">
          	<div class="pl-3">
              	<h3>Data Galeri Foto</h3>
              	<div>
                  	<p class="text-muted">
                    Menampilkan Data Galeri Foto
                  	</p>
              	</div>
          	</div>

          	<div class="pl-3">
              	<div class="card">
	  			<div class="card-header">
  					<a href="{{ route('galleries.create') }}" class="btn btn-primary"><span class="oi io-plus"></span> Tambah Foto Galeri</a>
	  			</div>
	  			<div class="card-body">
					
					<div class="ml-auto mb-3">
  						<form action="{{ route('galleries.index') }}">
					        <div class="input-group">
					        	<input
						          value="{{Request::get('keyword')}}"
						          name="keyword"
						          class="form-control"
						          type="text"
						          placeholder="Cari Berdasarkan Keterangan Gambar"/>
						          <div class="input-group-append">
					                <input
					                type="submit"
					                value="Cari"
					                class="btn btn-primary">
					            </div>
						    </div>
						    
						</form>
  					</div>
			    	<div class="table-responsive">
			    		
			    	
		  				<table class="table table-striped" id="data-table">
							<thead>
								<th>No.</th>
								<th>Caption</th>
								<th>Foto</th>
								<th>Action</th>
							</thead>
							<tbody>
								@if (count($galleries)>0)
									@php
										$no = $galleries->firstItem();
									@endphp
									@foreach ($galleries as $gallery)
										<tr>
											<td>{{ $no++}}</td>
											<td>{{ $gallery->caption }}</td>
											<td>
												<a data-fancybox="gallery" href="{{ asset('storage/gallery/' . $gallery->photo) }}"><img style="width: 100px" class="thumbnail" src="{{ asset('storage/gallery/' . $gallery->photo) }}"></a>
											</td>
											<td>
												<a href="{{ route('galleries.edit', $gallery->id) }}" class="btn btn-info"><span class="oi oi-pencil"></span></a>
												<a onclick="removeData({{ $gallery->id }})" href="javascript:void(0)" class="btn btn-delete btn-danger"><span class="oi oi-trash"></span></a>
											</td>
										</tr>
									@endforeach
								@else
									<tr>
										<td colspan="5" class="text-center">Maaf, Data Kosong</td>
									</tr>
								@endif
								
								
							</tbody>
						</table>
						{{ $galleries->links() }}

					</div>
	  			</div>
	  		</div>
      </div>
    </div>
</div>
@endsection

@push('script')
	<script src="{{ asset('js/jquery.fancybox.js') }}"></script>
	<script>

		function removeData(id){
			let route = `galleries/${id}`;
			window.fn.delete(id,route);
		}


	    @if (session()->has('success'))
	      $(function(){
	        swal("Sukses", `{{ session()->get('success') }}`, "success")
	      });
	    @endif
	</script>
@endpush