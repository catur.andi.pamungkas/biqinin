<form method="get" action="{{ $route ?? route('history') }}">
    <div class="input-group">
        <select
            name="type" class="form-control">
            <option {{ Request::get('type') == '' ? 'selected': '' }} value="">Semua Pesanan</option>
            <option {{ Request::get('type') == 'SUBMIT' ? 'selected': '' }} value="SUBMIT">Pesanan Baru</option>
            <option {{ Request::get('type') == 'PROCESS' ? 'selected': '' }} value="PROCESS">Pesanan Diproses</option>
            <option {{ Request::get('type') == 'PAID' ? 'selected': '' }} value="PAID">Dibayar/Menunggu Konfirmasi</option>
            <option {{ Request::get('type') == 'PAID_CONFIRM' ? 'selected': '' }} value="PAID_CONFIRM">Sudah Dibayar/ Sudah Dikonfirmasi</option>
            <option {{ Request::get('type') == 'DELIVER' ? 'selected': '' }} value="DELIVER">Pesanan Dikirimkan</option>
            <option {{ Request::get('type') == 'CANCEL' ? 'selected': '' }} value="CANCEL">Pesanan Dibatalkan</option>

            {{ $options ?? '' }}
        </select>

          <div class="input-group-append">
            <input
            type="submit"
            value="Filter"
            class="btn btn-primary">
        </div>

    </div>

    {{ $slot ?? '' }}
</form>
