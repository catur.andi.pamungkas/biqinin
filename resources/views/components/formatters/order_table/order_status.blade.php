
@if ($order->order_status === 'PAID' || $order->order_status === 'PAID_CONFIRM' )
    @if ($order->order_status == 'PAID')
    <span class="badge badge-info">Dibayar/Menunggu Konfirmasi Admin</span>
    @endif

    @if ($order->order_status == 'PAID_CONFIRM')
    <span class="badge badge-info">Sudah Dibayar/Sudah Dikonfirmasi Admin</span>
    @endif
@else

<span class="badge badge-info">{{ strtolower($order->order_status) }}</span>

@endif
