@push('style')
<link rel="stylesheet" href="{{ asset('plugins/fileinput/css/fileinput.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/fileinput/themes/explorer-fas/theme.min.css') }}">
@endpush


{{ $slot }}

@push('script')
<script src="{{ asset('plugins/fileinput/js/plugins/piexif.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/fileinput/js/plugins/sortable.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/fileinput/js/fileinput.min.js') }}"></script>
<script src="{{ asset('plugins/fileinput/js/locales/id.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/fileinput/themes/fas/theme.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/fileinput/themes/explorer-fas/theme.js') }}" type="text/javascript"></script>

{{ $scripts ?? '' }}

@endpush
