@extends('layouts.customer')

@section('title')
  Chat
@endsection
@section('content')


  <script>
     window.auth = '@json(auth()->user())';
  </script>
  
  <div id="app" class="col-lg-10 col-md-9 p-4">
    <chat-component></chat-component>
  </div>
  

@endsection