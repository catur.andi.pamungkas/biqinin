@component('mail::message')
# Pesanan Anda Dengan Nomor Invoice ({{ $order->invoice_number }}) Telah Kami Proses

Kode Unik Pembayaran Anda adalah  {{ $unique_id }}
Silahkan Lakukan Transfer Dana Sebesar `{{ formatRupiah($order->total) }}` Ke Rekening Berikut :

@component('mail::table')

| Bank           | No. Rekening  | Atas Nama                 |
| :------------- |:-------------| :-------------------------|
@foreach ($banks as $bank)
| {{ $bank->bank_name }} | {{ $bank->account_number }} | {{ $bank->owner_name }} |
@endforeach

@endcomponent

##Pastikan Anda Transfer Tepat Sesuai Nominal Yang Tertera agar Sistem kami dapat otomatis melakukan pengecekan pembayaran anda.

###Lakukan Pembayaran Maksimal 6 Jam Kedepan ({{ tanggalIndonesia($order->order_expired) }}), Jika tidak pesanan anda akan otomatis dibatalkan.

###Berikut Detail Dari Pesanan Anda

@component('mail::table')

| 							| 					 													 |
| ------------------------- |:-----------------------------------------------------------------------|
| Invoice       			| {{ $order->invoice_number }}                   						 |
| Nama Pemesan  			| {{ $order->customer->first_name}} {{ $order->customer->last_name}}     |
| Total Biaya Barang 		| {{ formatRupiah($order->total_price)}}   								 |
| Total Yang Harus Dibayar  | {{ formatRupiah($order->total)}}   									 |
| Tanggal Pesanan           | {{ tanggalIndonesia($order->created_at)}} 							 |
| Maksimal Transfer         | {{ tanggalIndonesia($order->order_expired)}} 							 |
| Informasi Tambahan        | {{ $order->additional_information}}                                    |
| Estimasi Pengerjaan       | {{ $order->work_estimation}} 		                                     |
| Kurir Pengiriman          | {{ $order->courier}}       				                             |
| Biaya Pengiriman	        | {{ formatRupiah($order->courier_cost)}}        						 |

@endcomponent


Terimakasih,<br>
{{ config('app.name') }}
@endcomponent
