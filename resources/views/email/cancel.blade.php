@component('mail::message')
# Pesanan Anda Dengan Nomor Invoice ({{ $order->invoice_number }}) Telah Kami Batalkan

##Pesanan Anda telah kami batalkan, silakan lakukan pemesanan ulang atau gunakan fitur reorder di histori pesanan anda untuk melakukan pesanan ulang secara lebih mudah.

###Berikut Detail Dari Pesanan Anda yang telah dibatalkan

@component('mail::table')

| 							| 					 													 |
| ------------------------- |:-----------------------------------------------------------------------|
| Invoice       			| {{ $order->invoice_number }}                   						 |
| Nama Pemesan  			| {{ $order->customer->first_name}} {{ $order->customer->last_name}}     |
| Total Biaya Barang 		| {{ formatRupiah($order->total_price)}}   								 |
| Total Yang Harus Dibayar  | {{ formatRupiah($order->total)}}   									 |
| Tanggal Pesanan           | {{ tanggalIndonesia($order->created_at)}} 							 |
| Maksimal Transfer         | {{ tanggalIndonesia($order->order_expired)}} 							 |

@endcomponent

###Buka Halaman Histori Pesanan pada Dashboard anda untuk melakukan pemesanan ulang.
@component('mail::button', ['url' => url('customer/orders')])
Lihat Histori Pesanan Saya
@endcomponent

Terimakasih,<br>
{{ config('app.name') }}
@endcomponent
