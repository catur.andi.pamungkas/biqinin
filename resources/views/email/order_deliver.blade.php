@component('mail::message')
# Pesanan ({{ $order->invoice_number }}) Telah Kami Kirimkan.

##Pesanan Anda sedang dikirim, 

###Berikut Detail Status Pesanan Anda

@component('mail::table')

| 							| 					 													 |
| ------------------------- |:-----------------------------------------------------------------------|
| Invoice       			| {{ $order->invoice_number }}                   						 |
| Nama Pemesan  			| {{ $order->customer->first_name}} {{ $order->customer->last_name}}     |
| Total Biaya Barang 		| {{ formatRupiah($order->total_price)}}   								 |
| Total Yang Harus Dibayar  | {{ formatRupiah($order->total)}}   									 |
| Tanggal Pesanan           | {{ tanggalIndonesia($order->created_at)}} 							 |
| Maksimal Transfer         | {{ tanggalIndonesia($order->order_expired)}} 							 |
| Nomor RESI Pengiriman     | {{ $order->airbill_number }}							 				 |

@endcomponent

###Mohon Cek Histori Pengiriman Anda Untuk Terus memantau status pengiriman barang.

@component('mail::button', ['url' => url('customer/orders')])
Lihat Histori Pesanan Saya
@endcomponent

Terimakasih,<br>
{{ config('app.name') }}
@endcomponent
