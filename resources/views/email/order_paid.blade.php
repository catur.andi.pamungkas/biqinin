@component('mail::message')
# Pembayaran Untuk pesanan ({{ $order->invoice_number }}) Telah Kami terima

## Pembayaran untuk pesanan {{ $order->invoice_number }} telah kami terima, selanjutnya silakan tunggu pesanan dikirim ke alamat anda.

### Berikut Detail pembayaran anda

@component('mail::table')

| 							| 					 													 |
| ------------------------- |:-----------------------------------------------------------------------|
| Tipe Pembayaran       	| {{ $order->payment_type }}                   							 |
| Tanggal Pembayaran  		| {{ tanggalIndonesia($order->payment_date)  }} 						 |
| Jumlah Transfer 			| {{ formatRupiah($order->payment_amount)}}   							 |
| Berita Transfer		    | {{ $order->payment_description }}   									 |

@endcomponent

### Detail Pesanan Anda

@component('mail::table')

| 							| 					 													 |
| ------------------------- |:-----------------------------------------------------------------------|
| Invoice       			| {{ $order->invoice_number }}                   						 |
| Nama Pemesan  			| {{ $order->customer->first_name}} {{ $order->customer->last_name}}     |
| Total Biaya Barang 		| {{ formatRupiah($order->total_price)}}   								 |
| Total Yang Harus Dibayar  | {{ formatRupiah($order->total)}}   									 |
| Tanggal Pesanan           | {{ tanggalIndonesia($order->created_at)}} 							 |
| Maksimal Transfer         | {{ tanggalIndonesia($order->order_expired)}} 							 |

@endcomponent

Terimakasih,
{{ config('app.name') }}
@endcomponent
