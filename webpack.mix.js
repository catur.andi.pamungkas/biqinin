const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css');

mix.scripts([
    'public/plugins/datatables/jquery.dataTables.min.js',
    'public/plugins/datatables/dataTables.bootstrap4.min.js',
    'public/plugins/datatables/dataTables.responsive.min.js',
    'public/plugins/datatables/responsive.bootstrap4.min.js',
    'public/admin/bootstrap-validate.js',
    'public/client/js/sweetalert.js',
    'public/js/global_func.js',
], 'public/js/plugins.js');

mix.styles([
    'public/admin/polished.min.css',
    'public/plugins/datatables/dataTables.bootstrap4.min.css',
    'public/plugins/datatables/select.bootstrap4.min.css',
    'public/plugins/datatables/responsive.bootstrap4.min.css',
    'public/css/jquery.fancybox.css',
    'public/css/filepond.css',
    'public/css/filepond-plugin-image-preview.css',
    'public/plugins/selectpicker/css/bootstrap-select.min.css',
], 'public/css/admin-customer.css');
