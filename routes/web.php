<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('invoice', 'PagesController@invoice');

Route::get('/', 'HomeController@index');
Route::get('province/{id}', 'HomeController@province');
Route::get('city/{id}', 'HomeController@city');
Route::get('district/{id}', 'HomeController@district');

Route::get('email/resend', 'Auth\VerificationController@resend');
Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');
Route::get('auth/{provider}', 'AuthController@redirectToProvider');
Route::get('auth/{provider}/callback', 'AuthController@handleProviderCallback');

// Chat Module Routes
Route::get('/session/get-friends', 'SessionController@getFriends');
Route::post('/session/create', 'SessionController@create');
Route::post('/session/{session}/chats', 'ChatController@chats');
Route::post('/session/{session}/read', 'ChatController@read');
Route::post('/session/{session}/clear', 'ChatController@clear');
Route::post('/send/{session}', 'ChatController@sendMessage');
Route::post('/session/{session}/block', 'BlockController@block');
Route::post('/session/{session}/unblock', 'BlockController@unblock');

// Drag n Drop Upload Controller
Route::post('upload', 'Admin\UploadController@upload');
Route::delete('revert', 'Admin\UploadController@revert');

Route::get('about-us', 'HomeController@about');
Route::get('galleries', 'HomeController@galleries');

Route::post('cek-mutasi/callback', 'CallbackCekMutasi@cekMutasi');

Route::group(['prefix' => 'admin', 'middleware' => ['role:admin']], function () {
    Route::resource('woods', 'Admin\WoodsController')->except([
        'store', 'update', 'destroy'
    ]);
    Route::resource('banks', 'Admin\BanksController')->except([
        'store', 'update', 'destroy'
    ]);
    Route::resource('finishing', 'Admin\FinishingColorsController')->except([
        'store', 'update', 'destroy'
    ]);
    Route::resource('galleries', 'Admin\GalleriesController')->except([
        'store', 'update', 'destroy'
    ]);
});


Route::group(['prefix' => 'admin', 'middleware' => ['role:superadmin']], function () {
    Route::resource('woods', 'Admin\WoodsController');
    Route::resource('banks', 'Admin\BanksController');
    Route::resource('orders', 'Admin\OrdersController')->name('index', 'admin.orders');
    Route::resource('finishing', 'Admin\FinishingColorsController');
    Route::resource('galleries', 'Admin\GalleriesController');
});

Route::group(['prefix' => 'admin', 'middleware' => ['role:admin|superadmin']], function () {
    Route::get('/', 'Admin\DashboardController@index');
    Route::resource('dashboard', 'Admin\DashboardController');
    Route::get('chat', 'ChatController@index');

    Route::resource('orders', 'Admin\OrdersController')->name('index', 'admin.orders');

    Route::get('customers', 'Admin\CustomersController@index')->name('customers.index');
    Route::get('customers/{id}', 'Admin\CustomersController@show');

    Route::get('orders/detail/{id}', 'Admin\OrdersController@detail')->name('orders.detail');
    Route::post('orders/process', 'Admin\OrdersController@process')->name('orders.process');
    Route::post('orders/cancel', 'Admin\OrdersController@cancel')->name('orders.cancel');
    Route::post('orders/set-paid', 'Admin\OrdersController@paid');
    Route::post('orders/airbill', 'Admin\OrdersController@updateAirbill')->name('orders.airbill');
    Route::get('orders/invoice/{id}', 'Admin\OrdersController@printInvoice')->name('orders.invoice');
    Route::post('order/set_payment_complete', 'Admin\OrdersController@setOrderPaymentComplete')->name('orders.set_payment_complete');

    Route::get('settings', 'Admin\SettingsController@index')->name('settings');
    Route::put('settings/{id}', 'Admin\SettingsController@update')->name('settings.update');
    Route::post('settings/password', 'Admin\SettingsController@password')->name('settings.password');
});



Route::group(['prefix' => 'customer', 'middleware' => ['role:customer']], function () {
    Route::resource('dashboard', 'Customer\DashboardController');
    Route::resource('orders', 'Customer\OrdersController');

    Route::post('orders/set-paid', 'Customer\OrdersController@paid');
    Route::get('history', 'Customer\OrdersController@history')->name('history');
    Route::get('chat', 'ChatController@index');
    Route::get('profile', 'Customer\ProfileController@index');
    Route::get('how-to-order', 'Customer\PagesController@howToOrder');
    Route::get('profile/edit', 'Customer\ProfileController@edit')->name('profile.edit');
    Route::get('orders/detail/{id}', 'Customer\OrdersController@detail');
    Route::post('orders/reorder', 'Customer\OrdersController@repeatOrder');
    Route::put('profile/{id}', 'Customer\ProfileController@update')->name('profile.update');
    Route::post('profile/password', 'Customer\ProfileController@password')->name('profile.password');

    Route::get('woods/{id}', 'Customer\OrdersController@getWoodsWith');
    Route::get('finishing/{id}', 'Customer\OrdersController@getFinishingColor');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
