<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('login', 'Api\Auth\AuthController@login');
Route::post('register', 'Api\Auth\AuthController@register');
Route::post('refresh', 'Api\Auth\AuthController@refresh');


Route::get('provinces', 'HomeController@provinces');
Route::get('province/{id}', 'HomeController@province');
Route::get('city/{id}', 'HomeController@city');
Route::get('district/{id}', 'HomeController@district');


Route::middleware('auth:api')->group(function () {
    // Orders
    // Route::get('orders/detail/{id}', 'Api\Customer\OrdersController@detail');
    Route::get('orders/history', 'Api\Customer\OrdersController@history');
    Route::get('orders/filter', 'Api\Customer\OrdersController@filterByOrderStatus');
    Route::get('orders/detail/{id}', 'Api\Customer\OrdersController@detail');
    Route::post('orders/search', 'Api\Customer\OrdersController@search');
    Route::post('orders/process', 'Api\Customer\OrdersController@process');
    Route::put('orders/proof_of_payment/{order}', 'Api\Customer\OrdersController@uploadProofOfPayment');


    Route::get('profile', 'Api\Auth\AuthController@profile');
    Route::post('profile/change-photo', 'Api\Customer\ProfileController@changePhotoProfile');
    Route::post('profile/change-profile', 'Api\Customer\ProfileController@changeProfile');
    Route::get('customer', 'Api\Customer\ProfileController@customer');
    Route::post('logout', 'Api\Auth\AuthController@logout');
});

#Route::get('customer', 'Api\Customer\ProfileController@index');
// Woods
Route::get('woods', 'Api\Customer\OrdersController@woods');
Route::get('woods/{id}/finishing-color', 'Api\Customer\OrdersController@finishingColorsByWood');

Route::get('finishing-colors/{id}', 'Api\Customer\OrdersController@finishingColor');

Route::get('galleries', 'Api\Customer\PagesController@galleries');
Route::get('helps', 'Api\Customer\PagesController@helps');
Route::get('banks', 'Api\Customer\PagesController@bankList');
Route::get('general-info', 'Api\Customer\PagesController@generalInfo');

Route::get('contact', 'Api\Customer\PagesController@contact');
