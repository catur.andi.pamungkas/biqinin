<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Wood
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\FinishingColor[] $finishings
 * @property-read int|null $finishings_count
 * @property-read \App\Order|null $order
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wood newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wood newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wood query()
 * @mixin \Eloquent
 */
class Wood extends Model
{

    public $table = 'woods';
    protected $fillable = [
    	'wood_name',
    	'photo'
    ];

    public function order()
    {
    	return $this->hasOne('App\Order');
    }

    public function finishings()
    {
    	return $this->belongsToMany('App\FinishingColor','wood_finishings');
    }
}
