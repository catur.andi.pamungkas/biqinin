<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * App\WoodFinishings
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\WoodFinishings newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\WoodFinishings newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\WoodFinishings query()
 * @mixin \Eloquent
 */
class WoodFinishings extends Pivot
{
    //
}
