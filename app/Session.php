<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Session
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Chat[] $chats
 * @property-read int|null $chats_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Message[] $messages
 * @property-read int|null $messages_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Session newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Session newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Session query()
 * @mixin \Eloquent
 */
class Session extends Model
{
    protected $fillable = ['user1_id', 'user2_id'];

    public function chats()
    {
    	return $this->hasManyThrough(Chat::class, Message::class);
    }

    public function messages()
    {
    	return $this->hasMany(Message::class);
    }

    public function deleteChat()
    {
        return $this->chats()->where('user_id', auth()->id())->delete();
    }

    public function deleteMessages()
    {
        return $this->messages()->delete();
    }
}
