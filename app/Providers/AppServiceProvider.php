<?php

namespace App\Providers;
use App\Services\IndonesiaRegion\IndonesiaRegionService;

use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        date_default_timezone_set('Asia/Jakarta');
        Carbon::setLocale('id_ID.utf8');

        $this->registerBindings();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        
    }

    private function registerBindings()
    {
        $this->app->singleton(IndonesiaRegionService::class, function ($app) {
            return new IndonesiaRegionService;
        });
        
    }
}
