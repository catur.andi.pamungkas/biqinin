<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class BladeComponentServiceProvider extends ServiceProvider
{


    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::component('components.filters.order_filter', 'orderfilter');
        Blade::component('components.plugins.upload_file_single', 'uploadfilesingle');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }
}
