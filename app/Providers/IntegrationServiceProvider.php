<?php

namespace App\Providers;

use Twizo\Api\Twizo;
use Illuminate\Support\ServiceProvider;

class IntegrationServiceProvider extends ServiceProvider
{

    public $twizzo;

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton('twizzo', function ($app) {
            return $this->twizzo;
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->twizzo = Twizo::getInstance(config('twizzo.app_key'), config('twizzo.host'));
    }
}
