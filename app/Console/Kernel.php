<?php

namespace App\Console;

use App\Notifications\OrderCancelled;
use App\Order;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
       
        $schedule->call(function () {
            $orders = Order::where('order_status', '=', 'PROCESS')->get();
            foreach ($orders as $order) {
                $order_date = Carbon::parse($order->created_at);
                // $order_exp = Carbon::parse($order->created_at)->addHours(3);
                $order_exp = Carbon::parse($order->order_expired);
                $now = Carbon::now();
                // $length = $order_exp->diffInDays($order_date);
                
                if($now->gte($order_exp)){
                    $order->order_status = 'CANCEL';
                    $order->update();
                    $order->customer->user->notify(new OrderCancelled($order));
                }
            }
        })->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
