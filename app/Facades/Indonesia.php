<?php

namespace App\Facades;
use App\Services\IndonesiaRegion\IndonesiaRegionService;

use Illuminate\Support\Facades\Facade;

class Indonesia extends Facade {

	protected static function getFacadeAccessor() { return IndonesiaRegionService::class; }

}