<?php

namespace App\Traits;

trait UploadTrait {
	public function imageUploadBase64($imageField, $publicPath)
	{

		$customer = auth()->user()->username;
		$imageBase64Decrypt = base64_decode($imageField);
        $image = imagecreatefromstring($imageBase64Decrypt);
        $imageFileName = $customer .'_' .date('Y-m-d H-i-s'). '.jpg';
        $imagePath = storage_path('app/public/' . $publicPath .'/'. $imageFileName);
        imagejpeg($image, $imagePath);
        imagedestroy($image);

        return $imageFileName;
    }


}
