<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\City;
use App\District;
use App\Province;
use App\Village;

/**
 * App\Customer
 *
 * @property-read \App\City $city
 * @property-read \App\District $district
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Order[] $orders
 * @property-read int|null $orders_count
 * @property-read \App\Province $province
 * @property-read \App\User $user
 * @property-read \App\Village $village
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer query()
 * @mixin \Eloquent
 */
class Customer extends Model
{
    protected $fillable = [
    	'user_id',
        'first_name',
        'last_name',
        'address',
        'phone_number',
        'province_id',
        'city_id',
        'district_id',
        'village_id',
        'postal_code'
    ];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function district()
    {
        return $this->belongsTo(District::class);
    }

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function village()
    {
        return $this->belongsTo(Village::class);
    }

    public function orders()
    {
        return $this->hasMany('App\Order');
    }


}
