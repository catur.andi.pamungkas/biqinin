<?php

use App\Order;
use App\Setting;
use Illuminate\Support\Facades\Auth;

	if(! function_exists('get_social')){
		function get_social($keyname){
			$data = Setting::first();

			if($keyname == 'fb'){
				return $data->facebook ?? '-';
			}
			if($keyname == 'wa'){
				return $data->whatsapp ?? '-';
			}

			if($keyname == 'ig'){
				return $data->instagram ?? '-';
			}

		}
	}

    function activeRoute($key) {

        if(request()->is("*/$key/*") || request()->is("*/$key")) {
            return 'active';
        }
        return false;
    }

	function formatRupiah($nominal)
    {
        $hasil = number_format($nominal,0,',','.');
        return 'Rp.' . $hasil;
    }

    function tanggalIndonesia($tgl, $tampil_hari=true,$jam=true)
    {
        $nama_hari = array("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu");
        $nama_bulan = array(1=>"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

        $tahun = substr($tgl,0,4);
        $bulan = $nama_bulan[(int)substr($tgl,5,2)];
        $tanggal = substr($tgl,8,2);

        $text = "";

        if($tampil_hari){
            $urutan_hari = date('w', mktime(0,0,0, substr($tgl,5,2), $tanggal, $tahun));
            $hari = $nama_hari[$urutan_hari];
            $text .= $hari.", ";
        }

        $text .= $tanggal ." ". $bulan ." ". $tahun;

        if($jam){
            $jam = date("H:i:s",strtotime($tgl));
            $text.=" ".$jam;
        }
        return $text;
    }

    function rupiahTerbilang($rupiah)
    {
        $angka = abs($rupiah);
        $baca = array("", "satu", "dua", "tiga", "empat", "lima",
        "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $terbilang = "";
        if ($angka <12) {
            $terbilang = " ". $baca[$angka];
        } else if ($angka <20) {
            $terbilang = rupiahTerbilang($angka - 10). " belas";
        } else if ($angka <100) {
            $terbilang = rupiahTerbilang($angka/10)." puluh". rupiahTerbilang($angka % 10);
        } else if ($angka <200) {
            $terbilang = " seratus" . rupiahTerbilang($angka - 100);
        } else if ($angka <1000) {
            $terbilang = rupiahTerbilang($angka/100) . " ratus" . rupiahTerbilang($angka % 100);
        } else if ($angka <2000) {
            $terbilang = " seribu" . rupiahTerbilang($angka - 1000);
        } else if ($angka <1000000) {
            $terbilang = rupiahTerbilang($angka/1000) . " ribu" . rupiahTerbilang($angka % 1000);
        } else if ($angka <1000000000) {
            $terbilang = rupiahTerbilang($angka/1000000) . " juta" . rupiahTerbilang($angka % 1000000);
        }

        return $terbilang;
    }
