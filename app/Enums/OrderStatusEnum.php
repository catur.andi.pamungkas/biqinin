<?php
namespace App\Enums;

class OrderStatusEnum {
    const SUBMIT = 'SUBMIT';
    const PROCESS = 'PROCESS';
    const PAID = 'PAID'; // Needs payment
    const PAID_CONFIRM = 'PAID_CONFIRM'; // paid confirmed
    const DELIVER = 'DELIVER';
    const FINISH = 'FINISH';
    const CANCEL = 'CANCEL';
}
