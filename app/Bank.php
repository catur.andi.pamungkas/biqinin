<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Bank
 *
 * @property-read \App\Order|null $order
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bank newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bank newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bank query()
 * @mixin \Eloquent
 */
class Bank extends Model
{
    protected $fillable = [
    	'bank_name', 'account_number','owner_name'
    ];

    public function order()
    {
    	return $this->hasOne('App\Order');
    }
}
