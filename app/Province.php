<?php

namespace App;

use Laravolt\Indonesia\Models\Province as ProvinceLaravolt;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Province
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\City[] $cities
 * @property-read int|null $cities_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\District[] $districts
 * @property-read int|null $districts_count
 * @property-read mixed $logo_path
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Province newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Province newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Province query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Laravolt\Indonesia\Models\Model search($keyword)
 * @mixin \Eloquent
 */
class Province extends ProvinceLaravolt
{
	protected $table = 'provinces';

    public function cities()
    {
        return $this->hasMany(City::class, 'province_id');
    }

    public function districts()
    {
        return $this->hasManyThrough(District::class, City::class);
    }
}
