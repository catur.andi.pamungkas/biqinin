<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Chat
 *
 * @property-read \App\Message $message
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Chat newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Chat newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Chat query()
 * @mixin \Eloquent
 */
class Chat extends Model
{
    protected $guarded = [];

    protected $dates = [
        'read_at',
    ];
    
    public function message()
    {
    	return $this->belongsTo(Message::class);
    }
}
