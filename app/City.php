<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravolt\Indonesia\Models\City as CityLaravolt;

/**
 * App\City
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Customer[] $customers
 * @property-read int|null $customers_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\District[] $districts
 * @property-read int|null $districts_count
 * @property-read mixed $logo_path
 * @property-read mixed $province_name
 * @property-read \App\Province $province
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Village[] $villages
 * @property-read int|null $villages_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Laravolt\Indonesia\Models\Model search($keyword)
 * @mixin \Eloquent
 */
class City extends CityLaravolt
{

    protected $table = 'cities';

    public function customers()
    {
    	return $this->hasMany(Customer::class);
    }

    public function province()
	{
	    return $this->belongsTo(Province::class, 'province_id');
	}

	public function districts()
    {
        return $this->hasMany(District::class, 'city_id');
    }

    public function villages()
    {
        return $this->hasManyThrough(Village::class, District::class);
    }
}
