<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Order
 *
 * @property-read \App\Bank $bank
 * @property-read \App\Customer $customer
 * @property-read \App\FinishingColor $finishing
 * @property-read \App\Wood $wood
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order query()
 * @mixin \Eloquent
 */
class Order extends Model
{
    public function customer()
    {
        return $this->belongsTo('App\Customer', 'customer_id');
    }

    public function wood()
    {
        return $this->belongsTo('App\Wood');
    }

    public function bank()
    {
        return $this->belongsTo('App\Bank');
    }

    public function finishing()
    {
        return $this->belongsTo('App\FinishingColor', 'finishing_color_id');
    }

    public function scopeWhereStatus($q, string $status)
    {
        return $q->where('order_status', $status);
    }
}
