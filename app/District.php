<?php

namespace App;
use Laravolt\Indonesia\Models\District as DistrictLaravolt;

use Illuminate\Database\Eloquent\Model;

/**
 * App\District
 *
 * @property-read \App\City $city
 * @property-read mixed $city_name
 * @property-read mixed $province_name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Village[] $villages
 * @property-read int|null $villages_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\District newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\District newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\District query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Laravolt\Indonesia\Models\Model search($keyword)
 * @mixin \Eloquent
 */
class District extends DistrictLaravolt
{

    protected $table = 'districts';

    public function city()
	{
	    return $this->belongsTo(City::class, 'city_id');
	}

	public function villages()
    {
        return $this->hasMany(Village::class, 'district_id');
    }
}
