<?php

namespace App\Http\View\Composers;

use App\Order;
use Illuminate\View\View;

class OrderStatisticComposer
{
    protected $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function compose(View $view)
    {
        $view->with([
            'submitted' => $this->order->where('order_status', 'SUBMIT')->count(),
            'processed' => $this->order->where('order_status', 'PROCESS')->count(),
            'cancelled' => $this->order->where('order_status', 'CANCEL')->count(),
            'delivered' => $this->order->where('order_status', 'DELIVER')->count(),
            'finished' => $this->order->where('order_status', 'FINISH')->count(),
            'has_paid' => $this->order->where('order_status', 'PAID')->count(),
            'paid_confirmed' => $this->order->where('order_status', 'PAID_CONFIRM')->count()
        ]);
    }
}
