<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    const IMAGE_PATH = 'storage/orders/';
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "customer_id" => $this->customer_id,
            "wood_id" => $this->wood->wood_name,
            "finishing_color_id" => $this->finishing->finishing_color_name,
            "invoice_number" => $this->invoice_number,
            "photo" => $this->photoUrl($this->photo),
            "number_of_orders" => $this->number_of_orders,
            "additional_information" => $this->additional_information !== '' ? $this->additional_information : '=',
            "order_status" => $this->orderStatus($this->order_status),
            "total_price" => formatRupiah($this->total_price),
            "work_estimation" => $this->work_estimation !== '' ? $this->work_estimation : 'Belum Diproses Admin',
            "courier" => $this->courier !== '' ? $this->courier : 'Belum Diproses Admin',
            "courier_cost" => formatRupiah($this->courier_cost),
            "order_status_enum" => $this->order_status,
            "airbill_number" => $this->airbill_number !== '' ? $this->airbill_number : 'Belum Ada Resi',
            "shipping_address" => $this->shipping_address,
            "total" => formatRupiah($this->total),
            "payment_type" => $this->payment_type,
            "payment_date" => $this->payment_date,
            "payment_amount" => formatRupiah($this->payment_amount),
            "payment_description" => $this->payment_description,
            "order_expired" => $this->order_expired,
            "is_payment_confirmed" => $this->is_payment_confirmed,
            "created_at" => tanggalIndonesia($this->created_at),
            "updated_at" => $this->updated_at,
        ];
    }

    private function photoUrl($photoPath)
    {
        return asset(self::IMAGE_PATH . $photoPath);
    }

    private function orderStatus($status)
    {
        switch ($status) {
            case 'SUBMIT':
                return 'Pesanan Disubmit';

            case 'PROCESS':
                return 'Pesanan Diproses';

            case 'FINISH':
                return 'Pesanan Selesai';

            case 'CANCEL':
                return 'Pesanan Dibatalkan';

            case 'PAID':
                return 'Pesanan Dibayar/Menunggu Konfirmasi Admin';

            case 'PAID_CONFIRM':
                return 'Pembayaran Dikonfirmasi Admin';

            case 'DELIVER':
                return 'Pesanan Dikirim';
            default:
                return '';
                break;
        }
    }
}
