<?php

namespace App\Http\Resources;

use App\Http\Resources\WoodResource;
use App\Wood;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class WoodCollection extends ResourceCollection
{

    // public function __construct()
    // {
    //      Resource::withoutWrapping();
    // }

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function (Wood $wood) {
            return new WoodResource($wood);
        });
        return parent::toArray($request);
    }
}
