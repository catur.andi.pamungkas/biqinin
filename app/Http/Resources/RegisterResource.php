<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RegisterResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'       =>  $this->id,
            'username' => $this->username,
            'email'    => $this->email,
            'avatar'   => $this->avatar,
            'email_verified_at'=> $this->readAtTiming($this->email_verified_at)
        ];
    }

    public function readAtTiming($ref)
    {
        return $ref ? $ref->diffForHumans() : null;
    }
}
