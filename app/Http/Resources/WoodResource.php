<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\Resource;

class WoodResource extends JsonResource
{
    const IMAGE_PATH = 'storage/woods/';

    
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'wood_name' => $this->wood_name,
            'photo' => $this->photoUrl($this->photo)
        ];
    }

    private function photoUrl($photoPath)
    {
        return asset(self::IMAGE_PATH . $photoPath);
    }
}
