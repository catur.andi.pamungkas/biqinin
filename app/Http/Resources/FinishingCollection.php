<?php

namespace App\Http\Resources;

use App\FinishingColor;
use App\Http\Resources\FinishingResource;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class FinishingCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function (FinishingColor $finishing) {
            return new FinishingResource($finishing);
        });
        return parent::toArray($request);
    }
}
