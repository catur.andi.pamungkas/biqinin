<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ChatResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'message' => $this->message['content'],
            'id' => $this->id,
            'type'=> $this->type,
            'read_at' =>$this->readAtTiming($this),
            'send_at' => $this->created_at->format('H:i')
        ];
    }

    public function readAtTiming($ref)
    {
        //jika sender kembalikan data dibaca pada, else return null
        $read_at = $ref->type == 0 ? $ref->read_at : null; 
        //jika sender punya data read_at, kembalikan dengan format humanize date
        return $read_at ? $read_at->diffForHumans() : null;
    }
}
