<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\Resource;

class FinishingResource extends JsonResource
{
    const IMAGE_PATH = 'storage/finishings/';

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'finishing_color_name' => $this->finishing_color_name,
            'photo' => $this->photoUrl($this->photo)
        ];
    }

    private function photoUrl($photoPath)
    {
        return asset(self::IMAGE_PATH . $photoPath);
    }
}
