<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class CustomerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" =>  $this->id,
            "first_name" => $this->customer->first_name,
            "last_name" => $this->customer->last_name,
            "address" => $this->customer->address,
            "postal_code" => $this->customer->postal_code,
            "phone_number" => $this->customer->phone_number,
            "username" =>  $this->username,
            "email" =>  $this->email,
            "email_verified_at" => Carbon::parse($this->email_verified_at)->diffForHumans(),
            "avatar" => $this->avatar,
            "registered_at" => $this->created_at->diffForHumans(),
            "city" => $this->customer->city->name,
            "district" => $this->customer->district->name,
            "province" => $this->customer->province->name,
            "village" => $this->customer->village->name
        ];
    }
}
