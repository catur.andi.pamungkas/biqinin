<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BankResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        static::withoutWrapping();
        return [
            'id'=> $this->id,
            'bank_name' => $this->bank_name,
            'owner_name' => $this->owner_name,
            'account_number' => $this->account_number
        ];
    }
}
