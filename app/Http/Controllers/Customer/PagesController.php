<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Setting;
use Illuminate\Http\Request;

class PagesController extends Controller
{
	public function howToOrder()
	{
		$setting = Setting::get()->first();
		return view('customer.pages.how_to_order', compact('setting'));
	}
}
