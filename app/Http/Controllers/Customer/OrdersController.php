<?php

namespace App\Http\Controllers\Customer;

use App\Bank;
use App\Wood;
use App\Order;
use Carbon\Carbon;
use Twizo\Api\Twizo;
use App\FinishingColor;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;
use App\Enums\OrderStatusEnum;
use App\Notifications\OrderPaid;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Notifications\InvoiceOrderPaid;
use App\Http\Controllers\Admin\UploadController;
use Illuminate\Http\JsonResponse;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Contracts\Container\BindingResolutionException;

class OrdersController extends Controller
{

    use UploadTrait;
    const IMAGE_PATH = 'payments';

    private $twizzo;

    public function __construct(){
        $this->twizzo = app()['twizzo'];
        $this->middleware(['auth', 'verified']);
    }

    public function rules()
    {
        return [
            'additional_information' => 'required',
            'wood_id' => 'required',
            'finishing_color_id' => 'required',
            'number_of_orders' => 'required|numeric|min:1'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $woods = Wood::all();
        $finishings = FinishingColor::all();
        return view('customer.orders.index', compact('woods', 'finishings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function history(Request $request, Builder $htmlBuilder)
    {
        $customer_id = auth()->user()->customer->id;
        $processedOrders = Order::with(['customer'])
                                ->where('customer_id', $customer_id)
                                ->where('order_status', '=', 'PROCESS')
                                ->latest()
                                ->get();
        $banks = Bank::all();

        $type = $request->get('type');

        $orders = Order::with(['customer'])
            ->when($request->type, function ($q) use ($request) {
                $q->where('order_status','=', $request->type);
            })
            ->where('customer_id', $customer_id)
            ->latest();

        if ($request->ajax()) {

            return DataTables::make($orders)
                ->editColumn('customer', function(Order $order){
                    return $order->customer->first_name . ' ' . $order->customer->last_name;
                })
                ->editColumn('created_at', function(Order $order){
                    return $order->created_at->diffForHumans() . " ($order->created_at)";
                })
                ->editColumn('airbill_number', function(Order $order){
                    if($order->airbill_number == ''){
                        return 'Belum Ada Resi';
                    }
                    return $order->airbill_number;
                })
                ->editColumn('status', function(Order $order){
                    // return '<span class="badge badge-info">' . $order->order_status. '</span>';
                    return view('components.formatters.order_table.order_status', compact('order'));
                })
                ->editColumn('photo', function(Order $order){
                    $file = asset('storage/orders/' . $order->photo);
                    $caption = $order->invoice_number;
                    return view('inc._image', compact('file', 'caption'));
                })
                ->addColumn('action', function (Order $order) {
                    return view('inc._action_order_customer', compact('order'));
                })
                ->rawColumns(['photo', 'action', 'status'])
                ->toJson(true);
        }

        $html = $htmlBuilder
            ->addColumn(['data' => 'created_at', 'name' => 'created_at', 'title' => 'Tanggal', '
                    responsive' => true])
            ->addColumn(['data' => 'invoice_number', 'name' => 'invoice_number', 'title' => 'Invoice', '
                responsive' => true])

            ->addColumn(['data' => 'status', 'name' => 'order_status', 'title' => 'Status', '
                responsive' => true])
            ->addColumn(['data' => 'airbill_number', 'name' => 'airbill_number', 'title' => 'No. Resi', '
                responsive' => true])
            ->addColumn(['data' => 'customer', 'name' => 'customer.first_name', 'title' => 'Customer', 'responsive' => true])
            ->addColumn(['data' => 'photo', 'name' => 'photo', 'title' => 'Foto', 'responsive' => true, 'orderable' => false])
            ->addColumn(['data' => 'action', 'name' => 'action', 'title' => 'Action', 'orderable' => false, 'searchable' => false]);
        return view('customer.history.index', compact('html', 'processedOrders', 'banks'));
    }

    public function detail(int $id)
    {
        $order = Order::with(['customer','wood', 'finishing'])->findOrFail($id);
        return response()->json($order,200);
    }

    public function repeatOrder(Request $request)
    {
        $id = $request['id'];
        $order = Order::with(['customer'])->findOrFail($id);
        $unique_id = (int) substr(hexdec(uniqid()),-3);
        $order->total = ($order->total_price + $order->courier_cost + $unique_id);

        $order->order_status = OrderStatusEnum::PROCESS;
        $order->created_at = Carbon::now();
        $order->order_expired = Carbon::parse($order->created_at)->addHours(6);
        $order->update();

        $format_waktu_expired = $order->order_expired->format('d M H:m');
        $message = "Pesanan anda akan kami proses ulang. Cek detail pesanan pada email anda. Lakukan pembayaran sebelum $format_waktu_expired atau pesanan anda otomatis dibatalkan.\n-BIQININ";

        $this->twizzo->createSms($message, $order->customer->phone_number, config('twizzo.sender'))->sendSimple();

        $order->customer->user->notify(new InvoiceOrderPaid($order, $unique_id));

        return response()->json($order,200);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate($this->rules());
        $photoFileName = session()->get(UploadController::$key);

        \Storage::move('tmp/' . $photoFileName, 'public/orders/' . $photoFileName);
        $order = new Order;
        $order->photo = $photoFileName;
        $order->wood_id = $request->input('wood_id');
        $order->finishing_color_id = $request->input('finishing_color_id');
        $order->invoice_number = $request->input('invoice_number');
        $order->additional_information = $request->input('additional_information');
        $order->number_of_orders = $request->input('number_of_orders');

        $shipping_address  = $request->input('shipping_address');

        if($shipping_address == 'default'){
            $order->shipping_address = $request->input('default_address');
        }

        if(($shipping_address == 'other') && ($request->input('other_address') != '')){
            $order->shipping_address = $request->input('other_address');
        } else {
            $order->shipping_address = $request->input('default_address');
        }

        $order->order_status = OrderStatusEnum::SUBMIT;
        $order->customer_id = Auth::user()->customer->id;

        $order->save();

        return redirect('customer/history')->with('success', 'Pesanan anda telah berhasil, Admin akan segera memproses pesanan anda dan Invoice Pesanan akan dikirimkan ke alamat E-mail.');

    }

    public function paid(Request $request)
    {
        $rules = [
            'payment_type' => 'required|string',
            'payment_description' => 'required',
            'payment_amount' => 'required|numeric',
            'bank_id' => 'required'
        ];

        // dd($request->file('transfer_document'));

        $validator = \Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json($validator->errors(),400);
        }

        $id = $request['id'];
        $order = Order::with(['customer'])->findOrFail($id);
        $order->order_status = OrderStatusEnum::PAID;
        $order->payment_date = now();
        $order->payment_type = $request->payment_type;
        $order->payment_description = $request->payment_description;
        $order->payment_amount = $request->payment_amount;
        $order->bank_id = $request->bank_id;

        if ($request->hasFile('transfer_document')) {
            $file = $request->file('transfer_document');
            $photoFileName = "payments_" . $order->payment_date . "." . $file->getClientOriginalExtension();
            $file->storePubliclyAs('public/payments/', $photoFileName);
            $order->transfer_document = $photoFileName;
        }

        $order->update();
        // $order->customer->user->notify(new OrderPaid($order));
        return response()->json($order,200);
    }

    /**
     *
     * @param int $id
     * @return JsonResponse
     * @throws ModelNotFoundException
     * @throws BindingResolutionException
     */
    public function getWoodsWith(int $id)
    {
        $wood = Wood::with(['finishings'])->findOrFail($id);
        return response()->json($wood,200);
    }

    /**
     *
     * @param int $id
     * @return JsonResponse
     * @throws BindingResolutionException
     */
    public function getFinishingColor(int $id)
    {
        $finishing = FinishingColor::findOrFail($id);
        return response()->json($finishing,200);
    }

}
