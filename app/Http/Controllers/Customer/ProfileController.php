<?php

namespace App\Http\Controllers\Customer;

use App\Customer;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{

    public function rules()
    {
        return [
            'first_name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
            'phone_number' => ['required', 'string', 'starts_with:62'],
            'address' => ['required', 'string'],
            'province_id' => ['required'],
            'city_id' => ['required'],
            'district_id' => ['required'],
            'village_id' => ['required'],
            'postal_code' => ['required', 'string'],
        ];
    }

	public function index()
	{
		$user_id = Auth::user()->id;
    	$profile = Customer::with(['city','district','user','village', 'province'])->where('user_id', '=', $user_id)->get()->first();
		return view('customer.profile.index', compact('profile'));
	}

    public function edit()
    {
    	$provinces = \Indonesia::allProvinces();
    	$user_id = Auth::user()->id;
    	$profile = Customer::with(['city','district','user','village', 'province'])->where('user_id', '=', $user_id)->get()->first();
    	return view('customer.profile.edit', compact('profile', 'provinces'));
    }

    public function update(Request $request, int $id)
    {
    	$request->validate($this->rules(),['phone_number.starts_with' => 'Format Nomor HP Harus diawali dengan 62']);

        // dd($request->all());

        //Save Customer Data
        $customer = Customer::findOrFail($id);
        $customer->first_name = $request['first_name'];
        $customer->last_name = $request['last_name'];
        $customer->address = $request['address'];
        $customer->phone_number = $request['phone_number'];
        $customer->province_id = $request['province_id'];
        $customer->city_id = $request['city_id'];
        $customer->district_id = $request['district_id'];
        $customer->village_id = $request['village_id'];
        $customer->postal_code = $request['postal_code'];

        $customer->update();
        return redirect('customer/profile')->with('success', 'Profil Berhasil Diperbaharui');
    }

    public function password(Request $request)
    {
        $user = User::findOrFail(Auth::user()->id);
        $user->password = bcrypt($request->input('password'));
        $user->update();

        return redirect('customer/profile')->with('success', 'Kata Sandi Berhasil Diperbaharui, Untuk Memastikan Perubahan, Silahkan Logout dan Login Kembali');
    }
}
