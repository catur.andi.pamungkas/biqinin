<?php

namespace App\Http\Controllers;

use App\Events\BlockEvent;
use App\Session;
use Illuminate\Http\Request;

class BlockController extends Controller
{
    public function block(Session $session)
    {
        $session->block = true;
        $session->blocked_by = auth()->id();
        
        $session->update();
        broadcast(new BlockEvent($session->id, true));
        return response()->json($session, 200);
    }

    public function unblock(Session $session)
    {
        $session->block = false;
        $session->blocked_by = null;
       
        $session->update();
        broadcast(new BlockEvent($session->id, false));
        return response()->json($session, 200);
    }
}
