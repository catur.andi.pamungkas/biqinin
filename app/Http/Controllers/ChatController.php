<?php

namespace App\Http\Controllers;

use App\Events\MessageReadEvent;
use App\Events\PrivateChatEvent;
use App\Http\Resources\ChatResource;
use App\Session;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    public function index()
    {
        $user = auth()->user();
        if($user->hasRole(['admin|superadmin'])){
            return view('chats.admin');
        }
        return view('chats.customer');
        
    }

    public function sendMessage(Request $request, Session $session)
    {   
        // Save Many ke tabel messages
        $message =  $session->messages()->create([
            'content' => $request->content
        ]);

        #RULE:: (1 Pesan) => 2 user terkait (1 penerima, 1 pengirim)

        // Insert data Untuk Pengirim Chat
        $chat = $message->createForSender($session->id);

        // Replikasi data Untuk Penerima Pesan
        $message->createForReceiver($session->id, $request->to_user);

        broadcast(new PrivateChatEvent($message->content,$chat));

        return response($chat->id,200);
    }

    public function chats(Request $request, Session $session)
    {
        // Transformasi output dengan chat resource, gunakan collection karena
        // data yang akan ditampilkan adalah sebuah koleksi (list) yang jumlahnya banyak
        return ChatResource::collection($session->chats->where('user_id', '=', auth()->id()));
    }

    public function read(Request $request, Session $session)
    {
        $chats = $session->chats
                        ->where('read_at',null)
                        ->where('type', 0)
                        ->where('user_id', '!=', auth()->id());        

        foreach($chats as $chat){
            $chat->update([
                'read_at' => now()
            ]);
            broadcast(new MessageReadEvent(new ChatResource($chat), $chat->session_id));
        }
    }

    public function clear(Session $session)
    {
        $session->deleteChat();
        $session->chats->count() == 0 ? $session->deleteMessages() : '';
        return response()->json(['message' => 'Message Cleared'], 200);
    }
}
