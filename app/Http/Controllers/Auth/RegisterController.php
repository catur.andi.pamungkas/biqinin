<?php

namespace App\Http\Controllers\Auth;

use App\Customer;
use App\Http\Controllers\Controller;
use App\Province;
use App\Role;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Indonesia;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    public function showRegistrationForm()
    {
        $provinces = Province::orderBy('name', 'ASC')->get();
        return view('client.extra.register', compact('provinces'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => ['required', 'string', 'max:255', 'unique:users'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'first_name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
            'phone_number' => ['required', 'string', 'starts_with:62'],
            'address' => ['required', 'string'],
            'province_id' => ['required'],
            'city_id' => ['required'],
            'district_id' => ['required'],
            'village_id' => ['required'],
            'postal_code' => ['required', 'string'],
        ], [
            'phone_number.starts_with' => 'Format Nomor HP diawali dengan 62',
            'phone_number.unique' => 'Nomor HP sudah digunakan, gunakan nomor lain'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        // \DB::transaction(function () use ($data) {
        // Save User Data
        $user = new User;
        $user->username = $data['username'];
        $user->email = $data['email'];
        $user->password = Hash::make($data['password']);
        $user->save();

        $customerRole = Role::where('name', 'customer')->get()->first();
        $user->attachRole($customerRole);

        //Save Customer Data
        $customer = new Customer;
        $customer->user_id = $user->id;
        $customer->first_name = $data['first_name'];
        $customer->last_name = $data['last_name'];
        $customer->address = $data['address'];
        $customer->phone_number = $data['phone_number'];
        $customer->province_id = $data['province_id'];
        $customer->city_id = $data['city_id'];
        $customer->district_id = $data['district_id'];
        $customer->village_id = $data['village_id'];
        $customer->postal_code = $data['postal_code'];

        $user->customer()->save($customer);

        return $user;
        // });

    }
}
