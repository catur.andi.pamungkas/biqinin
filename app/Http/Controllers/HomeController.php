<?php

namespace App\Http\Controllers;

use App\Gallery;
use App\Setting;
use Illuminate\Http\Request;
use App\Facades\Indonesia;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->middleware('verified');
        return view('client.extra.home');
    }

    public function provinces()
    {
        $provinces = Indonesia::allProvinces();
        return response()->json($provinces, 200);
    }

    public function province(int $id)
    {
        $province = Indonesia::findProvince($id, ['cities']);
        return response()->json($province, 200);
    }

    public function district(int $id)
    {
        $district = Indonesia::findDistrict($id, ['villages']);
        return response()->json($district, 200);
    }

    public function city(int $id)
    {
        $city = Indonesia::findCity($id, ['districts', 'villages']);
        return response()->json($city, 200);
    }

    public function galleries()
    {
        $gallery = Gallery::all();
        return view('client.extra.gallery', compact('gallery'));
    }

    public function about()
    {
        $about = Setting::first()->about_company;
        return view('client.extra.about', compact('about'));
    }
}
