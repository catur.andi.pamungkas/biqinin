<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Socialite;

class AuthController extends Controller
{
    use AuthenticatesUsers;

    private $redirectTo = '/';
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider)
    {
    
        $user = Socialite::driver($provider)->user();        

        if(!$user){
            return redirect('auth/' . $provider);
        } 


   
        // $authUser = $this->findOrCreateUser($user, $provider);
  
        // $authUser = User::where('id', $user->id)->first();
        $authUser = $this->findOrCreateUser($user, $provider);
        
        
        if($authUser) //if user is found
        {
            

            $socialUser = session()->get('social');
            $authUser->provider = $provider;
            $authUser->provider_id = $socialUser->getId();
    
            $authUser->update();        
            
            Auth::login($authUser, true);
            return redirect($this->redirectTo);
        }
        else //if user is not found, redirect to register page
        {
            return redirect('/register')->with('error', 'Mohon Lakukan Pendaftaran Terlebih Dahulu Sebagai Member Biqinin.com dengan Mengisi Data Berikut, Login dengan Akun Sosial Media hanya berlaku setelah anda mendaftar menjadi member');
        }
       
        // $user = Socialite::driver($provider)->user();
        
        // if(!$user){
        //     return redirect('login');
        // }
        
        // $authUser = $this->findOrCreateUser($user, $provider);
        // Auth::login($authUser, true);
        // return redirect($this->redirectTo);
    }

    /**
     * If a user has registered before using social auth, return the user
     * else, create a new user object.
     * @param  $user Socialite user object
     * @param $provider Social auth provider
     * @return  User
     */
    public function findOrCreateUser($socialUser, $provider)
    {
        $account = User::where('provider', $provider)->where('provider_id', $socialUser->getId())->first();
        if ($account) {
            return $account->user;
        } else{
           $user = User::where('email', $socialUser->getEmail())->first();
           session(['social' => $socialUser]);

           // if(!$user){
           //  $user = User::create([
           //      'email'=> $socialUser->getEmail(),
           //      'username' => $socialUser->getEmail(),
           //      'provider' => $provider,
           //      'provider_id' => $socialUser->getId()
           //  ]);
            
           // }

            // if($user){
            //     if($user->provider_id == null || $user->provider==null){
            //         $user->provider = $provider;
            //         $user->provider_id = $socialUser->getId();
            
            //         $user->update();        
            //     }
            // }
           

           return $user;
        }
    }

    // public function findOrCreateUser($user, $provider)
    // {
    //     $authUser = User::where('provider_id', $user->id)->first();
    //     if ($authUser) {
    //         return $authUser;
    //     }
    //     else{
    //         $user = User::create([
    //             'email'=> $authUser->getEmail(),
    //             'username' => $authUser->getName(),
    //             'provider' => $provider,
    //             'provider_id' => $authUser->getId()
    //         ]);
    //         return $user;
    //     }
    // }
}
