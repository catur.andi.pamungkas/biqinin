<?php

namespace App\Http\Controllers;

use App\Notifications\OrderPaid;
use App\Order;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CallbackCekMutasi extends Controller
{
    
    public function cekMutasi(Request $request)
    {
    	$post = file_get_contents("php://input");
		$json = json_decode($post);
		$orders = Order::where('order_status', '=', 'PROCESS')->get();
		if( $json->action == "payment_report" )
		{
			foreach ($orders as $order) {
				foreach( $json->content->data as $data )
			    {
			        # Waktu transaksi dalam format unix timestamp
			        $time = $data->unix_timestamp;

			        # Tipe transaksi : credit / debit
			        $type = $data->type;

			        # Jumlah (2 desimal) : 50000.00
			        $amount = $data->amount;

			        # Berita transfer
			        $description = $data->description;

			        # Saldo rekening (2 desimal) : 1500000.00
			        $balance = $data->balance;

			        if($amount == $order->total){
		        		$order->order_status = "PAID";
		        		$order->payment_type= $type;
		        		$order->payment_date= Carbon::createFromTimestamp($time)->toDateTimeString();
		        		$order->payment_description = $description;
		        		$order->payment_amount = $amount;
		        		$order->update();

		        		$order->customer->user->notify(new OrderPaid($order));
		        	}
			    }
	        	
	        }
		    
		}
    }
}
