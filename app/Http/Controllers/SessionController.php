<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use App\Session;
use Illuminate\Http\Request;
use App\Http\Resources\UserResource;
use App\Http\Resources\SessionResource;

class SessionController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create(Request $request)
    {
        $session = Session::create([
            'user1_id' => auth()->id(),
            'user2_id' => $request->friend_id
        ]);

        $sessionResource = new SessionResource($session);
        broadcast($sessionResource, auth()->id());
        return $sessionResource;
    }

    public function getFriends()
    {
        $user = auth()->user();
        if ($user->isAn('admin') || $user->isA('superadmin')) {
            $friends = User::whereHas('roles', function ($query) {
                $query->where('name', '=', 'customer');
            })->where('id','!=', auth()->id())->get();

        } else { //if it is customers

            $friends = User::whereHas('roles', function ($query) {
                $query->where('name', '=', 'admin')->orWhere('name', '=', 'superadmin');
            })->where('id','!=', auth()->id())->get();
        }
        
        return UserResource::collection($friends);
    }
}
