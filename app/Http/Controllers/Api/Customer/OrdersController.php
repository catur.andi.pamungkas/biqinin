<?php

namespace App\Http\Controllers\Api\Customer;

use App\Enums\OrderStatusEnum;
use App\Wood;
use App\Order;
use App\FinishingColor;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\OrderResource;
use App\Http\Resources\OrderCollection;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Container\BindingResolutionException;

class OrdersController extends Controller
{
    use UploadTrait;
    const ORDER_IMAGE_PATH = 'orders';
    const PER_PAGE = 5;

    /**
     * Process The New Order From Customer
     *
     * @param Request $request
     * @return void
     */
    public function process(Request $request)
    {
        // Handling
        $order = new Order;
        $order->photo = $this->imageUploadBase64($request['photo'], self::ORDER_IMAGE_PATH);
        $order->wood_id = $request['wood_id'];
        $order->finishing_color_id = $request['finishing_color_id'];
        $order->invoice_number = $request['invoice_number'];
        $order->additional_information = $request['additional_information'];
        $order->number_of_orders = $request['number_of_orders'];

        $shipping_address  = $request['shipping_address'];

        if ($shipping_address == 'default') {
            $order->shipping_address = $request['default_address'];
        } else if ($shipping_address == 'other') {
            $order->shipping_address = $request['other_address'];
        }

        $order->order_status = 'SUBMIT';
        $order->customer_id = auth()->user()->customer->id;

        $order->save();

        return new OrderResource($order);
    }

    /**
     * Show The Order Detail
     *
     * @param Request $request
     * @return Order
     */
    public function detail($id)
    {
        // ID of Order
        $order = Order::with(['customer', 'wood', 'finishing'])->findOrFail($id);
        return new OrderResource($order);
    }

    /**
     * Get All Order History for User logged in
     *
     * @param Request $request
     * @return collections
     */
    public function history(Request $request)
    {
        $customer_id = auth()->user()->customer->id;
        $orders = Order::where('customer_id', $customer_id)->latest()->paginate(self::PER_PAGE);
        return new OrderCollection($orders);
    }

    /**
     * Filter Orders by Order Status
     *
     * @param Request $request
     * @return collections
     */
    public function filterByOrderStatus(Request $request)
    {
        $customer_id = auth()->user()->customer->id;
        $orders = Order::with(['customer'])
            ->when($request->order_status, function ($q) use ($request) {
                $q->where('order_status', '=', $request->order_status);
            })
            ->where('customer_id', $customer_id)
            ->latest()
            ->paginate(self::PER_PAGE);

        return new OrderCollection($orders);
    }

    /**
     * Search User Orders History
     *
     * @param Request $request
     * @return collections
     */
    public function search(Request $request)
    {
        $customer_id = auth()->user()->customer->id;
        $keyword = $request['keyword'];
        $orders = Order::where('customer_id', $customer_id)
            ->where('invoice_number', 'LIKE', "%$keyword%")
            ->paginate(10);
        return new OrderCollection($orders);
    }

    /**
     * Get All Woods Type
     *
     * @return collections
     */
    public function woods()
    {
        $woods = Wood::all();
        return response()->json($woods, 200);
    }

    /**
     * Get Finishing Colors Type by Selected Wood
     *
     * @param integer $id: wood id
     * @return collections
     */
    public function finishingColorsByWood(int $id)
    {
        $finishingColorsByWood = Wood::with(['finishings'])->findOrFail($id);
        return response()->json($finishingColorsByWood->finishings, 200);
    }

    /**
     * Show Finishing Color By Id
     *
     * @param integer $id
     * @return Finishing Color
     */
    public function finishingColor(int $id)
    {
        $finishing = FinishingColor::findOrFail($id);
        return response()->json($finishing, 200);
    }


    /**
     * Upload Proof of payment
     * @param Request $request
     * @return JsonResponse
     * @throws BindingResolutionException
     */
    public function uploadProofOfPayment(Request $request, Order $order)
    {
        if ($order->transfer_document && file_exists(storage_path('app/public/orders/' . $order->transfer_document))) {
            \Storage::delete('public/orders/' . $order->transfer_document);
        }
        $order->transfer_document = $this->imageUploadBase64($request->photo, 'payments');
        $order->order_status = OrderStatusEnum::PAID;
        $order->payment_date = now();
        $order->payment_type = $request->payment_type;
        $order->payment_description = $request->payment_description;
        $order->payment_amount = $request->payment_amount;
        $order->bank_id = $request->bank_id;
        $order->update();
        return response()->json($order, 200);
    }
}
