<?php

namespace App\Http\Controllers\Api\Customer;

use App\User;
use App\Customer;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\CustomerResource;

class ProfileController extends Controller
{
    use UploadTrait;
    const USER_IMAGE_PATH = 'user';

    /**
     * Get User Profile
     *
     * @param Request $request
     * @return void
     */
    public function index(Request $request)
    {
        $userProfile = auth()->user()->customer->load(['user', 'city', 'district', 'province', 'village']);
        return response()->json($userProfile);
    }

    /**
     * Change Data Profile of user
     *
     * @param Request $request
     * @return void
     */
    public function changeProfile(Request $request)
    {
        $user = auth()->user();
        $customer = Customer::with('user')->where('user_id', $user->id)->first();
        $customer->first_name = $request->first_name;
        $customer->last_name = $request->last_name;
        $customer->address = $request->address;
        $customer->phone_number = $request->phone_number;
        $customer->city_id = $request->city_id;
        $customer->province_id = $request->province_id;
        $customer->district_id = $request->district_id;
        $customer->village_id = $request->village_id;
        $customer->postal_code = $request->postal_code;

        if (!empty($request['password'])) {
            $user->password = $request['password'];
        }

        $customer->user()->associate($user);
        $customer->update();
        return response()->json($customer, 200);
    }

    /**
     * Change Profile Photo of User
     *
     * @param Request $request
     * @return void
     */
    public function changePhotoProfile(Request $request)
    {
        /** @var App\User */
        $user = auth()->user();
        if ($user->avatar && file_exists(storage_path('app/public/user/' . $user->avatar))) {
            \Storage::delete('public/user/' . $user->avatar);
        }
        $user->avatar = $this->imageUploadBase64($request['avatar'], self::USER_IMAGE_PATH);
        $user->update();
        return response()->json($user, 200);
    }

    /**
     * Get Customer Profile Data
     *
     * @param Request $request
     * @return void
     */
    public function customer(Request $request)
    {
        $user_id = auth()->user()->id;
        $customer = User::with(['customer', 'customer.city', 'customer.district', 'customer.province', 'customer.village'])->where('id', $user_id)->first();

        return response()->json(new CustomerResource($customer), 200);
    }
}
