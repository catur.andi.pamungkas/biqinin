<?php

namespace App\Http\Controllers\Api\Customer;

use App\Bank;
use App\Wood;
use App\Gallery;
use App\Setting;
use App\FinishingColor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\BankResource;
use App\Http\Resources\WoodCollection;
use App\Http\Resources\GalleryCollection;
use App\Http\Resources\FinishingCollection;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class PagesController extends Controller
{

    public function contact(Request $request)
    {
    	$contact = Setting::firstOrFail();
    	return response()->json($contact);
    }

    public function galleries(Request $request)
    {
    	return new GalleryCollection(Gallery::paginate(10));
    }

    public function helps(Request $request)
    {
    	$response = [
    		'woods' => new WoodCollection(Wood::all()),
    		'finishings' => new FinishingCollection(FinishingColor::all()),
    		'how_to_order' => Setting::first()->how_to_order,
    		'banks' => BankResource::collection(Bank::all())
    	];

    	return response()->json($response, 200);
    }


    /**
     *
     * @return JsonResponse
     * @throws BindingResolutionException
     */
    public function generalInfo()
    {
        return response()-> json([
            'how_to_make_payment' => Setting::first()->how_to_make_payment,
        ]);
    }

    /**
     * Handle bank list resource
     * @return AnonymousResourceCollection
     */
    public function bankList()
    {
        return BankResource::collection(Bank::all());
    }
}
