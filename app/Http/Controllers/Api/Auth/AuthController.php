<?php

namespace App\Http\Controllers\Api\Auth;

use App\Role;
use App\User;
use Exception;
use App\Customer;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;
use Laravel\Passport\Client;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\LoginResource;
use App\Http\Resources\CustomerResource;
use App\Http\Resources\RegisterResource;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Api\Auth\IssueTokenTrait as IssueTokenTrait;

class AuthController extends Controller
{
    use IssueTokenTrait;
    use UploadTrait;

    const CLIENT_ID = 2;
    const USER_IMAGE_PATH = 'user';
    private $client;

    public function __construct()
    {
        $this->client = Client::find(self::CLIENT_ID);
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => ['required'],
            'password' => ['required'],
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }


        $token = $this->issueToken($request, 'password');
        return $token;
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => ['required', 'string', 'max:255', 'unique:users'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6'],
            'first_name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
            'phone_number' => ['required', 'string', 'starts_with:62'],
            'address' => ['required', 'string'],
            'province_id' => ['required'],
            'city_id' => ['required'],
            'district_id' => ['required'],
            'village_id' => ['required'],
            'postal_code' => ['required'],
        ], [
            'phone_number.starts_with' => 'Format Nomor HP diawali dengan 62'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        \DB::beginTransaction();
        try {
            //Save The User
            $user = User::create([
                'username' => $request->username,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);

            $customerRole = Role::where('name', 'customer')->get()->first();
            $user->attachRole($customerRole);

            //Save Customer Data
            $customer = $user->customer()->create([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'address' => $request->address,
                'phone_number' => $request->phone_number,
                'province_id' => $request->province_id,
                'city_id' => $request->city_id,
                'district_id' => $request->district_id,
                'village_id' => $request->village_id,
                'postal_code' => $request->postal_code,
            ]);

            $user->sendEmailVerificationNotification();
            \DB::commit();

            return response()->json(new RegisterResource($user), 200);
        } catch (Exception $e) {
            \DB::rollback();
        }
    }

    public function refresh(Request $request)
    {
        $this->validate($request, [
            'refresh_token' => 'required'
        ]);

        return $this->issueToken($request, 'refresh_token');
    }

    public function profile(Request $request)
    {
        return response()->json(auth()->user(), 200);
    }

    public function logout(Request $request)
    {
        $accessToken = auth()->user()->token();
        \DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update(['revoked' => true]);

        $accessToken->revoke();
        return response()->json([], 204);
    }
}
