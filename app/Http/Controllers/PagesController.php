<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;

class PagesController extends Controller
{
	public function invoice()
	{
		return PDF::loadView('pdf.invoice')->setPaper('A4','portrait')->stream('_sp.pdf');
	}
}
