<?php

namespace App\Http\Controllers\Admin;

use App\Bank;
use App\Enums\OrderStatusEnum;
use App\Http\Controllers\Controller;
use App\Http\Requests\OrderPaidRequest;
use App\Http\Requests\OrderProcessRequest;
use App\Mail\MailCancelledOrder;
use App\Notifications\InvoiceOrderPaid;
use App\Notifications\OrderCancelled;
use App\Notifications\OrderDelivered;
use App\Notifications\OrderPaid;
use App\Order;
use App\Setting;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\MassAssignmentException;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use PDF;
use Twizo\Api\Twizo;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder;

class OrdersController extends Controller
{
    private $twizzo;

    public function __construct()
    {
        $this->twizzo = app()['twizzo'];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Builder $htmlBuilder)
    {
        //$orders = Order::with(['customer'])->latest()->get();

        $orders = Order::with(['customer'])
            ->when($request->type, function ($q) use ($request) {
                $q->where('order_status','=', $request->type);
            })
            ->latest();


        if ($request->ajax()) {

            return DataTables::make($orders)
                ->editColumn('customer', function(Order $order){
                    return $order->customer->first_name . ' ' . $order->customer->last_name;
                })
                ->editColumn('created_at', function(Order $order){
                    return $order->created_at->diffForHumans();
                })
                ->editColumn('status', function(Order $order){
                    return view('components.formatters.order_table.order_status', compact('order'));
                })
                ->editColumn('photo', function(Order $order){
                    $file = asset('storage/orders/' . $order->photo);
                    $caption = $order->invoice_number;
                    return view('inc._image', compact('file', 'caption'));
                })
                ->addColumn('action', function (Order $order) {
                    return view('inc._action', compact('order'));
                })
                ->rawColumns(['photo', 'action', 'status'])
                ->toJson(true);
        }

        $html = $htmlBuilder
            ->addColumn(['data' => 'created_at', 'name' => 'created_at', 'title' => 'Tanggal', 'responsive' => true])
            ->addColumn(['data' => 'invoice_number', 'name' => 'invoice_number', 'title' => 'Invoice', '
                responsive' => true])
            ->addColumn(['data' => 'status', 'name' => 'order_status', 'title' => 'Status', '
                responsive' => true])
            ->addColumn(['data' => 'customer', 'name' => 'customer.first_name', 'title' => 'Customer', 'responsive' => true])
            ->addColumn(['data' => 'photo', 'name' => 'photo', 'title' => 'Foto', 'responsive' => true, 'orderable' => false])
            ->addColumn(['data' => 'action', 'name' => 'action', 'title' => 'Action', 'orderable' => false, 'searchable' => false]);
        return view('admin.orders.index', compact('html'));
    }

    public function detail(int $id)
    {
        $order = Order::with(['customer','customer.city','customer.district','customer.province', 'customer.village','wood', 'finishing'])->findOrFail($id);
        return response()->json($order,200);
    }

    public function process(OrderProcessRequest $request)
    {
        $id = $request['id'];
        $order = Order::with(['customer'])->findOrFail($id);
        $unique_id = (int) substr(hexdec(uniqid()),-3);
        $order->total_price = $request['total_price'];
        $order->work_estimation = $request['work_estimation'];
        $order->courier = $request['courier'];
        $order->courier_cost = $request['courier_cost'];
        $order->total = ($request['total_price'] + $request['courier_cost'] + $unique_id);

        $order->order_status = 'PROCESS';
        $order->order_expired = Carbon::parse($order->created_at)->addHours(6);
        $order->update();

        $expiredOrderFormat = $order->order_expired->format('d M H:m');
        $message = "Pesanan anda akan kami proses. Cek detail pesanan pada email anda. Lakukan pembayaran sebelum $expiredOrderFormat atau pesanan anda otomatis dibatalkan.\n-BIQININ";

        // foreach ($banks as $bank) {
        //     $message_bank = "$bank->bank_name - $bank->account_number a/n $bank->owner_name\n";
        // }

        // $message = $message . $message_bank . "Lakukan Transfer Sebesar".  formatRupiah($order->total). " (Harus Sesuai Nominal), Maksimal Transfer 3 Hari Jika Tidak pesanan anda otomatis dibatalkan sistem\n\nTerimakasih, Biqinin.com";
        // $twizo = Twizo::getInstance(config('twizzo.app_key'), config('twizzo.host'));
        $this->twizzo->createSms($message, $order->customer->phone_number, config('twizzo.sender'))->sendSimple();

        $order->customer->user->notify(new InvoiceOrderPaid($order, $unique_id));

        return response()->json($order,200);

    }

    public function cancel(Request $request)
    {
        $id = $request['id'];
        $order = Order::with(['customer'])->findOrFail($id);
        $order->order_status = OrderStatusEnum::CANCEL;
        $order->update();

        $order->customer->user->notify(new OrderCancelled($order));

        return response()->json($order,200);
    }

    /**
     *
     * @param OrderPaidRequest $request
     * @return JsonResponse
     * @throws ModelNotFoundException
     * @throws MassAssignmentException
     * @throws BindingResolutionException
     */
    public function paid(OrderPaidRequest $request)
    {
        $id = $request['id'];
        $order = Order::with(['customer'])->findOrFail($id);
        $order->order_status = OrderStatusEnum::PAID;
        $order->payment_date = Carbon::now();
        $order->payment_type = $request['payment_type'];
        $order->payment_description = $request['payment_description'];
        $order->payment_amount = $request['payment_amount'];
        $order->is_payment_confirmed = 1;
        $order->update();
        $order->customer->user->notify(new OrderPaid($order));
        return response()->json($order,200);
    }

    /**
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ModelNotFoundException
     * @throws MassAssignmentException
     * @throws BindingResolutionException
     */
    public function setOrderPaymentComplete(Request $request)
    {
        $id = $request['id'];
        $order = Order::with(['customer'])->findOrFail($id);
        $order->order_status = OrderStatusEnum::PAID_CONFIRM;
        $order->is_payment_confirmed = 1;
        $order->update();
        $order->customer->user->notify(new OrderPaid($order));
        return response()->json($order,200);
    }

    /**
     *
     * @param Request $request
     * @return JsonResponse
     * @throws BindingResolutionException
     * @throws ModelNotFoundException
     * @throws MassAssignmentException
     */
    public function updateAirbill(Request $request)
    {
        $rules = [
            'airbill_number' => 'required',
        ];

        $validator = \Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json($validator->errors(),400);
        }

        $id = $request['id'];
        $order = Order::with(['customer'])->findOrFail($id);
        $order->airbill_number = $request['airbill_number'];
        $order->order_status = 'DELIVER';
        $order->update();
        $order->customer->user->notify(new OrderDelivered($order));
        return response()->json($order,200);
    }

    public function printInvoice(int $id)
    {
        $order = Order::with(['wood', 'finishing', 'customer'])->findOrFail($id);
        $setting = Setting::firstOrFail();

        // dd($setting->site_name);
        $data = [
            'invoice' => $order->invoice_number,
            'order_date' => tanggalIndonesia($order->created_at),
            'customer' => $order->customer->first_name . ' ' . $order->customer->last_name,
            'address' => $order->shipping_address,
            'wood' => $order->wood->wood_name,
            'finishing' => $order->finishing->finishing_color_name,
            'number_of_orders' => $order->number_of_orders,
            'work_estimation' => $order->work_estimation,
            'total_price' => formatRupiah($order->total_price), //Harga Satuan Barang
            'courier' => $order->courier,
            'courier_cost' => formatRupiah($order->courier_cost),
            'additional_information' => $order->additional_information,
            'total' => formatRupiah($order->total),
            'admin' => auth()->user()->username,
            'site_name' => $setting->site_name,
            'office_address' => $setting->office_address,
            'email' => $setting->email,
            'office_phone' => $setting->phone_number,
            'office_whatsapp' => $setting->whatsapp
        ];

        return PDF::loadView('pdf.invoice', compact('data'))->setPaper('A4','portrait')->stream('_sp.pdf');
    }
}
