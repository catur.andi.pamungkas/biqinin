<?php

namespace App\Http\Controllers\Admin;

use App\Gallery;
use App\Http\Controllers\Admin\UploadController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GalleriesController extends Controller
{
    public function rules()
    {
        return [
            'caption' => 'required'
        ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $galleries = Gallery::paginate(5);

        $keyword = $request->get('keyword');
        if($keyword){
            $galleries = \App\Gallery::where('caption', 'LIKE', "%$keyword%")->latest()->paginate(5);
        }

        return view('admin.galleries.index', compact('galleries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.galleries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate($this->rules());
        $photoFileName = session()->get(UploadController::$key);

        \Storage::move('tmp/' . $photoFileName, 'public/gallery/' . $photoFileName);
        $gallery = new Gallery;
        $gallery->photo = $photoFileName;
        $gallery->caption = $request->input('caption');

        $gallery->save();

        session()->forget(UploadController::$key);

        return redirect('admin/galleries')->with('success', 'Foto Berhasil Disimpan Ke Database');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gallery = Gallery::findOrFail($id);
        return view('admin.galleries.edit', compact('gallery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate($this->rules());

        $gallery = Gallery::findOrFail($id);
        
        $gallery->caption = $request->input('caption');

        if(session()->get(UploadController::$key) != null){
            $photoFileName = session()->get(UploadController::$key);
            \Storage::move('tmp/' . $photoFileName, 'public/gallery/' . $photoFileName);

            if($gallery->photo && file_exists(storage_path('app/public/gallery/' . $gallery->photo))){
                \Storage::delete('public/gallery/'.$gallery->photo);
            }
            
            $gallery->photo = $photoFileName;

        }
        
        $gallery->update();

        session()->forget(UploadController::$key);

        return redirect('admin/galleries')->with('success', 'Foto Berhasil Disimpan Ke Database');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gallery = Gallery::findOrFail($id);
        if($gallery->photo && file_exists(storage_path('app/public/gallery/' . $gallery->photo))){
            \Storage::delete('public/gallery/'.$gallery->photo);
            $gallery->delete();

            session()->flash('success', 'Foto Galeri Berhasil Dihapus');
            return response()->json(['success' => true], 200);
        }
    }
}
