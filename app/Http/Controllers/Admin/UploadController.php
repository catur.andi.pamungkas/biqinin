<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
	public static $key = 'recentFileUploadName';

   	public function upload(Request $request){
    	$file = $request->file('photo');
    	$uploadingFile = Storage::put('tmp', $file);
    	$uploadingFileKeyName = basename($uploadingFile);
    	session([self::$key => $uploadingFileKeyName]);
    	return $uploadingFile;
    }

    public function revert(Request $request)
    {
    	$fileName = session()->get(self::$key);
    	Storage::delete('tmp/' . $fileName);
    	return null;

    }
}
