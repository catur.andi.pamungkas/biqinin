<?php

namespace App\Http\Controllers\Admin;

use App\FinishingColor;
use App\Http\Controllers\Admin\UploadController;
use App\Http\Controllers\Controller;
use App\Http\Requests\FinishingColorRequest;
use App\Wood;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class FinishingColorsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $finishings = \App\FinishingColor::with(['woods'])->when($request->keyword, function($q) use ($request) {
            $q->where('finishing_color_name', 'LIKE', "%$request->keyword%");
        })->latest()->paginate(5);

        return view('admin.finishing.index', compact('finishings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $woods = Wood::all();
        return view('admin.finishing.create', compact('woods'));
    }

    public function show(int $id)
    {
        $finishing = FinishingColor::with(['woods'])->findOrFail($id);
        return response()->json($finishing,200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\FinishingColorRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FinishingColorRequest $request)
    {
        $photoFileName = session()->get(UploadController::$key);

        $finishing = new FinishingColor;
        $finishing->finishing_color_name = $request->input('finishing_color_name');

        if (\Storage::move('tmp/' . $photoFileName, 'public/finishings/' . $photoFileName)) {
            $finishing->photo = $photoFileName;
        }

        $finishing->save();

        $wood_finishings = $request->input('wood_finishings');
        $finishing->woods()->attach($wood_finishings);

        session()->forget(UploadController::$key);

        return redirect('admin/finishing')->with('success', 'Finishing Warna Berhasil Disimpan Ke Database');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $woods = Wood::all();
        $finishing = FinishingColor::findOrFail($id);
        return view('admin.finishing.edit', compact('finishing', 'woods'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\FinishingColorRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FinishingColorRequest $request, $id)
    {
        $finishing = FinishingColor::findOrFail($id);

        if(session()->get(UploadController::$key) != null){
            $photoFileName = session()->get(UploadController::$key);
            \Storage::move('tmp/' . $photoFileName, 'public/finishings/' . $photoFileName);


            if($finishing->photo && file_exists(storage_path('app/public/finishings/' . $finishing->photo))){
                \Storage::delete('public/finishings/'.$finishing->photo);
            }

            $finishing->photo = $photoFileName;
        }


        $finishing->finishing_color_name = $request->input('finishing_color_name');
        $finishing->update();

        $wood_finishings = $request->input('wood_finishings');
        $finishing->woods()->sync($wood_finishings);

        session()->forget(UploadController::$key);

        return redirect('admin/finishing')->with('success', 'Finishing Warna Berhasil Diperbaharui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $finishing = FinishingColor::findOrFail($id);

        if($finishing->photo && file_exists(storage_path('app/public/finishings/' . $finishing->photo))){
            \Storage::delete('public/finishings/'.$finishing->photo);
        }

        $finishing->woods()->detach();
        $finishing->delete();
        session()->flash('success', 'Finishing Warna Berhasil Dihapus');
        return response()->json(['success' => true], 200);
    }
}
