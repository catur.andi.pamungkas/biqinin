<?php

namespace App\Http\Controllers\Admin;

use App\Customer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CustomersController extends Controller
{
    public function index(Request $request)
    {
        $customers = Customer::with('user')->whereHas('user', function($q) use ($request) {
            $q->when($request->keyword, function ($q1) use($request) {
                $q1->where('email', 'LIKE', "%$request->keyword%");
            });
        })->latest()->paginate(5);

        return view('admin.customers.index', compact('customers'));
    }

    public function show($id)
    {
        $customer = Customer::with(['city','district','user','village', 'province'])->findOrFail($id);
        return response()->json($customer,200);
    }
}
