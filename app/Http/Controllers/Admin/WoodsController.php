<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\UploadController;
use App\Http\Controllers\Controller;
use App\Http\Requests\WoodRequest;
use App\Wood;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule; //From Namespace Model

class WoodsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $woods = Wood::when($request->keyword, function($q) use ($request) {
            $q->where('wood_name', 'LIKE', "%$request->keyword%");
        })->latest()->paginate(5);
        return view('admin.woods.index', compact('woods'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.woods.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\WoodRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WoodRequest $request)
    {
        $photoFileName = session()->get(UploadController::$key);

        \Storage::move('tmp/' . $photoFileName, 'public/woods/' . $photoFileName);
        Wood::create([
            'photo' => $photoFileName,
            'wood_name' => $request->wood_name
        ]);

        session()->forget(UploadController::$key);

        return redirect('admin/woods')->with('success', 'Jenis Kayu Berhasil Disimpan Ke Database');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $wood = Wood::findOrFail($id);
        return view('admin.woods.edit', compact('wood'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\WoodRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(WoodRequest $request, $id)
    {
        $wood = Wood::findOrFail($id);

        if(session()->get(UploadController::$key) != null){
            $photoFileName = session()->get(UploadController::$key);
            \Storage::move('tmp/' . $photoFileName, 'public/woods/' . $photoFileName);

            if($wood->photo && file_exists(storage_path('app/public/woods/' . $wood->photo))){
                \Storage::delete('public/woods/'.$wood->photo);
            }

            $wood->photo = $photoFileName;

        }

        $wood->wood_name = $request->input('wood_name');
        $wood->update();

        session()->forget(UploadController::$key);
        return redirect('admin/woods')->with('success', 'Jenis Kayu Berhasil Diperbaharui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $wood = Wood::findOrFail($id);
        if($wood->photo && file_exists(storage_path('app/public/woods/' . $wood->photo))){
            \Storage::delete('public/woods/'.$wood->photo);
        }
        $wood->delete();
        session()->flash('success', 'Jenis Kayu Berhasil Dihapus');
        return response()->json(['success' => true], 200);


    }
}
