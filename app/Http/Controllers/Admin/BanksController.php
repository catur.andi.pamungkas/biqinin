<?php

namespace App\Http\Controllers\Admin;

use Cache;
use App\Bank;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\BankRequest;
use App\Http\Controllers\Controller;

class BanksController extends Controller
{

    public function index(Request $request)
    {
        $banks = Bank::when($request->get('keyword'), function($q) use ($request) {
            $q->where('bank_name', 'LIKE', "%$request->keyword%");
        })->latest()->paginate(5);

        return view('admin.banks.index', compact('banks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.banks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BankRequest $request)
    {
        $data = Bank::create($request->validated());
        return redirect('admin/banks')->with('success', 'Akun Bank Berhasil Disimpan Ke Database');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bank = Bank::findOrFail($id);
        return view('admin.banks.edit', compact('bank'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BankRequest $request, $id)
    {
        Bank::findOrFail($id)->update($request->validated());
        return redirect('admin/banks')->with('success', 'Akun Bank Berhasil Diperbaharui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bank = Bank::findOrFail($id)->delete();
        session()->flash('success', 'Akun Bank Berhasil Dihapus');
        return response()->json(['success' => true], 200);
    }
}
