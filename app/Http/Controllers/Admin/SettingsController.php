<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Setting;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SettingsController extends Controller
{
    public function index(Request $request)
    {
    	$setting = Setting::get()->first();
    	$user = Auth::user();
    	return view('admin.settings.index', compact('setting','user'));
    }

    public function update(Request $request, int $id)
    {
    	$setting = Setting::findOrFail($id);
        $setting->site_name = $request->input('site_name');
        $setting->office_address = $request->input('office_address');
        $setting->whatsapp = $request->input('whatsapp');
        $setting->facebook = $request->input('facebook');
        $setting->instagram = $request->input('instagram');
        $setting->phone_number = $request->input('phone_number');
        $setting->about_company = $request->input('about_company');
        $setting->owner_name = $request->input('owner_name');
        $setting->npwp = $request->input('npwp');
        $setting->how_to_order = $request->input('how_to_order');
        $setting->how_to_make_payment = $request->input('how_to_make_payment');
        $setting->update();

        return redirect('admin/settings')->with('success', 'Pengaturan Website Berhasil Diperbaharui');
    }

    public function password(Request $request)
    {
        $user = User::findOrFail(Auth::user()->id);
        $user->password = bcrypt($request->input('password'));
        $user->update();

        return redirect('admin/settings')->with('success', 'Kata Sandi Admin Berhasil Diperbaharui');
    }
}
