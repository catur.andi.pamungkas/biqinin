<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\FinishingColor
 *
 * @property-read \App\Order|null $order
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Wood[] $woods
 * @property-read int|null $woods_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FinishingColor newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FinishingColor newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FinishingColor query()
 * @mixin \Eloquent
 */
class FinishingColor extends Model
{
	protected $table = 'finishing_colors';
    protected $fillable = [
    	'finishing_color_name',
    	'photo'
    ];

    public function order()
    {
    	return $this->hasOne('App\Order');
    }

    public function woods()
    {
    	return $this->belongsToMany('App\Wood', 'wood_finishings');
    }
}
