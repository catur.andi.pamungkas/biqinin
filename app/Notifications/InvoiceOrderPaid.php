<?php

namespace App\Notifications;

use App\Bank;
use App\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class InvoiceOrderPaid extends Notification
{
    use Queueable;

    public $order;
    public $unique_id;
    public $banks;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Order $order, int $unique_id)
    {   
        $this->order = $order;
        $this->unique_id = $unique_id;
        $this->banks = Bank::all();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Pesanan ' . $this->order->invoice_number . ' Diproses Biqinin.com')
                    ->markdown('email.orders', ['order' => $this->order, 'unique_id' => $this->unique_id, 'banks' => $this->banks]);
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
