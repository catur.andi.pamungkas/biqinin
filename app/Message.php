<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Message
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Chat[] $chats
 * @property-read int|null $chats_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Message newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Message newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Message query()
 * @mixin \Eloquent
 */
class Message extends Model
{
    protected $guarded = [];
    
    public function chats()
    {
    	return $this->hasMany(Chat::class);
    }

    public function createForSender($session_id)
    {
        return $this->chats()->create([
            'session_id'=>$session_id,
            'type' => 0, //sender
            'user_id' => auth()->id()
        ]);
    }

    public function createForReceiver($session_id, $to_user)
    {
        return $this->chats()->create([
            'session_id'=>$session_id,
            'type' => 1, //receiver
            'user_id' => $to_user
        ]);
    }
}
