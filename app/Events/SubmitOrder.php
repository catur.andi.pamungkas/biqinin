<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SubmitOrder
{
    use Dispatchable, InteractsWithSockets, SerializesModels;


    public $order;
    public $message;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($order)
    {
        
        $this->order = $order;
        $this->message = "Pesanan Baru Dari $order->customer->first_name $order->customer->last_name Dengan Nomor Invoice $order->invoice_number";
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('order-submitted');
    }
}
