<?php

namespace App;

use Laravolt\Indonesia\Models\Village as VillageLaravolt;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Village
 *
 * @property-read \App\District $district
 * @property-read mixed $city_name
 * @property-read mixed $district_name
 * @property-read mixed $province_name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Village newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Village newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Village query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Laravolt\Indonesia\Models\Model search($keyword)
 * @mixin \Eloquent
 */
class Village extends VillageLaravolt
{

	protected $table = 'villages';

    public function district()
	{
	    return $this->belongsTo(District::class, 'district_id');
	}
}
