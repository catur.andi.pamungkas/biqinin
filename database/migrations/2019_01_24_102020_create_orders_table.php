<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('customer_id');
            $table->unsignedInteger('wood_id');
            $table->unsignedInteger('finishing_color_id');
            $table->string('invoice_number',255);
            $table->string('photo',255);
            $table->integer('number_of_orders');
            $table->text('additional_information');
            $table->enum('order_status', ['SUBMIT','PROCESS','PAID','PAID_CONFIRM','DELIVER', 'FINISH', 'CANCEL']);
            $table->decimal('total_price', 10,2)->nullable(); //harga Satuan barang
            $table->string('work_estimation', 255)->nullable();
            $table->string('courier',255)->nullable();
            $table->decimal('courier_cost',10,2)->nullable();
            $table->string('airbill_number',255)->nullable();
            $table->text('shipping_address')->nullable();
            $table->decimal('total', 10,2)->nullable(); //total keseluruhan pembayaran


            //diisi ketika integrasi API cek mutasi
            $table->string('payment_type')->nullable();
            $table->datetime('payment_date')->nullable();
            $table->decimal('payment_amount',10,2)->nullable();
            $table->text('payment_description')->nullable();
            $table->datetime('order_expired')->nullable();
            $table->timestamps();

            $table->foreign('customer_id')->references('id')->on('customers');
            $table->foreign('wood_id')->references('id')->on('woods')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('finishing_color_id')->references('id')->on('finishing_colors')->onUpdate('cascade')->onDelete('restrict');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
