<?php

use App\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = Role::where('name', 'admin')->get()->first();
        $customerRole = Role::where('name', 'customer')->get()->first();
        $superAdminRole = Role::where('name', 'superadmin')->get()->first();
    
		$user = new User;
		$user->username = 'admin';
		$user->email = "admin@biqinin.com";
		$user->email_verified_at = Carbon::now();
		$user->password = bcrypt('admin');
		$user->save();
        $user->attachRole($adminRole);

        $superAdminUser = new User;
        $superAdminUser->username = 'superadmin';
        $superAdminUser->email = "wisnu@biqinin.com";
        $superAdminUser->email_verified_at = Carbon::now();
        $superAdminUser->password = bcrypt('superadmin');
        $superAdminUser->save();
        $superAdminUser->attachRole($superAdminRole);

    }

}
