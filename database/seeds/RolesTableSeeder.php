<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$adminRole = new Role();
		$adminRole->name = "admin";
		$adminRole->display_name = "Administrator";
		$adminRole->save();


		$customer = new Role();
		$customer->name = 'customer';
		$customer->display_name = 'Customer';
		$customer->save();

		$superAdminRole = new Role();
		$superAdminRole->name = 'superadmin';
		$superAdminRole->display_name = 'Super Administrator';
		$superAdminRole->save();
		
    }
}
