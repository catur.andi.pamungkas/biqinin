<?php

use App\Setting;
use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $setting = new Setting;
        $setting->site_name = "Biqinin.com";
        $setting->office_address = "Jalan KH. Wahid Hasyim Kauman Batang Gg. 05";
        $setting->whatsapp = "+628564111111";
        $setting->facebook = "http://www.facebook.com/biqinin";
        $setting->instagram = "http://www.instagram.com/biqinin";
        $setting->phone_number = "+628564111111";
        $setting->about_company = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur quis itaque excepturi reiciendis est dicta consequatur, ad placeat pariatur iure harum molestiae, porro aut soluta tempore, illum facere vitae eos.";
        $setting->email = "biqinin@mail.com";
        $setting->owner_name = "Wisnu Adi Trianggoro";
        $setting->npwp = "xxx-xxx-xxx-xxx";
        $setting->how_to_make_payment = "<p>Pesanan anda sudah kami kami proses, Silakan lakukan transfer ke salah satu No. Rekening Bank Biqinin.com berikut ini :</p><ol><li><b>Bank BCA</b><br>02389232323 a/n Wisnuadi Trianggoro&nbsp;</li><li><b>Bank BRI</b><br>02839237283223 a/n Wisnuadi Trianggoro</li></ol><p>Setelah itu, lakukan upload bukti transfer anda melalui tombol dibawah ini. Terimakasih</p>";
        $setting->save();

    }
}
