window.fn = {};

window.fn.delete = (id,route)=>{

	swal({
		title: "Anda Yakin",
		  text: "File Anda Akan Dihapus",
		  type: "warning",
		  confirmButtonClass: "btn-danger",
		  confirmButtonText: "Ya, Hapus!",
		  closeOnConfirm: true,
		  title: "Are you sure?",
		  showCancelButton: true,
		  cancelButtonText: "Batalkan",
		  closeOnCancel: true
		},
		function(isConfirm) {
		  if (isConfirm) {
			axios.delete(route)
			  .then(function (response) {
			    console.log(response);
			  	if(response.status == 200){
			  		window.location.reload();
			  	}
			  })
			  .catch(function (error) {
			    console.log(error);
			  });
		  }
		}
	);	
}