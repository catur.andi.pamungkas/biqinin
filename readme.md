## Setelah Deploy  Jalankan :

> `artisan migrate:fresh --seed`

> `artisan laravolt:indonesia:seed`

> `artisan schedule:run`


#Konten How To Order

1. Lakukan Registrasi Sebagai Member (Registrasi Disini) , lalu Login ke Sistem Login Disini
2. Masuk Ke Halaman Dashboard
3. Masuk Menu Pemesanan
4. Upload Foto atau Gambar Sample yang ingin anda buat, pilih jenis kayu yang digunakan, warna finishing yang akan digunakan, masukkan informasi tambahan dan pilih alamat yang pengiriman.
5. Tunggu Pesanan anda diproses oleh kami, kami akan menginformasikan detail order anda via SMS dan E-Mail anda.
6. Transfer Sesuai Nominal yang diinformasikan agar konformasi pembayaran dilakukan secara otomatis oleh sistem kami.
7. Tunggu Pesanan datang.
8. Happy Shopping !